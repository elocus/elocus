# -*- coding: utf-8 -*-
#     kOLEKTi : a structural documentation generator
#     Copyright (C) 2007-2011 Stéphane Bonhomme (stephane@exselt.com)
#     Author Stéphane Bonhomme (stephane@exselt.com)
#     Author David Beniamine (david.beniamine@tetras-libre.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.



import os
import time
import shutil
import urllib
import urllib2
import json
import uuid
from lxml import etree as ET
from PIL import Image
from StringIO import StringIO
from zipfile import ZipFile
import tempfile
import subprocess
import re
import hashlib

import logging
logger = logging.getLogger(__name__)
import pygal

#from _odt import odtpdf
from kolekti.plugins import pluginBase

MFNS="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"

def getid(iter):
    return "10000000000%8X%05X"%(int(time.strftime('6013670%Y%m%d%M%S')),iter)

elocus_theme = os.getenv('KOLEKTI_THEME','elocus')


ns = {'namespaces': {'h':'http://www.w3.org/1999/xhtml'}}

class plugin(pluginBase.plugin):
    """
    Plugin for odt publication

    Injects the content of the pivot file into an odt template
    The way xhtml is converted into odt is defined :
    - by a xslt preprocessing of the xhtml file : design/publication/opendocument/[theme]/filter.xsl
    - by a set of conversion mapping rules design/publication/opendocument/[theme]/mapping.xml
    """

    _META_= {
        "templates":"design/publication/opendocument",
        "add_to_projects":True,
        "parameters":''
        }

    def export_report(self, inputf, formats, outputf):
        try:
            import socket
            from py3o.renderclient import RenderClient
            client = RenderClient(os.getenv('P3OSRV', 'py3orenderserver'), os.getenv('PY3OPORT', 8994))
            logger.debug("Base path : "+self._path)
            inf = self.syspath(inputf)
            logger.debug("inf : "+inf)
            outf = self.syspath(outputf)
            logger.debug("outf :"+outf)
            for fmt in formats:
                client.render(inf, outf+'.'+fmt, fmt)
        except socket.gaierror:
            shutil.copy(inf, outf)
            
    def postpub(self):
        res = []
        logger.debug(self.assembly_dir)
        dfile=StringIO()
        tfile = self.get_script_parameter('template')
        if tfile is None:
            tfile=self._plugin
        templatepath = '/'.join([self.get_base_template(self.scriptname),"%s.ott"%tfile])
        templatepath = '/kolekti/publication-templates/odt/%s.ott'%tfile
        logger.debug(templatepath)
        # get the theme elements for publishing

        mapfile = '/'.join([self._plugindir,"mapping.xml"])
        tmppivot = "%s/tmppiv.xml" %(self.publication_plugin_dir,)
        tmpstyles = "%s/tmpstyles.xml" %(self.publication_plugin_dir,)

        tmpdebug = "%s/tmpdebug.xml" %(self.publication_plugin_dir,)

        #coverfile= os.path.join(self.publisher.model.projectpath,tplpath,theme,"cover.xsl")
        #filterfile=os.path.join(self.publisher.model.projectpath,tplpath,theme,"filter.xsl")

        # res.append({'type':"opendocument", "label":"sss", "url": ""})
        # return res

        # generate elocus graphics
        self.generate_graphics()
        logger.debug(tmpdebug)

        # apply the pre filter.xsl to the pivot file
        try:
            xslt = self.get_plugin_xsl("filter", profile = self.profile, lang = self._publang)
            pivot = xslt(self.pivot)
        except:
            logger.exception('error in fiter pivot')
            pivot = self.pivot
            logger.debug(xslt.error_log )
        self.xwrite(pivot, tmpdebug, sync = False)
        # copy the template into the publication space
        # shutil.copy(tfile, dfile)


        # uncompress the template
        with ZipFile(self.getOsPath(templatepath),'r') as zipin:
            with ZipFile(dfile,'w') as zipout:
                # get the template index of files
                mf=ET.XML(zipin.read('META-INF/manifest.xml'))


                # handle all media
                odtids={}
                iter = 0
                try:
                    for img in pivot.xpath('/h:html/h:body//h:img', **ns):
                        newsrc = img.get('src')
                        if newsrc[0] == '/':
                            newsrc = self.getOsPath(self.assembly_dir + newsrc)
                        elif img.get('class','') == "projectlib":
                            newsrc = os.path.join(self._path, "sources", "fr", "images", "lib", newsrc)
                        else:
                            newsrc = os.path.join(self.cache_dir, 'img', newsrc)
                        logger.debug('image %s'%newsrc)
                        if odtids.get(newsrc, '') == '':
                            # get an uuid for the image
                            odtid = getid(iter)+os.path.splitext(newsrc)[1]
                            odtids.update({newsrc: odtid})
                            iter += 1
                            # copy the media in the zip
                            try:
                                with open(newsrc, "r") as f:
                                    imgdata = f.read()

                                zipout.writestr("Pictures/%s"%str(odtid),imgdata)

                                # registers the image in the manifest
                                ment=ET.SubElement(mf,"{%s}file-entry"%MFNS)
                                ment.set("{%s}media-type"%MFNS,"image/png")
                                ment.set("{%s}full-path"%MFNS,"Pictures/%s"%odtid)
                            except:
                                logger.exception('source image not found %s'%newsrc)
                        else:
                            odtid = odtids.get(newsrc)
                        # inserts the uuid in the pivot for futher references from xslt
                        img.set('newimgid',odtid)
                        try:
                            # sets the size and def of the image in the pivot for xslt processing
                            im = Image.open(newsrc)
                            (w,h)=im.size
                            try:
                                (dw,dh)=im.info['dpi']
                                img.set('orig_resw',str(dw))
                                img.set('orig_resh',str(dh))
                            except:
                                (dw,dh)=(150,150)
                            logger.debug('image props wh:[%dx%d] res:%d %d',w,h,dw,dh)
                            nw = (w/dw)*2.54
                            nh = (h/dh)*2.54     
                            if nw > 18:
                                ratio = nw / 18
                                nw = 18
                                nh = nh / ratio
                            logger.debug('image size: %dx%d',nw,nh)
                            img.set('print_width',str(nw))
                            img.set('print_height',str(nh))

                        except:
                            logger.exception('could not get image info')

                except:
                    logger.exception('could not add media to zip')
                mmt=mf.xpath('/manifest:manifest/manifest:file-entry[@manifest:full-path]', namespaces={'manifest':MFNS})[0]
                mmt.set('{urn:oasis:names:tc:opendocument:xmlns:manifest:1.0}media-type','application/vnd.oasis.opendocument.text')

                # write back the manifest in the produced odt file
                zipout.writestr('META-INF/manifest.xml', bytes=ET.tostring(mf))

                # creates a temporary pivot file (should use an xslt extension for that
                self.xwrite(pivot, tmppivot, sync=False)

                # creates a temporary styles file from the template, TODO : use an xslt extension
                styles = zipin.read('styles.xml')
                self.write(styles, tmpstyles, sync = False)
                styles = ET.XML(styles)



                # generates the metadata of the odt file

                tmeta=ET.XML(zipin.read('meta.xml'))

                try:
                    xslx=self.get_xsl('generate-meta')
                except:
                    logger.exception('XSL parse error generate-meta odt')
                    raise
                try:
                    logger.debug(tmppivot)
                    doc=xslx(tmeta, pivot="'%s'"%tmppivot)
                except:
                    logger.exception('XSL runtime error generate-meta odt')
                    logger.debug(xslx)
                    for entry in xslx.error_log:
                        logger.debug('message from line %s, col %s: %s' % (entry.line, entry.column, entry.message))

                zipout.writestr('meta.xml', bytes=str(doc))

                xslx = self.get_xsl('generate-styles')
                doc=xslx(styles,
                        pivot="'%s'"%tmppivot)

                for entry in xslx.error_log:
                    print('message from line %s, col %s: %s' % (entry.line, entry.column, entry.message))
                zipout.writestr('styles.xml', bytes=str(doc))

                # generates the content

                template=ET.XML(zipin.read('content.xml'))

                xslx = self.get_xsl('generate')
                content=xslx(template,
                         pivot="'%s'"%tmppivot,
                         styles="'%s'"%tmpstyles,
                         mapping="'file://%s'"%mapfile)
                self.xwrite(content, tmppivot+'-odt.xml', sync=False)
                for entry in xslx.error_log:
                    print('message from line %s, col %s: %s' % (entry.line, entry.column, entry.message))

                # self.xwrite(pivot, tmpdebug)
                zipout.writestr('content.xml', bytes=str(content))
                zipout.writestr('mimetype', bytes='application/vnd.oasis.opendocument.text')

                # Copy all unhandled files from template to generated doc

                for f in zipin.namelist():
                    if not zipout.namelist().__contains__(f):
                        zipout.writestr(f, bytes=zipin.read(f))

        docfilename = "%s/%s" %(self.publication_plugin_dir, self.publication_file)
        odtfilename = docfilename+'.odt'
        tmpfilename = docfilename+'_tmp.odt'

        # save the generated temporry zip (odt)
        self.write(dfile.getvalue(), tmpfilename, sync=False)
        # Generate all reports
        self.export_report(tmpfilename, ['doc', 'pdf', 'odt'], docfilename)
        # Remove temporary file
        os.remove(self.syspath(tmpfilename))

        #removes temporary files
        self.delete_resource(tmppivot)
        self.delete_resource(tmpstyles)

        # output file name
        res.append({'type':"opendocument", "label":"%s_%s"%(self.publication_file,self.scriptname), "url": odtfilename})
        return res

    def populate_sparql_data(self, assembly, ospath):
        path_query = '//h:div[@class="kolekti-sparql"]'
        for query in assembly.xpath(path_query, **ns):
            qid = query.get('id', None)
            if not qid is None:
                qid = qid.split('_')[-1]
                resdiv = query.xpath('h:div[@class="kolekti-sparql-result"]', **ns)
                if len(resdiv):
                    resjson = ET.SubElement(resdiv[0],'{http://www.w3.org/1999/xhtml}div', attrib = {"class":"kolekti-sparql-result-json"})
                    with open(os.path.join(ospath, 'json', qid+'.json'),'r') as jsonf:
                        resjson.text = jsonf.read()


    def generate_graphics(self):
        try:
            os.makedirs(os.path.join(self.cache_dir,'img'))
        except:
            pass

        self.populate_sparql_data(self.pivot, self.getOsPath(self.assembly_dir + '/sources/'+ self._publang + "/assembly"))
        xsl = self.get_system_xsl('components/collect_navmap_data')
        self.pivot = xsl(self.pivot, share="'False'", project="'none'", report="'none'")
        with open("/projects/.logs/pivot.html", 'w') as f:
            f.write(ET.tostring(self.pivot, encoding="utf-8"))

        for component in self.pivot.xpath('//h:div[starts-with(@class, "kolekti-component-")][not(@data-hidden)]', **ns):
            component_type = component.get('class').replace("kolekti-component-","")
            if hasattr(self, '_render_%s'%(component_type,)):
                payload = ET.tostring(component)
                comphash = hashlib.sha224(payload).hexdigest()
                compid = 'comp_' + comphash
                component.set('data-hash',compid)
                logger.debug('generate graphic for %s [%s]',compid, component_type)

                graphicfile = os.path.join(self.cache_dir, 'img', compid + '.png')
                if os.path.exists(graphicfile):
                    logger.debug('cached')
                    continue

                renderer = getattr(self, '_render_%s'%(component_type,))

                logger.debug('graphic export to %s',graphicfile)
                try:
                    renderer(component,  graphicfile)
                except:
                    logger.exception("unable to render component %s"%component_type)

    @property
    def staticpath(self):
#        return os.path.join(self._appdir,'..','kolekti_server/kserver/static')
        return "/static"
    
    def _render_map(self, component, imgpath):
        _, inpath = tempfile.mkstemp(suffix='.html')
        logger.debug('---render map---')
#        logger.debug(ET.tostring(component))
        xsl = self.get_system_xsl('components/render_map')
#        staticpath = os.path.join(self._appdir,'..','kolekti_server/kserver/static')
#        logger.debug(staticpath)
        domhtml = xsl(component, static = "'%s'"%self.staticpath, theme = "'%s'" % elocus_theme)
        if domhtml.xpath('.//h:div[@data-geojson=""] or not(.//h:div[@data-geojson])', **ns):
            logger.debug('json not found')
            return

        with open(inpath, 'w') as f:
            f.write('<!DOCTYPE html>\n')
            f.write(ET.tostring(domhtml, method="html", xml_declaration=None, pretty_print=True))
        self._rasterize_phantom("rasterize-map.js", inpath, imgpath)
#        os.unlink(inpath)

    def _render_chart(self, component, imgpath):
        _, inpath = tempfile.mkstemp(suffix='.html')
        xsl = self.get_system_xsl('components/render_chart')
#        staticpath = os.path.join(self._appdir,'..','kolekti_server/kserver/static')
        domhtml = xsl(component, static = "'%s'"%self.staticpath, theme = "'%s'" % elocus_theme)
        with open(inpath, 'w') as f:
            f.write('<!DOCTYPE html>\n')
            f.write(ET.tostring(domhtml, method="html", xml_declaration=None, pretty_print=True))
        self._rasterize_phantom("rasterize-chart.js", inpath, imgpath, "1200*630px")
#        os.unlink(inpath)

    def _render_svg(self, component, imgpath):
        _, inpath = tempfile.mkstemp(suffix='.html')
        xsl = self.get_system_xsl('components/render_svg')
#        staticpath = os.path.join(self._appdir,'..','kolekti_server/kserver/static')
        domhtml = xsl(component, static = "'%s'"%self.staticpath, theme = "'%s'" % elocus_theme)
        with open(inpath, 'w') as f:
            f.write('<!DOCTYPE html>\n')
            f.write(ET.tostring(domhtml, method="html", xml_declaration=None, pretty_print=True))
        self._rasterize_phantom("rasterize-svg.js", inpath, imgpath)
        os.unlink(inpath)
        
    def _render_navmap(self, component, imgpath):
        _, inpath = tempfile.mkstemp(suffix='_navmap.html')
        xsl = self.get_system_xsl('components/render_navmap')
#        staticpath = os.path.join(self._appdir,'..','kolekti_server/kserver/static')
        domhtml = xsl(component, static = "'%s'"%self.staticpath, theme = "'%s'" % elocus_theme)
        with open(inpath, 'w') as f:
            f.write('<!DOCTYPE html>\n')
            f.write(ET.tostring(domhtml, method="html", xml_declaration=None, pretty_print=True))
        self._rasterize_phantom("rasterize-navmap.js", inpath, imgpath)
        os.unlink(inpath)
        
    def _rasterize_phantom(self, script, inpath, imgpath, size="1200px"):
        try:
            _, tmpimg = tempfile.mkstemp(suffix='.png')
            cmd = ['phantomjs','--web-security=false','--disk-cache=true','/kolekti/src/kolekti_server/kserver/static/js/components/'+script, inpath, tmpimg, size]
            logger.debug(' '.join(cmd))
            exccmd = subprocess.Popen(
                cmd,
                shell=False,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                close_fds=True)
            err=exccmd.stderr.read()
            out=exccmd.stdout.read()
            exccmd.communicate()
            err=err.decode(self.LOCAL_ENCODING)
            out=out.decode(self.LOCAL_ENCODING)
            for line in err.split('\n'):
                logger.error(line)
            for line in out.split('\n'):
                logger.info(line)

            # set dpi information

            im = Image.open(tmpimg)
            im.save(imgpath.encode('utf-8'), dpi=(250,250) )
            os.unlink(tmpimg)
        except:
            logger.exception('error when raterize %s', str(cmd))

