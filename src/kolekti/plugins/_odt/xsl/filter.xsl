<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"   
		xmlns:html="http://www.w3.org/1999/xhtml"
		xmlns="http://www.w3.org/1999/xhtml"
		exclude-result-prefixes="html"
>

  <xsl:variable name="releasename" select="string(/html:html/html:head/html:title)"/>
  
  <xsl:template match="node()|@*">
    <xsl:copy>
	<xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="html:h1">
    <xsl:variable name="lvl" select="count(ancestor::html:div[@class='section' or @class='topic'])"/>
    <xsl:element name="h{$lvl}">
      <xsl:attribute name="class">
	<xsl:text>Heading_20_</xsl:text>
	<xsl:value-of select="$lvl"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="html:body">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      
      <div class="coverpage">
        <p> </p>
        <p> </p>
        <p> </p>
        
        <p class="coverimg">
          <img width="20" height="6" class="projectlib" src="logo-client.png"/>
        </p>
        <p> </p>
        <!--
            <p class="grandtitre"><xsl:value-of select="/html:html/html:head/html:meta[@name='DC.title']/@content"/></p>
        -->
        <p class="grandtitre"><xsl:value-of select="/html:html/html:head/html:title"/></p>
        <p class="coverimg">
          <img width="48" height="15" class="projectlib" src="beamline.png"/>
        </p>
        <p> </p>
        <p> </p>
        <p> </p>
        <p class="coverimg">
          <img width="12" height="4" class="projectlib" src="logo-elocus.png"/>
        </p>
      </div>
      
      <div class="TDM">
        <p class="TDM_titre">Table des matières</p>
      </div>
      <xsl:if test="html:div[@class='topic']">
        <div class="section">
          <h1>Introduction</h1>
          <xsl:apply-templates select="html:div[@class='topic']"/>
        </div>
      </xsl:if>
      <xsl:apply-templates select="html:div[@class='section']"/>
    </xsl:copy>
  </xsl:template>

  
  <!--
  <xsl:template match="html:div[@class='topic']">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="html:div[@class='topicinfo']"/>
      <xsl:apply-templates select="html:h1"/>
      <xsl:call-template name="chart"/>
      <xsl:apply-templates select="html:div[@class='details']"/>
      <xsl:apply-templates select="html:div[@class='analyse']"/>
    </xsl:copy>
  </xsl:template>
  -->

  <xsl:template match="html:div[@class='topic'][@data-hidden='yes']">
    <p class="info">Les données ne sont pas disponibles</p>
  </xsl:template>

  <xsl:template match="html:div[starts-with(@class,'kolekti-component-details')]"/>
  
  <xsl:template match="html:div[starts-with(@class,'kolekti-component-')][@data-hidden='yes']"/>
  
  <xsl:template match="html:div[@class='kolekti-component-chart'][not(@data-hidden)]">
    <p>
      <img width="43.18" height="22.70" src="{@data-hash}.png"/>
    </p>
  </xsl:template>
  
  <xsl:template match="html:div[@class='kolekti-component-svg'][not(@data-hidden)]">
    <p>
      <img width="43.18" height="32.43" src="{@data-hash}.png"/>
    </p>
  </xsl:template>
  
  <xsl:template match="html:div[@class='kolekti-component-map'][not(@data-hidden)]">
    <p>
      <img src="{@data-hash}.png" width="43.18" height="32.43"/>
    </p>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-navmap'][not(@data-hidden)]">
    <p>
      <img src="{@data-hash}.png" width="43.18" height="32.43"/>
    </p>
  </xsl:template>

  <xsl:template match="html:div/html:img">
    <p>
      <xsl:copy>
        <xsl:apply-templates select="@*"/>
      </xsl:copy>
    </p>
  </xsl:template>
  
  <xsl:template match="html:table/@class"/>
  
  <xsl:template match="html:span[@class='tplvalue']">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="html:span[@class='detaillabel']">
    <span class="label">
      <xsl:apply-templates/>
      <span class="tab"/>
    </span>
  </xsl:template>

  <xsl:template match="html:span[@class='detailvalue']">
    <span class="detailtext">
      <xsl:apply-templates/>
    </span>
  </xsl:template>


  <xsl:template match="html:p[@style='display:none']"/>

  <xsl:template match="html:div[@class='kolekti-component-wysiwyg']//html:a">
    <a class="wwwlink">
      <xsl:apply-templates select="node()|@*"/>
    </a>
  </xsl:template>


  <xsl:template match="html:div[@class='kolekti-component-wysiwyg']/html:p">
    <p class="analyse">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-wysiwyg']/html:blockquote/html:p">
    <p class="analysequote">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-wysiwyg']/html:ol">
    <ol class="Numbering_20_2">
      <xsl:apply-templates/>
    </ol>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-wysiwyg']/html:ul">
    <ul class="List_20_1">
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-wysiwyg']//html:li">
    <li>
      <p class="analyse">
	<xsl:apply-templates/>
      </p>
    </li>
  </xsl:template>
  
  <xsl:template match="html:div[@class='kolekti-component-description']//html:a">
    <a class="wwwlink">
      <xsl:apply-templates select="node()|@*"/>
    </a>
  </xsl:template>


  <xsl:template match="html:div[@class='kolekti-component-description']/html:p">
    <p class="analyse">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-description']/html:blockquote/html:p">
    <p class="analysequote">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-description']/html:ol">
    <ol class="Numbering_20_2">
      <xsl:apply-templates/>
    </ol>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-description']/html:ul">
    <ul class="List_20_1">
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-description']//html:li">
    <li>
      <p class="analyse">
	<xsl:apply-templates/>
      </p>
    </li>
  </xsl:template>
  
  <xsl:template match="html:div[@class='kolekti-sparql']">
    <xsl:apply-templates select=".//*[@class='kolekti-sparql-result-template']"/>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-html']//html:div[@class='kolekti-sparql']">
    <xsl:apply-templates select=".//*[@class='kolekti-sparql-result-template']/node()"/>
  </xsl:template>

      
</xsl:stylesheet>
