import os
import shutil
import logging

from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase

from .models import Template, Project

# Create your tests here.
class ProjectMethodTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test', email='test@elocus.fr', password='top_secret')
        Template.objects.create(
            name="elocus_test",
            description="elocus tests",
            svn="https://dev.elocus.fr/svn/elocus/test_template",
            svn_user = "test",
            svn_pass = "top_secret",
        )

    def tearDown(self):
        shutil.rmtree('/svn/elocus_tests')
        
    def test_project_create(self):
        logging.debug('test project create')
        name = "test"
        directory = "test"
        template = Template.objects.get(name = 'elocus_test')
        p = Project(name = name, directory = directory, owner = self.user, template = template)
        
        assert (os.path.exists('/svn/test'))

    
