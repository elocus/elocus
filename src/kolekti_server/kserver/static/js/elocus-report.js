var ajaxBeforeSend = function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
} ;

$.ajaxSetup({
    beforeSend: ajaxBeforeSend
});


window.jump = function(delay) {
    // open the accordion corresponding to urls hash, and scroll

    if (!delay)
	delay = 1000;
    var pageUrl = window.location.href;
    var hasEltIdInPageUrl = pageUrl.indexOf("#");
    if (hasEltIdInPageUrl != -1) {
	var eltIdInPageUrl = pageUrl.split('#')[1];
	if(eltIdInPageUrl.length) {
	    var parent = $("#collapse_section_"+eltIdInPageUrl).closest('panel-group').attr('id')
	    $('#'+eltIdInPageUrl).closest('.panel-group').find('.section-content.collapse.in').collapse('hide')

	    $('#'+eltIdInPageUrl).closest(".section-content.collapse").collapse('show');

	    $('#'+eltIdInPageUrl).find(".section-content.collapse").collapse('show');


	    setTimeout(function () {
		$('html, body').animate({
		    scrollTop: $("#"+eltIdInPageUrl).offset().top - 60
		}, 1);
	    }, delay);
	}
    }
}

$(document).ajaxStop(function() {
    if($('body').data('loopguard')) {
	return;
    }
    jump(1500)
    $('body').data('loopguard', true)

 })


$(function() {
    var project = $('body').data('project')
    var report = $('body').data('report')



    // collapse : close open collapse when an otherone is open
    $(".collapseTopic").on('show.bs.collapse', function() {
	console.log('shown collase')
	var current = $(this).attr('class')
//	$(this).closest('.topicCollapses').find('.collapseTopic').removeClass('in')
	$('body').trigger('redraw.elocus.topics', $(this).attr('id'));
   })

    //collapse : highlight button
    /*
    $(".elocus-action-collapse").on('click', function() {
	$(this).closest('.topicCollapses').find('.elocus-action-collapse').removeClass('active')
	$(this).addClass('active')
    })
    **/

    // actions

    // Creation nouveau rapport

    var ref_parameters = {}

    var get_ref_communes = function() {
	var ref = $("#elocus_select_referentiel").val();
	if (!ref_communes.hasOwnProperty(ref))
	    $.get('/elocus/'+project+'/communes/',{'referentiel':ref})
	    .done(function(data) {
		ref_communes[ref] = data;
	    })
    }

    var get_ref_parameters = function() {
	var ref = $("#elocus_select_referentiel").val();
	if (!ref_parameters.hasOwnProperty(ref)) {
	    $('#create_form_parameters').html('');
	    $("#create_form_parameters_loading").show();
	    $.get('/elocus/'+project+'/refparameters/',{'referentiel':ref})
		.done(function(data) {
		    ref_parameters[ref] = data;
		    build_create_fields();
		})
		.always(function() {
		    $("#create_form_parameters_loading").hide();
		});
	}
	else
	    build_create_fields();

    }

    var check_selected_parameters = function() {
	console.log('check_selected_parameters')
	var ref = $("#elocus_select_referentiel").val();
	var parameters =  {};
	// TODO get parameter in menus
	$.map(ref_parameters[ref], function(v) {
	    if ($('select#'+v.id).length) {
		var val = {'name':$('#'+v.id).attr('name'),
			   'id':$('#'+v.id).val()
			  }
		parameters['uservar_'+v.id] = val;
	    } else {
		var val = $('#'+v.id).typeahead("getActive")
		if (typeof val != 'undefined') parameters['uservar_'+v.id] = val;
	    }
	})
	return Object.keys(parameters).length > 0;
    }

    var get_selected_parameters = function() {
	var ref = $("#elocus_select_referentiel").val();
	var parameters =  {};
	// TODO get parameter in menus

	$.map(ref_parameters[ref], function(v) {
	    if ($('select#'+v.id).length) {
		var val = {'name':$('#'+v.id).attr('name'),
			   'id':$('#'+v.id).val()
			  }
		parameters['uservar_'+v.id] = val;
	    } else {
		var val = $('#'+v.id).typeahead("getActive")
		if (typeof val == 'undefined')
		    parameters['uservar_'+v.id] = {'id':'',name:''};
		else
		    parameters['uservar_'+v.id] = val;
	    }
	})
	return parameters;
    }

    var build_create_fields = function() {
	var ref = $("#elocus_select_referentiel").val();
	$('#create_form_parameters').html('')
	$.map(ref_parameters[ref], function(v) {
	    switch (v.type) {
	    case "menu":
		var input = $('<select>',{
		    'name':v.id,
		    'id':v.id,
		    'type':"text",
		    'class':"form-control",
		    'html': $.map(v.values, function(item) {
			return $('<option>', {
			    "value":item.id,
			    "html":item.name
			})
		    })
		})
		$('<div>', {
		    'class':"form-group",
		    'html':[
			$('<label>', {
			    'for':v.id,
			    'class':"col-sm-2 control-label",
			    'html':v.label}),
			$('<div>',{
			    'class':"col-sm-10",
			    'html':input
			})
		    ]
		}).appendTo('#create_form_parameters')
		break;
	    default:
		var input = $('<input>',{
		    'name':v.id,
		    'id':v.id,
		    'type':"text",
		    'class':"form-control typeahead",
		'data-provide':"typeahead"})

		$('<div>', {
		    'class':"form-group",
		    'html':[
			$('<label>', {
			    'for':v.id,
			    'class':"col-sm-2 control-label",
			    'html':v.label}),
			$('<div>',{
			    'class':"col-sm-10",
			    'html':input
			})
		    ]
		}).appendTo('#create_form_parameters')

		input.typeahead({source:function(query, process) {
		    return process(v.values)
		}})
		break;
	    }
	})
    }

    $('#elocus_select_referentiel').on('change', function() {
	$('.typeahead').val('')
	get_ref_parameters();
    });

    // typeahead

    var reset_errors = function() {
	$("#create_form_parameters_loading").hide();
	$("#create_form_parameters_error").hide();
	$("#create_form_parameters").removeClass("has-error")
	$(".form-group").removeClass('has-error');
	$("#create_title_field span.help-block").remove();
    }

    $('#modal_create').on('show.bs.modal', function () {
	reset_errors();
	$('#titre_rapport').val('')
    })

    $('#modal_create').on('hidden.bs.modal', function () {
	window.history.pushState("eLocus", "eLocus", "/elocus/");
    })


    $('#modal_create').on('shown.bs.modal', function (e) {
	// recupere la liste des referentiels
	$.get('/elocus/'+project+'/referentiels/')
	    .done(function(data) {
		$('#elocus_select_referentiel').find('option').remove()
		$(data).each(function(i,v) {
		    $('#elocus_select_referentiel').append(
			$('<option>',{'value':v, 'html':v.replace(new RegExp('\.x?html$'),'')})
		    );
		});
		get_ref_parameters();
	    });
	$('.typeahead').each(function(){
	    $(this).val('')
	});
	$('#elocus_select_referentiel').focus();
    });

    $(".elocus-action-create-report-project").on('click', function() {
	var dialog = $(this).data('target');
	project_url = '/projects/activate?project='+project;
	$.get(project_url)
	    .done(function(data) {
		$(dialog).modal();
		$(dialog).trigger('shown.bs.modal');
	    });
    })


    $('#modal_create_ok').on('click', function () {
	reset_errors();
	var referentiel = $("#elocus_select_referentiel").val();
	var title = $('#titre_rapport').val()
	if (title =="") {
	    $("#create_title_field").addClass('has-error')
	    $("#create_title_field").find('input').after($('<span>',{
		"class":"help-block",
		"html":'Ce champ est obligatoire'}))
	    return;
	}

	if(!check_selected_parameters()) {
	    $("#create_form_parameters_error").show();
	    $("#create_form_parameters .form-group").addClass('has-error');
	    return
	}

	var params = get_selected_parameters();

	params['title'] = title;
	params['toc'] = referentiel;

	$('#modal_create').modal('hide');
	$('#modal_create_processing').modal('show');
	$.ajax({
	    url:"/elocus/"+project+"/create/",
	    method:'POST',
	    data:$.param(params)
	}).done(function(data) {
	    $('#modal_create_processing').modal('hide');
	    if (data.status == 'ok') {
		var url = window.location.origin + '/elocus/'+ project +"/" + data.events[0].releasename +'/'
		window.location.replace(url)
	    } else if (data.status == 'timeout') {
		$('#modal_notif')
		    .find('.notiftitle').html("Génération du rapport");
		$('#modal_notif')
		    .find('.notifmessage').html("La connexion a été interrompue avant que le rapport ne soit completement généré.<br/>L'opération se poursuit en arrière plan, le résultat vous sera transmis par email.")
		$('#modal_notif').modal('show');
	    } else {
		$('#modal_error')
		    .find('.errortitle').html("Une erreur s'est produite lors de la génération du rapport");
		$('#modal_error')
		    .find('.errormessage').html(data.msg)
		$('#modal_error').modal('show');
	    }
/*
	    if (data.length) {
		var url = window.location.origin + '/elocus/report/?release=/releases/' + data[0].releasename
		window.location.replace(url)
	    }
*/
	}).fail(function(data) {
	    $('#modal_create_processing').hide()
	    $('#modal_error')
		.find('.errortitle').html("Une erreur s'est produite lors de la génération du rapport");
	    $('#modal_error')
		.find('.errormessage').html("Coupure de la communication avec le serveur");
	    $('#modal_error').modal('show');
	}).always(function() {
	    window.history.pushState("eLocus", "eLocus", "/elocus/");
	});
    })




    // Action globales sur le rapport
    // Actualisation des données
    $('.elocus-action-update-data').on('click', function() {

	var report = $('body').data('report')
	$('#modal_update_processing').modal('show')
	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/update/",
	    method:'POST',
	    data:$.param({
	    })
	}).done(function(data) {
	    $('#modal_update_processing').modal('hide');
	    if (data.status == 'ok') {
		window.location.reload(true)
	    } else if (data.status == 'timeout') {
		$('#modal_notif')
		    .find('.notiftitle').html("Actualisation des données");
		$('#modal_notif')
		    .find('.notifmessage').html("La connexion a été interrompue avant que les données ne soient entièrement actualisées.<br/>L'opération se poursuit en arrière plan, le résultat vous sera transmis par email.")
		$('#modal_notif').modal('show');
	    } else {
		$('#modal_error')
		    .find('.errortitle').html("Une erreur s'est produite lors de l'actualisation des données");
		$('#modal_error')
		    .find('.errormessage').html(data.msg)
		$('#modal_error').modal('show');
	    }
	}).fail(function(data) {
	    $('#modal_update_processing').modal('hide')
	    $('#modal_error')
		.find('.errortitle').html("Une erreur s'est produite lors de l'actualisation des données");
	    $('#modal_error')
		.find('.errormessage').html("Coupure de la communication avec le serveur");
	    $('#modal_error').modal('show');
	});
    });

    var generate_document = function(kind) {
	$("#modal_export_processing").modal("show")
	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/publish/",
	    method:'POST',
	    data:$.param({
		    'script': 'odt',
            'document_type': kind
	    })
	}).done(function(data) {
	    if (data.status == 'ok') {
		console.log(data.events)
		$.each(data.events, function(i,v) {
		    console.log(v)
		    if (v.event == 'result')
			window.location.replace('/elocus/'+ project+'/' + report + '/' + kind + '/')
		});
	    } else if (data.status == 'timeout') {
		$('#modal_notif')
		    .find('.notiftitle').html("Génération du rapport");
		$('#modal_notif')
		    .find('.notifmessage').html("La connexion a été interrompue avant que le document ne soit completement généré.<br/>L'opération se poursuit en arrière plan, le résultat vous sera transmis par email.")
		$('#modal_notif').modal('show');
	    }
	}).fail(function(data) {
	    $('#modal_update_processing').modal('hide')
	    $('#modal_error')
		.find('.errortitle').html("Une erreur s'est produite lors de la génération du document");
	    $('#modal_error')
		.find('.errormessage').html("Coupure de la communication avec le serveur");
	    $('#modal_error').modal('show');
	}).always(function() {
	    $("#modal_export_processing").modal("hide")
	})
    }

    // Téléchargement (publication kolekti)
    $('.elocus-action-dl-pdf').on('click', function() {
        generate_document('pdf')
    })

    $('.elocus-action-dl-word').on('click', function() {
        generate_document('doc')
    })

    $('.elocus-action-dl-odt').on('click', function() {
        generate_document('odt')
    })

    $('.elocus-action-dl-presentation').on('click', function() {
	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/publish/",
	    method:'POST',
	    data:$.param({
		'script': 'ppt'
	    })
	}).done(function(data) {
	}).fail(function(data) {
	});

    })
    $('.elocus-action-dl-html').on('click', function() {
	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/sharelink/",
	    method:'GET',
	    data:$.param({
	    })
	}).done(function(data) {
	    $("#modal_share #elocus_share_url").val(data.url);
	    $("#modal_share #elocus_share_link").attr('href',data.url);
	    $("#modal_share").modal("show")
	}).fail(function(data) {
	});

    })

    // partage de rapport
    $('.elocus-action-report-share').on('click', function() {
	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/share/",
	    method:'GET',
	    data:$.param({
	    })
	}).done(function(data) {
	    window.location.reload(true)
	}).fail(function(data) {
	    window.location.reload(true)
	});

    })

    // suppression de rapport
    $('.elocus-action-report-delete').on('click', function() {
	if (confirm('Etes vous sur de vouloir supprimer ce rapport ?')) {
	    $.ajax({
		url:"/elocus/"+project+"/"+report+"/delete/",
		method:'POST',
	    }).done(function(data) {
                console.log(data.status)
                if (data.status == 'ok')
                    window.location.href = "/"
	    }).fail(function(data) {
//		window.location.reload(true)
	    });
	}

    })


    // Actions sur les indicateurs
    // A la une (star)
    $('.elocus-action-star').on('click', function() {
	var topic = $(this).closest('.topic')
	var state = !$(this).hasClass('btn-warning')
	var btn = $(this)
	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/star/",
	    method:'POST',
	    data:$.param({
		    'topic': topic.attr('id'),
		    'state': state
		})
	}).done(function(data) {

	    if (data.status == 'ok') {
		if (state) {
		    btn.addClass('btn-warning')
		    btn.removeClass('btn-default')
		} else {
		    btn.addClass('btn-default')
		    btn.removeClass('btn-warning')
		}
	    }
	}).fail(function(data) {
	});
    })

    // masquer
    $('.elocus-action-hide').on('click', function() {
	var topic = $(this).closest('.topic')
	var state = !$(this).hasClass('ishidden')
	var btn = $(this)
	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/hide/",
	    method:'POST',
	    data:$.param({
		'topic': topic.attr('id'),
		'state': state
	    })
	}).done(function(data) {

	    if (data.status == 'ok') {
		if (state) {
		    topic.addClass("disabled")
		    btn.addClass('ishidden')
		    btn.removeClass('btn-default')
		    btn.addClass('btn-primary')
		} else {
		    topic.removeClass("disabled")
		    btn.removeClass('ishidden')
		    btn.addClass('btn-default')
		    btn.removeClass('btn-primary')
		}
	    }
	}).fail(function(data) {
	});
    })

    // bouton détail d'un topic

    $('.elocus-action-showdetails').on('click', function() {
	var topic = $(this).closest('.topic');
	$(topic).addClass('edition')

	var modal = $(topic).find('.modal-topic-details');
	modal.modal()

    });

    $('.modal-topic-details-ok').on('click', function(e) {
	console.log('modal ok')
	var modal = $(this).closest('.modal');
	modal.data('elocus_params', {})
	modal.trigger('confirm.bs.modal');

	$.ajax({
	    url:"/elocus/"+project+"/"+report+"/topic/save/",
	    method:'POST',
	    data:$.param(modal.data('elocus_params'))
	}).done(function(data) {
	    console.log('topic post done')
	    if (data.status == 'ok') {
		modal.trigger('confirmed.bs.modal');
		console.log('topic post ok')
	    } else {
		console.log(data)
	    }
	}).fail(function(data) {
	    console.log('chart post fail')
	});

	modal.modal('hide');
    });

})
