
var navmap_options = {
    "chartkind":"bar",
    "windowwidth":null,
    "show_icon":true,
    "bar_as_icon":false,
    "anim":true,
    "tooltip":true,
    'phantom':false   // est ce que la fonction est appellée dans le contexte du navigateur ou de phantomjs ?

}


var display_navmap = function (navmap, options) {
    // redisplay one map

    var co = {};
    if ($(navmap).attr("data-chartopts")) {
        co = $(navmap).data("chartopts")
    }

    var opts = $.extend({}, navmap_options, co, options);
    if (opts.phantom)
        window.callPhantom( JSON.stringify(opts))

    var map_container = $(navmap).find('.map-container').get(0)
    if ($(navmap).find('.map-layer').length == 0) {
        $(map_container).html('<div class="alert alert-info">No data for thiis map</div>')
        return
    }
    $(map_container).html('')
    var width = $(map_container).width();
    var height = width;
    var wh = $(window).height() * .8;
    if (height > wh)
        var height = wh;

    var fr = d3.formatLocale(locale_fr);

    // calculate largest bound for all items
    var geobounds = null;

    var addbounds = function(feature) {
        var gb = d3.geoBounds(feature)
        if(!geobounds) {
            geobounds = gb
        } else {
            if (gb[0][0] < geobounds[0][0])
                geobounds[0][0] = gb[0][0]

            if (gb[0][1] < geobounds[0][1])
                geobounds[0][1] = gb[0][1]

            if (gb[1][0] > geobounds[1][0])
                geobounds[1][0] = gb[1][0]

            if (gb[1][1] > geobounds[1][1])
                geobounds[1][1] = gb[1][1]
        }
    }
    var nblabels;
    // collect all data in map-layer elements, build a geojson property list
    var features = $(navmap).find('.map-layer').map(function() {
        nblabels = 0;
        var feature = {}
        feature['type'] = 'Feature';
        feature['properties'] = {};
        var l = $(this).data('layer').results.bindings[0]
        feature['properties']['link'] = $(this).data('link')
        if (l.hasOwnProperty('geojson')) {
            var geojson = l.geojson.value
            feature['geometry'] =  JSON.parse(geojson)
        }
        if (l.hasOwnProperty('wkt')) {
            var wkt = l.wkt.value;
            var wicket = new Wkt.Wkt(wkt);
            var jsondata = wicket.toJson();
            feature['geometry']  = jsondata;
        }
        feature['properties']['labels']=[]
        feature['properties']['overlabels']=[]

        $.each(l, function(i,v) {
            if (i.startsWith('label')) {
                feature['properties']['labels'].push({'id':i, 'value':v.value});
                nblabels++;
            }
            if (i.startsWith('overLabel')) {
                feature['properties']['overlabels'].push({'id':i, 'value':v.value});
            }
        });
        if (l.hasOwnProperty('color')) {
            feature['properties']['color'] = l.color.value
        } else {
            feature['properties']['color'] = ".5"
        }
        addbounds(feature)
        return feature
    }).get()

    // console.log(features)

    // create a fetaure with the globale bounds collected
    var geoboundsfeature ={"type":'Feature', 'geometry': {"type":"LineString","coordinates":geobounds}, 'properties':null}

    // get a first projection on france scale to get an approx of the scale
    var center = d3.geoCentroid(geoboundsfeature)
    var scale = 2600;
    var projection_guess = d3.geoConicConformal()
        .center(center)
        .scale(scale)
        .translate([width / 2, height / 2]);

    var path_guess = d3.geoPath().projection(projection_guess)
    var bounds = path_guess.bounds(geoboundsfeature)

    // calculate scale

    var hscale  = scale*width  / (bounds[1][0] - bounds[0][0]);
    var vscale  = scale*height / (bounds[1][1] - bounds[0][1]);
    var scale   = 0.9 * ((hscale < vscale) ? hscale : vscale);
    var offset  = [width - (bounds[0][0] + bounds[1][0])/2,
                   height - (bounds[0][1] + bounds[1][1])/2];

    // create effective path generator

    var projection = d3.geoConicConformal()
        .center(center)
        .scale(scale)
        .translate(offset);

    var path = d3.geoPath().projection(projection);

    // creates
    var svg = d3.select(map_container).append("svg")
        .attr("id", "svg")
        .attr("width", width)
        .attr("height", height);

    var deps = svg.append("g")
        .selectAll("g")
        .data(features)
        .enter();

    var dept = deps.append('g')
    dept.append('path')
        .attr("d",
              function(v,i) {
                  // console.log(v.geometry,i)
                  return path(v)
              })
        .attr("class", "elocus-geonav")
        .style('fill',function(v,i) {
            // console.log(features[i])
            return d3.interpolateRdYlGn(features[i].properties['color']);
        })
        .attr('data-link', function(v,i) {
            return features[i].properties['link']
        })
        .on('click', function(v,i) {
            var link = features[i].properties['link']
            if(link) {
                window.location.href = link
                jump();
            }

        })
        .on("mouseover", function(d) {
            d3.select(this).transition()
                .duration(50)
                .style("opacity", 1);
            d3.select(this.parentNode).selectAll(".elocus-navmap-overlabel").transition()
                .duration(50)
                .style("opacity",1);
        })
        .on("mouseout", function(d) {
            d3.select(this).transition()
                .duration(100)
                .style("opacity", .75);
            d3.select(this.parentNode).selectAll(".elocus-navmap-overlabel").transition()
                .duration(50)
                .style("opacity",0);
        })

    var labels = dept.append('g')
        .attr("transform", function(d) {
            // console.log("transform", d)
            return "translate(" + projection(d3.geoCentroid(d)) + ")";
        })




    labels.selectAll('text')
        .data(function(x) {
            return x.properties.labels;
        })
        .enter()
        .append('text')
        .attr("class", function(d, i) { return "elocus-navmap-label "+d.id.toString() })
        .attr("dy",function (d, i) { return "" +  1.2 * (.5 + i - (nblabels/2)) + "em"})
        .attr("text-anchor","middle")
        .text(function(d, i) {
            /*if($.isNumeric(d.value)) return fr.format(',.0f')(d.value.toString());
            return d.value.toString();*/
            //return string with formatted numbers
            var content = d.value.toString();
            var numbers = content.match(/\d+/g); // 123456
            if(numbers) {
                numbers.forEach(function(number) {
                    var formatedNumber = fr.format(',.0f')(number);
                    content = content.replace(number, formatedNumber);
                });
            }
            return content;
        })
        .on('click', function(v,i) {
            var link = d3.select(this.parentNode.parentNode).select('path').attr('data-link')
            if(link) {
                window.location.href = link
                jump()
            }
        })
        .on("mouseover", function(d) {
            d3.select(this.parentNode.parentNode).select('path').transition()
                .duration(50)
                .style("opacity", 1);
            d3.select(this.parentNode.parentNode).selectAll(".elocus-navmap-overlabel").transition()
                .duration(50)
                .style("opacity",1);
        })
        .on("mouseout", function(d) {
            d3.select(this.parentNode.parentNode).select('path').transition()
                .duration(100)
                .style("opacity", .75);
            d3.select(this.parentNode.parentNode).selectAll(".elocus-navmap-overlabel").transition()
                .duration(50)
                .style("opacity",0);
        })





    /** /var overlabels = dept.append("foreignObject")
        //.attr("class", "overLabelsWrapper")
        .attr("width", 480)
        .attr("height", 500)
        .append("xhtml:body")
            .append("div")
            .attr("class", "overLabelsWrapper")
            //.html("<div class=\"overLabelsWapper\"></div>");

    overlabels.selectAll("body div")
        .data(function(x) {return x.properties.overlabels })
        .enter()
        .append('p')
        .attr("class", function(d, i) { return "elocus-navmap-overlabel "+d.id.toString() })
        //.attr("dy",function (d, i) { return "" +  (1 + (1.2 * i )) + "em"})
        //.attr("dx", "2em")
        //.attr("text-anchor","left")
        .text(function(d, i) {
            //return string with formatted numbers
            var content = d.value.toString();
            var numbers = content.match(/\d+/g); // 123456
            if(numbers) {
                numbers.forEach(function(number) {
                    var formatedNumber = fr.format(',.0f')(number);
                    content = content.replace(number, formatedNumber);
                });
            }
            return content;
        })
        .on('click', function(v,i) {
            var link = features[i].properties['link']
            if(link) {
                window.location.href = link
                jump()
            }
        })*/

    /**/var overlabels = dept.append('g')
                .attr("class", "overLabelsWrapper")

    overlabels.selectAll('text')
        .data(function(x) {return x.properties.overlabels })
        .enter()
        .append('text')
        .attr("class", function(d, i) { return "elocus-navmap-overlabel "+d.id.toString() })
        .attr("dy",function (d, i) { return "" +  (1 + (1.2 * i )) + "em"})
        .attr("dx", "2em")
        .attr("text-anchor","left")
        .text(function(d, i) {
            //return string with formatted numbers
            var content = d.value.toString();
            var numbers = content.match(/\d+/g); // 123456
            if(numbers) {
                numbers.forEach(function(number) {
                    var formatedNumber = fr.format(',.0f')(number);
                    content = content.replace(number, formatedNumber);
                });
            }
            return content;
        })
        .on('click', function(v,i) {
            var link = features[i].properties['link']
            if(link) {
                window.location.href = link
                jump()
            }
        })//*/

}; // display
