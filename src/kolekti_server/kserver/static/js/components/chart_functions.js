
var chartoptions = {
    "chartkind":"bar",
    "windowwidth":null,
    "show_icon":true,
    "bar_as_icon":false,
    "anim":true,
    "tooltip":true,
    'phantom':false   // est ce que la fonction est appellée dans le contexte du navigateur ou de phantomjs ?

}

 
var compatibility_item = function(item, vars) {
    newitem = {
        
        "indicateurURI":"",
        "indicateurLabel":"",
        "xapprox":item.dim0Value,
        "valueURI":item.dim0UnitURI ,
        "valueLabel":item.dim0UnitLabel,
        
        "placeURI":item.dim1URI,
        "placeLabel":item.dim1Label,
        
        "year":item.dim2Label
    }

    if (vars.hasOwnProperty('xDisplay')) {
        newitem['xDisplay'] = item['dim0Label']
    }
    
    if (vars.hasOwnProperty('icon')) {
        newitem['icon'] = item['configIcon']
    }
    if (vars.hasOwnProperty('xRatio')) {
        newitem['xRatio'] = item['dim0UnitRatio'],
        newitem['valueLabelR'] = item['dim0UnitLabelR']
    }
    if (vars.hasOwnProperty('chartType')) {
        newitem['chartType'] = item['dim0ChartType']
    }
    
    return newitem;
}

var compatibility = function(dataset) {
    // convertit les données au format commune / year en series
    // console.log("old", dataset)
    var vars = dataset['head']['vars']; 
    if (vars.indexOf('dim0Value') != -1) {        
        var newdataset = {
            "head" : {
                "vars" : [
                    "indicateurURI",
                    "indicateurLabel",
                    "xapprox",
                    "valueURI",
                    "valueLabel",
                    "placeURI",
                    "placeLabel",
                    "year"
                    ]
            },
            "results" : {
                "bindings" : []
            }
        };
        var newvars = newdataset['head']['vars']; 
        if (vars.indexOf('config_icon') != -1) {
            newvars.push('icon')
        }
        if (vars.indexOf('config_chart_type') != -1) {
            newvars.push('chartType')
        }
        if (vars.indexOf('dim0Label') != -1) {
            newvars.push("xDisplay")
        }
        if (vars.indexOf('config_xRatio') != -1) {
            newvars.push('valueLabelR'),
            newvars.push('XRatio')
        }

        var series = dataset['results']['bindings'];
        var newbind = newdataset['results']['bindings'];

        for (i=0; i < series.length; i++) {
            newbind.push(compatibility_item(series[i], vars));
        }
        // retourne le dataset converti
//        console.log("new", newdataset)
        return newdataset
        
   }
    // retourne le dataset original
    return dataset
}


var pictosize = 48;
var gradcount = 0;
var maskcount = 0;

var drawchart = function(elt, options) {
    //  console.log("draw chart");
    var co = {};
    if ($(elt).attr("data-chartopts")) {
        co = $(elt).data("chartopts")
    }

    var opts = $.extend({}, chartoptions, co, options);
    // console.log(opts)
    if (opts.phantom)
    window.callPhantom( JSON.stringify(opts))

    /*
    if ($(elt).attr("data-chartkind")=="bar")
    opts.chartkind = "bar";
    if ($(elt).attr("data-chartkind")=="line")
    opts.chartkind = "line";
    */

    // récupère data dans le dom (bindings = liaison entre les requetes et les réponses)

    var chartdata = compatibility($(elt).data('chartdata'))
    var data = chartdata['results']['bindings'];
    var datavars = chartdata['head']['vars'];
    var has_charttype = (datavars.indexOf('chartType') != -1)
    var hasRightYAxis = (datavars.indexOf('xRatio') != -1)
    var fr = d3.formatLocale(locale_fr);

    // on réorganise data en classant par année...
    var by_year = d3.nest()
    .key(function(d) { return d.year.value; })
    .sortKeys(d3.ascending)
    .entries(data);


    // used for legends, linecharts
    // ... et par lieu
    var by_place = d3.nest()
    .key(function(d) { return d.placeURI.value; }).sortKeys(d3.ascending)
    .sortValues(function(a,b) {
        return b.year.value < a.year.value ? -1 : b.year.value > a.year.value ? 1 : b.year.value >= a.year.value ? 0 :NaN
    })
    .entries(data);

    // liste des labels de lieux (noms)
    var places = by_place.map(function(item) {
        return item.values[0].placeLabel.value;
    })


    var places_histo = by_place.filter(function(item, indice) {
        if (! has_charttype)
            return true;
        return item.values[0].chartType.value == "bar";
    }).map(function(item) {
        return item.values[0].placeLabel.value;
    })

    // calcul des dimensions du svg pour cet élément
    var wwidth = opts.windowwidth?opts.windowwidth:$(elt).width();
    /*
    if ($('body').width() > 980)
    wwidth = wwidth/ 3 * 2;
    */
    var wheight = wwidth / 2;
    /*
    if (wheight > 400)
    wheight = 400;
    */

    // calculate image ratio for margins, space between elements, lagend size...
    // linear but should use logarithmic scale

    var ird = function(v) {
        return v * wwidth / 600
    }
    var ir = function(v) {
        return Math.round(ird(v))
    }

    var logir = function(v) {
        if (opts.phantom) {
            window.callPhantom("logir " + v )
            window.callPhantom(Math.log(wwidth))
            return v * Math.log(wwidth/ 600) / Math.log(1.8);
        }
        return v * Math.log2(wwidth / 600)
        //
    }


    var margin;
    if (opts.chartkind == "icons") {
        margin = {top: ir(10), right: 0, bottom: ir(10), left: 0};
    } else {
        margin = {top: ir(30), right: ir(hasRightYAxis?80:20), bottom: ir(80)+(ir(20)*places.length), left: ir(80)};
    }
    var width = wwidth - margin.left - margin.right,
    height = wheight - margin.top - margin.bottom;

    //    var fontratio = logir(48);
    var fontratio = ir(18);

    // console.log("fontratio " + fontratio)

    // Parse the date / time

    // var parseDate = d3.time.format("%Y-%m").parse;

    // crée une echelle linéaire pour les ordonnées dans le domaine adéquate (une pour l'axe gauce : y, une pour l'axe droit (y2)
    var y = d3.scaleLinear().range([height, 0]);
    var y2 = d3.scaleLinear().range([height, 0]);
    var datadomains = data.slice(0)
    datadomains.push({"xapprox":{"value":0}, "xRatio":{"value":0}}) // ajouté 0 comme valeur arbitraire pour l'inclure dans le domaine

    y.domain([
        d3.min(datadomains, function(d) {
            return parseFloat(d.xapprox.value);
        }),
        d3.max(datadomains, function(d) {
            return parseFloat(d.xapprox.value);
        })
    ]);

    if (hasRightYAxis)
    y2.domain([
        d3.min(datadomains, function(d) {
            return parseFloat(d.xapprox.value) * parseFloat(d.xRatio.value);
        }),
        d3.max(datadomains, function(d) {
            return parseFloat(d.xapprox.value) * parseFloat(d.xRatio.value);
        })
    ]);

    // définit le roulement des couleurs, le mieux serait d'assigner des calsses et de gérer la colorisation en css
    var seriescolor = d3.scaleOrdinal()
    .range(["#00B1D8", "#1E4B63", "#F3652A", "#424242", "#a8282e", "#184931"]);

    // association des couleurs avec les lieux
    var colorscale = d3.scaleBand()
    .domain(by_place.map(function(d) {
        return d.key;
    }))
    .rangeRound([0, places.length]);

    // console.log("chart w: " + width + " h: " + height);


    // crée élément svg dans component avec un groupe g
    var chart = d3.select(elt).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .style('font-size', fontratio + 'px')

    var axes = function() {
        // calcul le nombre de graduation sur l'axe y
        var yticks = (height < 60)?2:(height < 130)?5:10;

        // définit l'axe des ordonnées
        var yAxis = d3.axisLeft(y)
        .ticks(yticks)
        .tickFormat(fr.format(',.0f'));

        var yAxis2 = d3.axisRight(y2)
        .ticks(yticks)
        .tickFormat(fr.format(',.0f'));

        // crée la ligne de l'axe des 0 sur l'ensemble de la largeur dispo
        chart.append('line')
        .attr("class", "zeroaxis")
        .attr("x1", 0)
        .attr("x2", width)
        .attr("y1", function() {return y(0)})
        .attr("y2", function() {return y(0)})

        // crée l'axe des ordonnées
        chart.append("g")
            .attr("class", "y axis")
            .style("font-size", (.4 * fontratio) + 'pt')
            .call(yAxis)
        
        .selectAll('text')

        // crée l'axe des ordonnées (droite)
        if (hasRightYAxis)
        chart.append("g")
        .attr("class", "y axis right")
        .attr("transform", "translate("+ width +",0)")
        .call(yAxis2)
        .selectAll('text')

        //	    .style('font-size','60%')

        // legende en haut de l'axe gauche
        chart.append("text")
        .attr("y", ir(-10))
        .attr("x", ir(-10))
        .text(data[0].valueLabel.value)
        .style('font-size','80%')
        .style('font-weight','bold')
        .style("text-anchor", "left");

        // legende en haut de l'axe droite
        if (hasRightYAxis)
        chart.append("text")
        .attr("y", ir(-10))
        .attr("x", width - ir(50))
        .text(data[0].valueLabelR.value)
        .style('font-size','80%')
        .style('font-weight','bold')
        .style("text-anchor", "right");

    }

    // pour construction graphique en barre
    var barchart = function() {
         // division par section principale (en général, les années)
        var x0 = d3.scaleBand()
        .padding(ird(.1))
        .paddingInner(.1)
        .rangeRound([0, width]);

        // division de chaque année en "places"
        var x1 = d3.scaleBand()

        // ligne pour la seconde série de valeurs
        var area = d3.line()
        .curve(d3.curveLinear)
        .x(function(d) {
            return x0(d.year.value) + (x0.bandwidth() / 2);
        })
        .y(function(d) {
            return y(d.xapprox.value);
        });

        // Créé le domaine des annéesq
        x0.domain(by_year.map(function(d) {
            return d.key;
        }));

        // divise l'espace dispo pour une année par le nombre de places
        x1.domain(places_histo).rangeRound([0, x0.bandwidth()]);

        // défini l'axe des abscisses
        var xAxis = d3.axisBottom(x0)

        // dessine l'axe des abscisses
        chart.append("g")
        .attr("class", "x axis hiddenaxis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .style('font-weight','bold')
        .style('text-anchor','end')
        .style('font-size', (.4 * fontratio) + 'pt')
        //	    .attr("dy", ir(12))
        //	    .attr("dx", ir(-25))
        .attr("transform", "rotate(-30)")

        // créé le groupe permettant d'afficher les barres par années
        var year = chart.selectAll(".year")
        .data(by_year)
        .enter()
        .append("g")
        .attr("class", "year")
        .attr("transform", function(d) {
            // décalage pour position x initiale du groupe
            return "translate(" + x0(d.key) + ",0)";
        })



        var rect = year.selectAll("rect")
        .data(function(d) {
            return d.values.map(function(i) {
                var val =  {
                    'name':i.placeLabel.value,
                    'place':i.placeURI.value,
                    'value':i.xapprox.value
                };
                if (has_charttype)
                    val['type']=i.chartType.value
                return val
            });
        })
        .enter()
        .filter(function(item) {
            if (!has_charttype)
            return true;
            return item.type == "bar";
        })
        .append("rect")
        //.style("fill", function(d) { return seriescolor(colorscale(d.place)); })
        .attr("class", function(d) { return "fill color-" + colorscale(d.place); })
        .attr("width", x1.bandwidth())
        .attr("x", function(d) {
            return x1(d.name);// position de la barre
        })

        // debut ajout graphique mixtes barres / lignes
        // --------------------------------------------
        // si une série est definie en ligne (chartType == 'line' dans les données) : on cree une ligne avec les points...

        var place = chart.selectAll(".place")
        .data(by_place)
        .enter()
        .filter(function(item) {
            if (!has_charttype)
                return false;
            return item.values[0].chartType.value == "line";
        })
        .append("g")
        .attr("class", "place");

        place.append("path")
        .attr("d", function(d) {
            return area(d.values);
        })
        .attr("class", function(d) {
            return "area stroke color-" + colorscale(d.key);
        })

        // dots
        place.selectAll(".dot")
        .data(function(d){
            return d.values.map(function(i) {
                return {'year':i.year.value,'place':i.placeURI.value, 'value':i.xapprox.value}
            })
        })
        .enter()
        .append("circle")
        //.attr("class","dot")
        .attr("r", ird(3.5))
        .attr("cx", function(d) {
            return x0(d.year) + (x0.bandwidth()/2);
        })
        .attr("cy", function(d) { return y(d.value); })
        //.style("fill",function(d) { return seriescolor(colorscale(d.place)); })
        .attr("class", function(d) { return "dot fill color-" + colorscale(d.place); })

        // fin ajout graphique mixtes barres / lignes
        // --------------------------------------------

        // assignation de la hauteur du rectangle avec ou sans anim
        if (opts.anim)
        rect.attr("y", function(d) {return y(0)})
        .attr("height", function(d) { return 0})
        .transition()
        .duration(500)
        .attr("y", function(d) {
            return d3.min([y(d.value),y(0)]);
        })
        .attr("height", function(d) { return Math.abs(y(0) - y(d.value)); });
        else
        rect.attr("y", function(d) {
            return d3.min([y(d.value),y(0)]);
        })
        .attr("height", function(d) { return Math.abs(y(0) - y(d.value)); });

        // ajout icone en fond de graphique
        if (opts.show_icon && opts.icon_element) {
            var scale =  (height / pictosize);
            var translate = (width / 2) - (scale * 24);
            $(opts.icon_element).attr('transform','translate('+translate+',0) scale('+scale+')');
            $(opts.icon_element).attr('style','opacity:0.8');
            $(opts.icon_element).attr('class','icon-layer');
            chart.node().appendChild(opts.icon_element)
        }

        // création d'un rectangle de capture d'évenement sur toute la hauteur du graph et la largeur du baton (pour affichage du tooltip au survol de ce rectangle)
        if (opts.tooltip)
        year.append("rect")
        .attr('class','eventrect')
        .attr('x', 0)
        .attr('width', x0.bandwidth())
        .attr('y', 0)
        .attr('height', height )
        .on("mousemove", function(d){
            var mouse = d3.mouse(chart.node());
            tooltip(d.key, mouse[0], mouse[1]);
            chart.select('.charttooltip').style("visibility", "visible");
        })
        .on("mouseout", function(){
            chart.select('.charttooltip').style("visibility", "hidden");
        });


} // FIN barchart

var linechart = function() {


    var x0 = d3
    .scalePoint()
    .domain(by_year.map(function(d) {
        return d.key;
    }))
    .rangeRound([0, width]);


    var xAxis = d3.axisBottom(x0);

    chart.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .selectAll('text')

    var area = d3.line()
    .curve(d3.curveLinear)
    .x(function(d) {
        return x0(d.year.value);
    })
    .y(function(d) {
        return y(d.xapprox.value);
    });

    var place = chart.selectAll(".place")
    .data(by_place)
    .enter()
    .append("g")
    .attr("class", "place");

    place.append("path")
    .attr("d", function(d) {
        return area(d.values);
    })
    .attr("class", function(d) {
        return "area stroke color-" + colorscale(d.key); }
    )

    // dots
    place.selectAll(".dot")
    .data(function(d){
        return d.values.map(function(i) {
            return {'year':i.year.value,'place':i.placeURI.value, 'value':i.xapprox.value}
        })
    })
    .enter()
    .append("circle")
    .attr("r", ird(3.5))
    .attr("cx", function(d) {
        return x0(d.year);
    })
    .attr("cy", function(d) { return y(d.value); })
    .attr("class", function(d) {
        return "dot fill color-" + colorscale(d.place);
    })
    
    place.selectAll(".tipdot")
    .data(function(d){
        return d.values.map(function(i) {
            return {'year':i.year.value,'place':i.placeURI.value, 'value':i.xapprox.value}
        })
    })
    .enter().append("circle")
    .attr("class","tipdot eventrect")
    .attr("r", ird(20))
    .attr("cx", function(d) {
        return x0(d.year);
    })
    .attr("cy", function(d) { return y(d.value); })
    .on("mouseover", function(d){
        var mouse = d3.mouse(this);
        tooltip(d.year, x0(d.year) - ir(60), mouse[1]);
        chart.select('.charttooltip').style("visibility", "visible");
    })
    .on("mouseout", function(){
        chart.select('.charttooltip').style("visibility", "hidden");
    });

    if (opts.show_icon && opts.icon_element) {
        var scale = (height / pictosize);
        var translate = (width / 2) - (scale * ir(24));
        $(opts.icon_element).attr('transform','translate('+translate+',0) scale('+scale+')');
        $(opts.icon_element).attr('style','opacity:0.8');
        $(opts.icon_element).attr('class','icon-layer');

        chart.node().appendChild(opts.icon_element)
    }



} // fin linechart

// icones qui grandissent : au lieu de dessiner des rect, on dessine des icones

var iconchart = function() {
    // icons display
    // affiche une icone par Serie, avec masque de remplissage couleur,
    // intitulé de la série et valeur
    // console.log('display icon chart')
    opts.phantom && window.callPhantom('icon chart')
    var x0 = d3.scaleBand()
    .padding(ir(.05))
    .paddingInner(ir(.2))
    .rangeRound([0, width])
    var years = by_year.map(function(d) {
        return d.key;
    })
    x0.domain(years);

    var x1 = d3.scaleBand();
    x1.domain(places).rangeRound([0, x0.bandwidth()]);


    chart.append("text")
    .attr("x", width / 2)
    .attr("y", ir(20))
    .style("text-anchor", "middle")
    //	    .style("font-size", "200%")
    .style("fill", "#333")
    .style("font-weight", "bold")
    .style('text-anchor', "middle")
    .text(data[0].valueLabel.value);


    var yeargroup = chart.selectAll(".year")
    .data(by_year);

    var year = yeargroup.enter().append("g")
    .attr("class", "year")
    .attr("transform", function(d) {
        return "translate(" + x0(d.key) + ",0)";
    })

    var cardinality = (years.length * places.length )
    var pictowidthratio  = width / ir(150)

    if (cardinality > 1)
    pictowidthratio  = width / (ir(75 * cardinality))



    var pictowithmask = function(arg) {
        /*	    var s; */
        maskcount++;
        var maskid = "mask"+maskcount;
        var mask = d3.select(elt).selectAll("svg").append("defs")
        .append("clipPath")
        .attr("id", maskid)
        var maskrect = mask
        .append("rect")
        .attr("x", "0")
        .attr("y", pictosize)
        .attr("height", "0")
        .attr("width", pictosize)
        .attr("style", "fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:none");


        var valratio = (arg.value / arg.max);

        if (opts.anim) {
            maskrect.attr("height",0)
            .attr("y",pictosize)
            .transition()
            .duration(500)
            .attr("height",pictosize * valratio)
            .attr("y",pictosize * (1 - valratio))

        } else {
            maskrect.attr("height",pictosize * valratio)
            .attr("y",pictosize * (1 - valratio))
        }


        opts.phantom && window.callPhantom(opts.icon_element);

        var e = opts.icon_element.cloneNode(true)
        d3.select(e).style('clip-path', "url(#" + maskid +")")
        d3.select(e).selectAll('path')
            //.style("fill", function(d) { return seriescolor(colorscale(arg.place)); })
            .attr("class", function(d) { return "fill color-" + colorscale(arg.place); })
        d3.select(e).selectAll('circle')
            //.style("fill", function(d) { return seriescolor(colorscale(arg.place)); })
            .attr("class", function(d) { return "fill color-" + colorscale(arg.place); })
        d3.select(e).selectAll('rect')
            //.style("fill", function(d) { return seriescolor(colorscale(arg.place)); })
            .attr("class", function(d) { return "fill color-" + colorscale(arg.place); })

        return e
    } // picto with mask

    var pictogrey = function(arg) {
        var e = opts.icon_element.cloneNode(true)
        return e
    }

    var pictogroup = year.selectAll("g")
    .data(function(d) {
        return d.values.map(function(i) {
            var xmax;
            if (i.xmax)
            xmax = i.xmax.value;
            else
            xmax = d3.max(datadomains, function(d) {
                return parseFloat(d.xapprox.value);
            })
            return {
                'name':i.placeLabel.value,
                'place':i.placeURI.value,
                'value':i.xapprox.value,
                'max':xmax
            };
        });
    })
    var pictos = pictogroup
    .enter()
    .append('g')
    .attr("transform", function(d) {
        var xpos = x1(d.name) + (x1.bandwidth() / 2) - (pictowidthratio * pictosize / 2) ;
        return "matrix("+pictowidthratio+",0,0,"+pictowidthratio+"," + xpos + "," + (height - ir(30) - (ir(150) * Math.log(pictowidthratio))) +  ")";
    })

    pictos.append(pictogrey, ':first-child')
    pictos.append(pictowithmask, ':first-child')
    /*	    .attr("transform", function(d) {
    return "matrix("+pictowidthratio+",0,0,"+pictowidthratio+"," + x1(d.name) + "," + (height - (70 * pictowidthratio)) +  ")";
})
*/



var wrap = function (text, width) {
    text.each(function() {
        var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
            }
        }
    });
}



// texte valeur numérique

pictogroup
.enter()
.append('text')
.text(function(d) { return d.value })
//.style("fill", function(d) { return seriescolor(colorscale(d.place)); })
.attr("class", function(d) { return "fill color-" + colorscale(d.place); })
.style("font-size", "50%")
.style("font-weight", "bold")
.style('text-anchor', "middle")
.attr("transform", function(d) {
    var xpos = x1(d.name) + (x1.bandwidth() / 2);
    var ypos = height - fontratio;
    return "matrix("+pictowidthratio+",0,0,"+pictowidthratio+"," + xpos + "," + (height - fontratio) +")";
})



// texte legende

var text_legende = pictogroup
.enter()
.append('text')
.style('text-anchor', "middle")
.style("font-size", "30%")
.attr("transform", function(d) {
    var xpos = x1(d.name) + (x1.bandwidth() / 2);
    var ypos = height - (2.5 * fontratio) - 10 * logir(pictowidthratio) ;
    return "matrix("+pictowidthratio+",0,0,"+pictowidthratio+"," + xpos + "," + ypos +")";
})
.attr("dx", 0)
.attr("dy", 0)
//.style("fill", function(d) { return seriescolor(colorscale(d.place)); })
.attr("class", function(d) { return "fill color-" + colorscale(d.place); })
.text(function(d) { return d.name })
text_legende.selectAll('text')
.call(wrap, x1.bandwidth()/2)




if (by_year.length > 1) {
    yeargroup.enter()
    .append('line')
    .attr("x1", function(d) {
        return x0(d.key)
    })
    .attr("y1", (height - (ir(80) * pictowidthratio)))
    .attr("x2" ,function(d) {
        return x0(d.key) + x0.bandwidth();
    })
    .attr("y2" , (height - (ir(80) * pictowidthratio)))
    .style('stroke','#CCC')
    .style('stroke-width',"1px");

    yeargroup.enter()
    .append('text')
    .text(function(d) { return d.key })
    .style("fill", "#333")
    .style("font-weight", "bold")
    .style('text-anchor', "middle")
    .attr("transform", function(d) {
        var xpos = x0(d.key) + (x0.bandwidth() / 2);
        return "translate("+ xpos + "," + (height - (ir(85) * pictowidthratio)) +")";
    })
}


} // iconchart


// adds legend
var legend_layer = function() {
    var legend = chart.selectAll(".legend")
    .data(by_place)
    .enter().append("g")
    .attr("class", "legend")
    .attr("transform", function(d, i) {
        // TODO Deux colonnes si + de 4 séries
        var pos = "translate(0," + (ir(40) + (i * ir(20))) + ")";
        return pos
    })

    legend.append("rect")
    .attr("x", 0)
    .attr("y", height + ir(17))
    .attr("width", ir(15))
    .attr("height", ir(15))
    //.style("fill", function(d){return(seriescolor(colorscale(d.key)))});
    .attr("class", function(d) { return "fill color-" + colorscale(d.key); })

    // console.log('legend font ratio', fontratio)
    legend.append("text")
    .attr("x", ir(17))
    .attr("y", height + ir(17))
    .attr("dy", "1.2em")
//    .attr("dy", '1em')
    .style("text-anchor", "start")
    .style('font-size', (0.5*fontratio) +'pt')
    .text(function(d) { return d.values[0].placeLabel.value; });

    // overlay for tooltips interactivity
};

var tooltipelt;

var tooltip_layer = function() {
    if (opts.tooltip) {
        tooltipelt = chart.append("g")
        .attr("class", "charttooltip")

        tooltipelt.append('rect')
        .attr("x",1)
        .attr("y",0)
        .attr("rx",3)
        .attr("ry",3)
        .attr("width",120)
        .attr("class","tooltipbg")
    }
};

var tooltipyear;
var tooltip = function(year, xpos, ypos) {
    var values = by_year.filter(function(d) { return d.key == year})[0].values
    tooltipelt.attr("transform", "translate(" + (xpos - 60) + "," + (ypos - ((values.length * 20)+10))  + ")")

    tooltipelt.select('.tooltipbg').attr("height",3+((values.length ) * 20) )


    if (year == tooltipyear)
    return
    tooltipyear = year;
    var tipdata = values.map(function(d,i) {
        var v;
        if (d.hasOwnProperty('xDisplay'))
            v = d.xDisplay.value;
        else
            v = d.xapprox.value;
        //        console.log('tooltip', v)
		//format number in string v
		var number = v.match(/\d+/g); // 123456
        var formatedNumber = fr.format(',.0f')(number);
        formatedV = v.replace(number, formatedNumber);
        return {"key":d.placeURI.value, "value":formatedV}
    })
    tooltipelt.selectAll('.tip').remove()
    var tips = tooltipelt.selectAll('.tip')
    .data(tipdata)

    var tipline = tips.enter()
    .append('g')
    .attr("class","tip")
    .attr("transform", function(d, i) { return "translate(3," + (i-1) * 20 + ")"; });

    tipline.append("rect")
    .attr("x", 0)
    .attr("y", 22)
    .attr("width", 18)
    .attr("height", 18)
    //.style("fill", function(d){return(seriescolor(colorscale(d.key)))})
    .attr("class", function(d) { return "fill color-" + colorscale(d.key); })


    tipline.append("text")
    .attr("x", 24)
    .attr("y", 22)
    .attr("dy", "1.2em")
    .style("font-size","12px")
    .style("text-anchor", "start")
    .text(function(d) { return d.value; });
}

var calldraw = function() {
    if (opts.chartkind == "bar") {
        axes();
        barchart();
        tooltip_layer();
        legend_layer()
    }
    if (opts.chartkind == "line") {
        axes();
        linechart();
        tooltip_layer();
        legend_layer()
    }
    if (opts.chartkind == "icons")
    iconchart();
}

opts.phantom && window.callPhantom('get icon ?')
if (opts.show_icon && data[0].hasOwnProperty("icon"))
{
    opts.phantom && window.callPhantom('has icon')
    var icon = data[0].icon.value;
    $( document ).ajaxError(function(e,x,s,err) {
        opts.phantom && window.callPhantom("ajax error")
        opts.phantom && window.callPhantom(JSON.stringify(err))
        // console.log(err)
    });
    var url, async;
    var theme = $('body').data('theme')
    if (opts.phantom) {
        url = 'file:///kolekti/src/kolekti_server/kserver/static/img/'+ theme + '/'+ icon
        async = false;
        window.callPhantom(url)
    } else {
        async = true;
        url = kolekti.static+'img/'+ theme + '/' + icon
    }

    $.ajax({"url":url,
    "dataType":"xml",
    "async":async})
    .done(function(data) {
        /*
        $(data.documentElement).find('path').each(function() {
        $(this).attr('style','fill:#D0D0D0');
    });
    */

    opts.phantom && window.callPhantom(data)
    if(data) {
        opts.icon_element = $(data.documentElement).find('g')[0];
        var svg_group = $(data.documentElement).find('g')[0];
    } else {
        opts.draw_icon = false;
    }
    /*
    var scale = (height / pictosize);

    var translate = margin.left + ((width / by_year.length) * .2);

    chart.select('.year').node().insertBefore(svg_group,chart.select('.eventrect').node());
    $(svg_group).attr('transform','translate('+translate+',0) scale('+scale+')');
    $(svg_group).attr('style','opacity:0.8');
    */
})
.fail(function(data) {
    opts.phantom && window.callPhantom('ajax fail')
    opts.draw_icon = false;
})
.always(function() {
    opts.phantom && window.callPhantom('do draw')
    calldraw();
})

} else {
    opts.show_icon = false;
    calldraw()
}



}; // drawchart
