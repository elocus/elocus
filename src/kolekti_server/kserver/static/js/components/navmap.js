$(function() {
    $.getScript(kolekti.static+'js/locale_fr.js');
    $.getScript(kolekti.static+'js/components/navmap_functions.js', function(){
        var redraw = function() {
            $('.navigation-map').each(function() {
                display_navmap(this);

            });
            $(".intro-topics").trigger( "displayed.elocus.topics", [ "navmap" ] ); // why ??
        };

        $(window).on('resize', redraw);
        $('body').on('redraw.elocus.topics', redraw);
        redraw()
    })
})
