var elocus_map_shown_properties =['type_equipement'];

var drawmap = function(root, cb, timeout) {
    // console.log(root);
    // console.log($(root).data('geojson'))
    if ($(root).data('geojson').results) {
	var map = L.map($(root).get(0), {
	    fullscreenControl: true
	})
	    .setView([42.444508, -76.499491], 12);
	map.invalidateSize();
	var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	    
	    maxZoom: 18,
	    attribution: 'Map data ©<a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
		'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="http://mapbox.com">Mapbox</a>',
	    id: 'mapbox.streets'
	});
	
	// console.log(tiles.on);
	
	tiles.on("load", function() {
	    // console.log('tiles loaded')
	    // console.log(map)
	    cb(map);
	})
	
	tiles.addTo(map);
	var bounds;
	$.each($(root).data('geojson').results.bindings, function(i,e){ 
	    if (e.hasOwnProperty('geojson')) {
		var geojson = e.geojson.value	
		var geodata = JSON.parse(geojson)
	
		var myLayer = L.geoJson(geodata, {
/*
  onEachFeature: function(feature, layer) {
  // console.log(feature);
  if (feature.hasOwnProperty('properties'))
  layer.bindPopup(feature.properties.description);
  }
*/
		}).addTo(map) ;
	    }
	    if (e.hasOwnProperty('wkt')) {
		// console.log('WKT')
		var myLayer = omnivore.wkt.parse(e.wkt.value).addTo(map);
	    }
	    if (e.popup) {
		var buf = ""
		var popup = JSON.parse(e.popup.value)
		$.each(popup, function(pi,pe) {
		    // console.log(pe)
		    buf += pe.label + ": " + pe.value +"<br>"
		})
		       
		    myLayer.bindPopup(buf)
	    }
	    // 
	    
	    if (i == 0)
		bounds = myLayer.getBounds();
	    else
		bounds.extend(myLayer.getBounds());
	});
		    
	map.fitBounds(bounds, { padding: [20, 20] });
	if(timeout)
	    window.setTimeout(function () {
		cb(map);
            }, 5000);
    }

}
