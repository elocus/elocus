var mapsreg = {}
var register_map = function(map, id) {
    mapsreg[id] = map
}

var get_map = function(id) {
    return mapsreg[id]
}

$(document).ready(function() {
    $.getScript(kolekti.static+'js/components/map_functions.js', function(){
        if ($('.star-topics .leafletmap').length) {
            // display map on report main page for starred topics
            $('.star-topics .leafletmap').each(function() {
                if (! $(this).find('div').length) {
                    drawmap(this, function(map){
                        register_map(map, $(this).attr('id'));
                    })
                }
            });
            $(".star-topics").trigger( "elocus.graphics.displayed", [ "map" ] );
        }

        if ($('.intro-topics .leafletmap').length) {
            // display map on report main page for starred topics
            $('.intro-topics .leafletmap').each(function() {
                if (! $(this).find('div').length) {
                    drawmap(this, function(map){
                        register_map(map, $(this).attr('id'));
                    })
                }
            });
            $(".intro-topics").trigger( "elocus.graphics.displayed", [ "map" ] );
        }

        // deroulement de la section (affichage des topics)
        $('.section-content.collapse').on('shown.bs.collapse', function(e) {
            if ($(e.target).hasClass('section-content')) {
                $(e.target).find('.panel .leafletmap').each(function() {
                    if (! $(this).find('div').length) {
                        drawmap(this, function(map){
                            register_map(map, $(this).attr('id'));
                        })
                    }
                });
            }
            resize();
        })

        $('.modal-topic-details').on('shown.bs.modal', function(e) {
            $(e.target).find('.leafletmappanel').each(function() {
                $(this).attr('style','width:100%; height:400px ; position:relative');
                if (! $(this).find('div').length) {
                    drawmap(this, function(map){
                        // console.log(map)
                        register_map(map, $(this).attr('id'));
                    })
                }
            })
        })

        var resize =  function() {
            $('.collapse.in').find('.leafletmap, .leafletmappanel').each(function(e,i) {

                var wwidth = $(this).width();
                var wheight = (wwidth / 2) + 3;
                if (wheight > 400)
                wheight = 400;
                $(this).attr('style','width:100%; height:'+ wheight +'px ; position:relative');
                var map = get_map($(this).attr('id'));
                map && map.invalidateSize();
            });

            $(".collapse.in").trigger( "elocus.graphics.displayed", [ "map" ] );

        };

        $(window).on('resize', resize);
        $(window).on('redraw.elocus.topics', function(){
            $('.collapse.in').find('.leafletmap, .leafletmappanel').each(function(e,i) {
                var map = get_map($(this).attr('id'));
                map && map.invalidateSize();
            });
        });

    })

})
