/* --------------------------
 ajout topic chooser
 ----------------------------*/

$(function() {

    var project = $('body').data('project')
    var report = $('body').data('report')

    $("body").on("click", ".elocus-action-insert-topic", function(){
        console.log("elocus-action-insert-topic");
        var topicLoader = $($('#user_topic_chooser').html());
        topicLoader.data('creator-id', $(this).closest('.topic').attr('id'))
        $(this).closest('.widget').after(topicLoader);
        $(this).closest('.topics').trigger("harmonize.topics.heights");
        return false;
    });

    $("body").on("click", ".elocus-action-delete-topic", function(){
        var topic = $(this).closest('.topic')
        var topicid = $(topic).attr('id')
    	var btn = $(this)
    	$.ajax({
    	    url:"/elocus/"+project+"/"+report+"/usertopic/"+topicid+"/delete/",
    	    method:'POST',
    	    data:$.param({
    		    'topic': topic.attr('id'),
    		})
    	}).done(function(data) {
    	    if (data.status == 'ok') {
                var topics = $(topic).closest('.topics');
                $(topic).closest('.widget').remove();
				//console.log('topics', topics);
                topics.trigger("harmonize.topics.heights");
                console.log("elocus-action-delete-topic");
    	    }
    	}).fail(function(data) {
    	});

    });


    $("body").on("click", ".add-topic-text", function(){
        var formTopicWidget = $(this).closest('.widget')
        var creatorTopicId = formTopicWidget.data('creator-id')

        $.ajax({
            url : '/elocus/' + project + '/' + report + '/usertopic/' + creatorTopicId + '/newtext/',
            type : 'POST',
        }).done(function(data){
            formTopicWidget.after(data);
            edid = formTopicWidget.next().find('div.description-editor').attr('id')
            editor = CKEDITOR.replace(edid,{startupFocus : true, height: 180 })
            editor.elocus_state = false
            var topics = $(formTopicWidget).closest('.topics');
            formTopicWidget.remove();
            topics.trigger("harmonize.topics.heights");
        })
    })


    $('body').on('change', '.upload-file input', function() {
        var formTopicWidget = $(this).closest('.widget')
        var creatorTopicId = formTopicWidget.data('creator-id')
        var form = $(this).closest('form').get(0)
        var formdata = new FormData()
        formdata.append('file', this.files[0]);

        $.ajax({
            url : '/elocus/' + project + '/' + report + '/usertopic/' + creatorTopicId + '/newimage/',
            data : formdata,
            type : 'POST',
            processData: false,
            contentType: false,
        }).done(function(data){
            formTopicWidget.after(data);
            formTopicWidget.remove();
            formTopicWidget.closest('.topics').trigger("harmonize.topics.heights");
        })

    })

    $("body").on('dragenter', '.drop-zone', function() {
        // console.log("dragenter");
        $(this).addClass('panel-danger');
        $(this).addClass('drop');
        return false;
    });

    $("body").on('dragover', '.drop-zone', function(e){
        // console.log("dragover");
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('panel-danger');
        $(this).addClass('drop');
        return false;
    });

    $("body").on('dragleave', '.drop-zone', function(e) {
        // console.log("dragleave");
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('panel-danger');
        $(this).removeClass('drop');
        return false;
    });

    $("body").on('drop', '.drop-zone', function(e) {
        // console.log("drop");
        var dndTopicWidget = $(this).closest('.widget')
        var creatorTopicId = dndTopicWidget.data('creator-id')
        var upload = function(files) {
            var promises = []
            var progress_uploads = 0;
            var imagePromise = function(f) {
                // Only process image files.
                /*
                if (!f.type.match('image/jpeg')) {
                alert('The file must be a jpeg image') ;
                return false ;
                }
                */
                var loader = new FileReader();
                var def = $.Deferred(), promise = def.promise();
                loader.onprogress = loader.onloadstart = function (e) { def.notify(e); };
                loader.onerror = loader.onabort = function (e) { def.reject(e); };
                promise.abort = function () { return loader.abort.apply(loader, arguments); };

                // When the image is loaded,
                // run handleReaderLoad function
                loader.onload = function(evt) {
                    var pic = {};
                    var picinfo = evt.target.result.split(',');

                    pic.mime = picinfo[0];
                    pic.file = picinfo[1];
                    pic.name = f.name;
                    pic.path = "/";
                    $.ajax({
                        type: 'POST',
                        url: '/elocus/' + project + '/' + report + '/usertopic/' + creatorTopicId + '/newimage/',
                        data: $.param(pic),
                    }).done(function(data) {
                        def.resolve(data) ;
                    })
                }
                loader.readAsDataURL(f);

                return promise;
            }
            $.each(files, function(i,f) {
                var q = imagePromise(f)
                    .done(function(data){
                        console.log('upload done', f)
                        console.log(data)
                        dndTopicWidget.after(data);
                    })
                promises.push(q)
            });
            $.when.apply($, promises).done(function (arg) {
                var topics = dndTopicWidget.closest('.topics');
                dndTopicWidget.remove();
                topics.trigger("harmonize.topics.heights");
                console.log("elocus-action-created-image-topic");
            });
        }
        // end upload

        if(e.originalEvent.dataTransfer){
            if(e.originalEvent.dataTransfer.files.length) {
                // Stop the propagation of the event
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass('panel-danger');
                $(this).removeClass('drop');
                // Main function to upload
                upload(e.originalEvent.dataTransfer.files);
            }
        }
        else {
            $(this).removeClass('drop');
            $(this).removeClass('panel-danger');
        }
        return false;
    });


});
