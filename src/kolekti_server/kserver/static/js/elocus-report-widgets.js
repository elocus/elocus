$(function() {

    /* -------------------------------------------------------------------------------
    * heights harmonization after redraw or resize
    * ------------------------------------------------------------------------------*/

    var mapsReady = ($(".leafletmap").length == 0); // mapsReady true if no map
    var chartsReady = ($(".elocus-chart").length == 0); // chartsReady true if no chart

    /* equalize widgets part height, subfunction of equalizeTopics
     * @param elt, defined topics set
     * @param partClass, panel subclass name, ex : panel-heading
     */
    var equalizePanelParts = function(elt, partClass) {
        var maxHeight = 0;
        var panelParts = $('.panel .' + partClass, elt);
        // on enlève la spécification assignée par un appel précédent à cette fonction
        $(panelParts).css('height', 'inherit');
        // get max height
        $(panelParts).each(function(){
            if($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        // Set the height of all those children to whichever was highest
        if(maxHeight > 20) $(panelParts).height(maxHeight);
    }

    /* equalize widgets body height, subfunction of equalizeTopics
     * @param elt, defined topics set
     */
    var equalizePanels = function(elt) {
        var maxHeight = 0;
        var panels = $('.panel', elt);
        //on enlève la spécification assignée par un appel précédent à cette fonction
        $(panels).find('.panel-body').css('height', 'inherit');
        // définition de la hauteur maximale des headers
        $(panels).each(function(){
            if($(this).height() > maxHeight) {
                maxHeight = $(this).outerHeight();
            }
        });
        // set panel-body height depending on panel-heading and panel-footer height
        $(panels).each(function(){
            panelHeadingHeight = $(this).find('.panel-heading').length ? $(this).find('.panel-heading').outerHeight() : 0;
            panelFooterHeight = $(this).find('.panel-footer').length ? $(this).find('.panel-footer').outerHeight() : 0;
            // console.log("panelHeadingHeight" + panelHeadingHeight);
            // console.log("panelFooterHeight" + panelFooterHeight);
            panelBodyHeight = maxHeight - ( panelHeadingHeight + panelFooterHeight ) + 1;
            $(this).find('.panel-body').css("height", panelBodyHeight);
        });
    }

    /* equalize widgets height
     * @param elt, defined topics set
     */
    var equalizeTopics = function(elt) {
        // elt can be : .star-topics (ensemble des indicateurs à la une), .topics (ensemble des topics en head), .section-content.collapse (.in) (accordeons du rapport qui s'ouvrent ou qui sont ouverts),
        var currentOffset = null;
        var currentTopics = [];
        var yOffsetsArray = []; // array containing the y-offset of each line
        var topicsJoinedByLine = {}; // doble dimension array of aligned topics joined by yOffsetKey
        $('.topic', elt).each(function(index, topic) { // for each topic in elt, equalizePanels step by step
            //var topicName = $(topic).find("> .panel > .panel-heading > h4").html();
            var yOffset = Math.round($(topic).closest('.widget').offset().top) + "_y";
            if (yOffset == currentOffset) {
                currentTopics.push(topic);
            } else {
                $(currentTopics).closest('.widget').attr("yOffset", currentOffset); // pour debug, pour indiquer l'offset trouvé
                equalizePanelParts(currentTopics, 'panel-heading'); //necessary equalize panel-headings also
                equalizePanelParts(currentTopics, 'panel-footer'); //necessary equalize panel-footer also
                equalizePanels(currentTopics);
                currentTopics = [topic];
                currentOffset = Math.round($(topic).closest('.widget').offset().top) + "_y";
            }
        });
        // traite la derniere ligne
        $(currentTopics).closest('.widget').attr("yOffset", currentOffset); // pour debug, pour indiquer l'offset trouvé
        equalizePanelParts(currentTopics, 'panel-heading'); //necessary equalize panel-headings also
        equalizePanelParts(currentTopics, 'panel-footer'); //necessary equalize panel-footer also
        equalizePanels(currentTopics);
    }



    $('body').on("displayed.elocus.topics", function(e, component) { //evt déclarés dans chart.js et map.js
        if(component == 'map') mapsReady = true;
        if(component == 'chart') chartsReady = true;
        $(e.target).trigger("harmonize.topics.heights");
        //if(mapsReady && chartsReady) ...
    });

    $('body').on("harmonize.topics.heights", function(e) {
        //console.log("harmonize.topics.heights on:", e.target);        
        equalizeTopics(e.target);
    });

    $('body').on("harmonize.panels.heights", function(e) {
		equalizePanelParts(e.target, 'panel-heading'); //necessary equalize panel-headings also in some cases
		equalizePanelParts(e.target, 'panel-footer'); //necessary equalize panel-footer also in some cases
		equalizePanels(e.target);
    });

    // trigger base document ready events (events for charts are triggered when the chart is drawed : displayed.elocus.topics)
	$('body').on("harmonize.base", function() {
		//lance l'harmonisation des teasers, des intro-topics et des topics dont l'accordéon est ouvert
	    $('.home-general .teasers-project').trigger("harmonize.panels.heights");
	    $('.home-report.sections').trigger("harmonize.panels.heights");
	    $('.panel-group .panel .collapse.in').each(function(index, el) {
	        //console.log("closest panel-collapse-in id: " + $(el).closest('panel-collapse').attr('id'));
	        $(el).trigger("harmonize.topics.heights");
	    });

    });
	$('body').trigger('harmonize.base');


    /* -------------------------------------------------------------------------------
    * pour ouverture accordéon quand on suit un lien d'indicateur à la une
    * ------------------------------------------------------------------------------*/

    var opening;
    window.doOpenTopic = function() {
        if(opening) return
        var pageUrl = window.location.href;
        var hasEltIdInPageUrl = pageUrl.indexOf("#");
        if (hasEltIdInPageUrl != -1) {
                if (!opening) {
            opening = true
            var eltIdInPageUrl = '#' + pageUrl.split('#')[1];
            // console.log(eltIdInPageUrl);

            $(eltIdInPageUrl).closest('.topicCollapses').find('.collapseTopic').removeClass('in')
            $(eltIdInPageUrl).parents().collapse('show');
            $(eltIdInPageUrl).closest(".section-content.collapse").trigger("shown.elocus.section");

            $('html, body').animate({
                    scrollTop: $(eltIdInPageUrl).offset().top - 60
            }, 1);
                }
        }
    }


    /* -------------------------------------------------------------------------------
    * extension des topics
    * ------------------------------------------------------------------------------*/

    var set_topic_display_size = function(topic, size, func) {
        var report = $('.report').data('report')
        var project = $('.report').data('project')
        $.ajax({
            url:"/elocus/"+project+"/"+report+"/topicsize",
            method:'POST',
            data:$.param({
                'topic': topic,
                'size': size
            })
        }).done(function(data) {
            if (data.status == 'ok') {
                func()
            }
        }).fail(function(data) {
        });
    }

    $('body').on('click','.elocus-action-extend', function() {
        var topic = $(this).closest('.topic')
        var topic_container = $(topic).parent()
        // console.log(topic_container)
        var ntiles = topic.data('display-size')
        if (!ntiles || ntiles == 1) {
            set_topic_display_size($(topic).attr('id'), 2, function() {
                $(topic).data('display-size', 2)
                $(topic_container).addClass('col-lg-12')
                $(topic_container).removeClass('col-lg-6')
                $(topic).find('.components>div')
                        .removeClass('col-lg-12')
                        .addClass('col-lg-6')
                $(window).trigger('resize');
            })
        }
    })

    $('body').on('click','.elocus-action-reduce', function() {
        var topic = $(this).closest('.topic')
        var topic_container = $(topic).parent()
        var ntiles = topic.data('display-size')
        if (ntiles == 2) {
            set_topic_display_size($(topic).attr('id'), 1, function() {
                $(topic).data('display-size', 1)
                $(topic_container).addClass('col-lg-6')
                $(topic_container).removeClass('col-lg-12')
                $(topic).find('.components>div')
                        .removeClass('col-lg-6')
                        .addClass('col-lg-12')
                $(window).trigger('resize');
            })
        }
    })

});

$(window).resize(function() {
	$('body').trigger('harmonize.base');
});
