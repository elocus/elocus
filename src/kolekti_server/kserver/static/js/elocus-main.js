$(function() {

    /* --------------------------
         retour haut de page
     ----------------------------*/
    $(window).scroll(function(){
        var posScroll = $(document).scrollTop();
        if(posScroll >=300) {
            $('#topTopLink').fadeIn(600);
            $('#topTopLink').css('display','block');
        } else {
            $('#topTopLink').fadeOut(600);
        }
    });
    $('#topTopLink').click(function(){
        $('html').animate({scrollTop:0}, 'slow');
        return false;
    });


    /* --------------------------
         menu extractible
     ----------------------------*/
    /* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
    var openNav = function() {
        var transitionTime = 400;
        if (sessionStorage && (sessionStorage.getItem("navbar-state") == 'true')) {
            transitionTime = 0;
            $("#mySidenav").addClass("no-transition");
            $("#reportContent").addClass("no-transition");
        }
        $("#mySidenav").css("left", "0px");
        //$("#mySidenav").css("width", "300px");
        $("#reportContent").css("marginLeft", "300px");
        $("#openNavBtn").hide(transitionTime);
        $("#closeNavBtn").show(transitionTime);
        sessionStorage && sessionStorage.setItem("navbar-state", true);
        $("#mySidenav").removeClass("no-transition");
        $("#reportContent").removeClass("no-transition");
        setTimeout(function () {
            $('.home-report.sections').trigger("harmonize.panels.heights");
            $('body').trigger('redraw.elocus.topics');
            jump();
        }, 500);
    }

    /* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
    var closeNav = function() {
        var transitionTime = 400;
        if (sessionStorage && (sessionStorage.getItem("navbar-state") == 'false')) {
            transitionTime = 0;
            $("#mySidenav").addClass("no-transition");
            $("#reportContent").addClass("no-transition");
        }
        $("#mySidenav").css("left", "-300px");
        //$("#mySidenav").css("width", "0");
        $("#reportContent").css("marginLeft", "0");
        $("#closeNavBtn").hide(transitionTime);
        $("#openNavBtn").show(transitionTime);
        sessionStorage && sessionStorage.setItem("navbar-state", false);
        $("#mySidenav").removeClass("no-transition");
        $("#reportContent").removeClass("no-transition");
        setTimeout(function () {
            $('.home-report.sections').trigger("harmonize.panels.heights");
            $('body').trigger('redraw.elocus.topics');
            jump();
        }, 500);
    }

    //$("#openNavBtn").hide('0');
    //$("#closeNavBtn").show('0');
    $('#openNavBtn').click(openNav);
    $('#closeNavBtn').click(closeNav);
    sessionStorage && (sessionStorage.getItem("navbar-state") == 'false') && closeNav();

    // if home page, remove marginLeft even if menu was open.
    if( $(".home-general").length ) $("#reportContent").css("marginLeft", "0");


    /* --------------------------
     widget page home-report clicables
     ----------------------------*/
     $('div.home-report .panel').each(function(index, el) {
        var href = $(el).find('a').attr('href');
        $(el).click(function(e) {
            e.preventDefault();
            e.stopPropagation()
            window.location = href;
        });
     });

    /* $('.sidebar').affix({
        offset: {
            top: 100,
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    }) */



    /* --------------------------
     formattage des nombres (avec js/locale_fr.js)
     ----------------------------*/

    var fr = d3.formatLocale(locale_fr);
    $(".formatNumbers").each(function(index, el) {
        var content = $(el).html();
        //return string with formatted numbers
        var numbers = content.match(/(\d+ )/g); // 123456 74
        if(numbers) {
            numbers.forEach(function(number) {
                var formatedNumber = fr.format(',.0f')(number);
                content = content.replace(number, formatedNumber);
                console.log(number,)
            });
        }
        $(el).html(content);
    });

});
