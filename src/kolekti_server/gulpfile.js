// Requis
var gulp = require('gulp');

// Include plugins
var plugins = require('gulp-load-plugins')(); // tous les plugins de package.json

// Variables de chemins
var source = './kserver/static'; // dossier de travail
var destination = '/static'; // dossier à livrer
var theme = process.env.KOLEKTI_THEME

gulp.task('bootstrap', function () {
    return gulp.src(source + '/bootstrap/less/bootstrap.less')
        .pipe(plugins.less())
        .pipe(plugins.csso())
        .pipe(plugins.rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(destination + '/bootstrap/css/'));    
})

gulp.task('default', function () {
    return gulp.src(source + '/less/default.less')
        .pipe(plugins.less({
            globalVars:{
                theme: theme
            }
        }))
        .pipe(gulp.dest(destination + '/css/'));
})

gulp.task('theme', function () {
    return gulp.src(source + '/less/' + theme + '/theme.less')
        .pipe(plugins.less({
            globalVars:{
                theme: theme
            }
        }))
        .pipe(gulp.dest(destination + '/css/'));
})

gulp.task('components', function () {
    return gulp.src(source + '/less/components/*.less')
        .pipe(plugins.less({
            globalVars:{
                theme: theme
            }
        }))
        .pipe(gulp.dest(destination + '/css/components/'));
})

gulp.task('componentstheme', function () {
    return gulp.src(source + '/less/' + theme + '/components/*.less')
        .pipe(plugins.less({
            globalVars:{
                theme: theme
            }
        }))
        .pipe(plugins.rename(function(path){
            path.basename += '-theme'
        }))

        .pipe(gulp.dest(destination + '/css/components/'));
})



gulp.task('build', ['bootstrap','default', 'theme', 'components', 'componentstheme']);
