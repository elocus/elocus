from django.conf import settings
from os import getenv

def elocus_debug_context(request):
    return {'DEBUG': settings.DEBUG}

def elocus_theme_context(request):
    return {'theme': getenv('KOLEKTI_THEME', 'elocus')}

def elocus_registration_context(request):
    return {'registration': getenv('ELOCUS_REGISTRATION_PROJECTS') is not None }