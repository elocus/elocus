"""
Django settings for kolekti_server project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import json
import logging
logger = logging.getLogger('kolekti.'+__name__)
from kolekti.settings import settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

KOLEKTI_CONFIG = settings()
def __get_config(env, section, item):
    try:
        VALUE = os.environ[env]
    except:
        try:
            VALUE = KOLEKTI_CONFIG.get(section).get(item,'')
        except:
            VALUE = ''
    return VALUE

KOLEKTI_BASE = __get_config('KOLEKTI_BASE','InstallSettings','projectspath')
KOLEKTI_SVN_ROOT = __get_config('KOLEKTI_SVN_ROOT','InstallSettings','svnroot')
KOLEKTI_SVN_PASSFILE = __get_config('KOLEKTI_SVN_PASSFILE','InstallSettings','svnpassfile')
KOLEKTI_SVN_GROUPFILE = __get_config('KOLEKTI_SVN_GROUPFILE','InstallSettings','svngroupfile')

KOLEKTI_THEME = os.getenv('KOLEKTI_THEME','elocus')

KOLEKTI_LANGS = ['fr','en','us','de','it']

KOLEKTI_SVNTPL_USER = __get_config('KOLEKTI_SVNTPL_USER','SvnRemotes','svnuser')
KOLEKTI_SVNTPL_PASS = __get_config('KOLEKTI_SVNTPL_PASS','SvnRemotes','svnpass')


# APP_DIR  = KOLEKTI_CONFIG.get('InstallSettings').get('installdir')
if os.sys.platform[:3] == "win":
    appdatadir = os.path.join(os.getenv("APPDATA"),'kolekti')
    DB_NAME = appdatadir + '\\db.sqlite3'
    DB_NAME = DB_NAME.replace('\\','/')
else:
    DB_NAME = os.getenv('KOLEKTI_DBFILE', os.path.join(BASE_DIR, 'db.sqlite3'))

try:
    os.makedirs(os.path.join(KOLEKTI_BASE,'.logs'))
except:
    pass


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '47+&9*yikq4^1_fpxaf32!u^5&m(tw7dssr+h-%4sq&3uzz7q9'

# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = (os.getenv('KOLEKTI_DEBUG',"") == "True")
TEMPLATE_DEBUG = False

HOSTNAME=os.getenv('SUBDOMAIN','localhost') + '.' + os.getenv('DOMAIN','localdomain')

if DEBUG:
    ALLOWED_HOSTS = ['127.0.0.1', 'localhost', HOSTNAME]
else:
    ALLOWED_HOSTS = [HOSTNAME]


SECURE_SSL_REDIRECT = (os.getenv('HTTPS','') == 'on')

# SMTP config

email_config = KOLEKTI_CONFIG.get('smtp_ssl')
if email_config is None:
    email_config = {}

EMAIL_HOST = os.getenv('KOLEKTI_EMAIL_HOST',email_config.get('host',''))
EMAIL_PORT = os.getenv('KOLEKTI_EMAIL_PORT',email_config.get('port',''))
EMAIL_HOST_USER = os.getenv('KOLEKTI_EMAIL_USER',email_config.get('user',''))
EMAIL_HOST_PASSWORD = os.getenv('KOLEKTI_EMAIL_PASSWORD',email_config.get('pass',''))
EMAIL_USE_TLS=False
EMAIL_USE_SSL=True
DEFAULT_FROM_EMAIL = os.getenv('KOLEKTI_EMAIL_FROM',email_config.get('from',''))
ADMIN_EMAIL = os.getenv('ADMIN_EMAIL',email_config.get('from',''))
ELOCUS_OPT = os.getenv('ELOCUS_OPT',"/opt/elocus")

# password validation

AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
            'OPTIONS': {
                'min_length': 8,
                }
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
        {
            'NAME': 'kserver.validators.HTPasswordGenerator',
        },
    ]

AUTH_SYNC_HTPASS = os.getenv('KOLEKTI_PRIVATE','/etc/kolekti') + '/htpasswd'
AUTH_SYNC_HTGROUP = os.getenv('KOLEKTI_PRIVATE','/etc/kolekti') + '/htgroup'

#LOG_PATH = os.path.join(KOLEKTI_BASE,'.logs')
LOG_PATH = '/var/log/kolekti'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[%(asctime)s] %(levelname)s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'console': {
            'format': '[%(name)s.%(funcName)s:%(lineno)d] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'debug.log'),
            'formatter':'verbose',
        },
        'file_info': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'info.log'),
            'formatter':'simple',
        },
        'file_kolekti': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'kolekti.log'),
            'formatter':'verbose',
        },
        'console_kolekti': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter':'console',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.info': {
            'handlers': ['file_info'],
            'level': 'INFO',
            'propagate': True,
        },
        'kolekti': {
            'handlers': ['file_kolekti'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'registration': {
            'handlers': ['file_kolekti'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

    # Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django_gulp',
    'django.contrib.staticfiles',
    'registration',
    'bootstrapform',
    'elocus',
    'kserver',
    'upload_data',
    'help',
    'notify',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
#    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'kolekti_server.urls'

WSGI_APPLICATION = 'kolekti_server.wsgi.application'

ACCOUNT_ACTIVATION_DAYS = 7

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases


# APP_DIR  = KOLEKTI_CONFIG.get('InstallSettings').get('installdir')
#if os.sys.platform[:3] == "win":
#    appdatadir = os.path.join(os.getenv("APPDATA"),'kolekti')
#    DB_NAME = appdatadir + '\\db.sqlite3'
#    DB_NAME = DB_NAME.replace('\\','/')
#else:
#    DB_NAME = os.path.join(BASE_DIR, 'db.sqlite3')


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'OPTIONS': {
#             'read_default_file': '/home/waloo/my.cnf',
#         },
#     }
#}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': DB_NAME,
        },
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'fr_fr'
#TIME_ZONE = KOLEKTI_CONFIG.get('InstallSettings',{'timezone':"Europe/Paris"}).get('timezone')
TIME_ZONE = "Europe/Paris"
#TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

if DEBUG:
    STATIC_URL = '/staticdev/'
else:
    STATIC_URL = '/static/'

STATIC_ROOT = '/static'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # insert your TEMPLATE_DIRS here
            ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'kolekti_server.context_processors.elocus_debug_context',
                'kolekti_server.context_processors.elocus_theme_context',
                'kolekti_server.context_processors.elocus_registration_context',
                ],
            },
    },
    ]


TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    "/kolekti/src/kolekti_server/elocus/templates",
    "/kolekti/src/kolekti_server/kserver/templates",
)
RE_BROWSER_IGNORE=["~$","^\.svn$", "^#.*#$"]

# kolekti configuration

#STATICFILES_DIRS = (
#    KOLEKTI_BASE,
#)
# settings for active project


# celery / rabbit
from kombu import Exchange, Queue
# Redis

REDIS_PORT = 6379
REDIS_DB = 0
REDIS_HOST = os.environ.get('REDIS_PORT_6379_TCP_ADDR', 'redis')

RABBIT_HOSTNAME = os.environ.get('RABBIT_PORT_5672_TCP', 'rabbit')

if RABBIT_HOSTNAME.startswith('tcp://'):
    RABBIT_HOSTNAME = RABBIT_HOSTNAME.split('//')[1]

BROKER_URL = os.environ.get('BROKER_URL','')
if not BROKER_URL:
    BROKER_URL = 'amqp://{user}:{password}@{hostname}/{vhost}/'.format(
        user=os.environ.get('RABBIT_ENV_USER', 'admin'),
        password=os.environ.get('RABBIT_ENV_RABBITMQ_PASS', 'mypass'),
        hostname=RABBIT_HOSTNAME,
        vhost=os.environ.get('RABBIT_ENV_VHOST', ''))

# We don't want to have dead connections stored on rabbitmq, so we have to negotiate using heartbeats
BROKER_HEARTBEAT = '?heartbeat=60'
if not BROKER_URL.endswith(BROKER_HEARTBEAT):
    BROKER_URL += BROKER_HEARTBEAT

BROKER_POOL_LIMIT = 1
BROKER_CONNECTION_TIMEOUT = 10

# Celery configuration

# configure queues, currently we have only one
CELERY_DEFAULT_QUEUE = 'default'
CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
    )

# Sensible settings for celery
CELERY_ALWAYS_EAGER = False
CELERY_ACKS_LATE = True
CELERY_TASK_PUBLISH_RETRY = True
CELERY_DISABLE_RATE_LIMITS = False

# By default we will ignore result
# If you want to see results and try out tasks interactively, change it to False
# Or change this setting on tasks level
CELERY_IGNORE_RESULT = False
CELERY_SEND_TASK_ERROR_EMAILS = False
CELERY_TASK_RESULT_EXPIRES = 600

# Set redis as celery result backend
CELERY_RESULT_BACKEND = 'redis://%s:%d/%d' % (REDIS_HOST, REDIS_PORT, REDIS_DB)
CELERY_REDIS_MAX_CONNECTIONS = 10

# Don't use pickle as serializer, json is much safer
CELERY_TASK_SERIALIZER = "json"
CELERY_ACCEPT_CONTENT = ['application/json']

CELERYD_HIJACK_ROOT_LOGGER = False
CELERYD_PREFETCH_MULTIPLIER = 1
CELERYD_MAX_TASKS_PER_CHILD = 1000
