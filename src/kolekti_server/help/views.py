# -*- coding: utf-8 -*-

# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr> (<year>)
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.shortcuts import render
from os import getenv
from .form import issueForm
from .helpProvider import ProviderConfigurationError, HelpProvider

# Create your views here.
class HelpView(TemplateView):
    template_name = "help.html"
    try:
        helpProvider = HelpProvider()
    except ProviderConfigurationError as e:
        helpProvider = None
        error = e

    def _prepareContext(self, data=None):
        context = self.get_context_data()
        if self.helpProvider is not None :
            context.update(self.helpProvider.getDocumentation())
            if self.helpProvider.supportAvailable():
                if data is not None :
                    context['Form'] = issueForm(data)
                else:
                    context['Form'] = issueForm()
        else:
            context['Error'] = self.error
        return context

    def get(self, request):
        return  self.render_to_response(self._prepareContext())

    def post(self, request):
        context = self._prepareContext(request.POST)
        form = context['Form']
        if form is not None and form.is_valid():
            instanceName = getenv('SUBDOMAIN')+'.'+getenv('DOMAIN')
            fromPage = request.META['HTTP_REFERER']
            form.process_data(self.helpProvider, request.user.email, request.user.username, fromPage, instanceName)
            context['Subject'] = form.cleaned_data['subject']

        return  self.render_to_response(context)
