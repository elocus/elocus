# -*- coding: utf-8 -*-

# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr> (<year>)
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from .helpProvider import DocumentationProvider, SupportProvider, ProviderConfigurationError

import requests
import re
from lxml import etree
from os import getenv

import logging
logger = logging.getLogger('kolekti.'+__name__)

class GitlabAPI():
    GITLAB_API = 'api/v4/'
    def __init__(self, instanceUrl, projectId, token=None):
        self.baseUrl = instanceUrl  + self.GITLAB_API
        self.projectUrl = self.baseUrl + 'projects/'+ projectId
        self.headers = {'PRIVATE-TOKEN': token} if (token is not None) else {}
        self.projectPath = self.getJSON('/')[u'path_with_namespace']
        self.projectExtUrl = instanceUrl + self.projectPath

    def get(self, path):
        return requests.get(self.projectUrl+'/'+path, headers=self.headers)

    def post(self, path, data):
        return requests.post(self.projectUrl+'/'+path, headers=self.headers, data=data).json()

    def getJSON(self, path):
        return self.get(path).json()

    def toHtml(self, markdown):
        return requests.post(self.baseUrl+'markdown',
                             data = {
                                 'text': markdown,
                                 'gfm': True,
                                 'project': self.projectPath
                             }).json()['html']

    def url(self, slug):
        return self.projectExtUrl+'/'+slug



class GitlabDocumentation(DocumentationProvider):
    def __configurationError(self, settings):
        raise ProviderConfigurationError('DOCUMENTATIN_SETTINGS', settings, 'https://gitlab.domain.tld•projectId•FAQPageSlug')

    def __init__(self):
        settings = getenv('DOCUMENTATIN_SETTINGS')
        try:
            instanceUrl,projectId, self.faqPage = settings.split('•')
        except ValueError:
            self.__configurationError(settings)

        try:
            URLValidator(instanceUrl)
        except ValidationError :
            __configurationError(settings)

        if(not unicode(projectId).isnumeric() or self.faqPage == ''):
            __configurationError(settings)

        self.API = GitlabAPI(instanceUrl, projectId)

    def _getWiki(self):
        return {
            'url' : self.API.url('wikis/home'),
            'contents' : [
                {
                    'title': t['title'],
                    'url' : self.API.url('wikis/'+t['slug']),
                }
                for t in self.API.getJSON('wikis')
            ],
        }

    def _getFAQ(self):
        pageUrl = self.API.url('wikis/'+self.faqPage)
        ret = {
            'url' : pageUrl,
            'contents' : []
        }

        # Extract headings
        try:
            HTMLContents = self.API.toHtml(self.API.getJSON('wikis/'+self.faqPage)['content'])
        except KeyError:
            html = '<div class="alert alert-warning">La page de wiki "{}" ne peut être chargée correctement.</div>'.format(self.faqPage)
            ret['contents'].append({'title' : 'Erreur', 'contents':html})
            return ret

        doc = etree.HTML(HTMLContents)
        html=""
        title = None
        for element in doc.iter():
            if(element.tag == 'h1'):
                if title is not None :
                    ret['contents'].append({'title': title, 'contents': html})
                html=""
                title=element.xpath('normalize-space(string(.))')
            else:
                html += etree.tostring(element)

        ret['contents'].append({'title': title, 'contents': html})
        logger.debug(ret['contents'])

        return ret

    def getDocumentation(self):
        return {
            'FAQ' : self._getFAQ(),
            'Documentation' : self._getWiki(),
        }

class GitlabSupport(SupportProvider):
    def __configurationError(self, settings):
        raise ProviderConfigurationError('SUPPORT_SETTINGS', settings, 'https://gitlab.domain.tld•projectId•APIToken')

    def __init__(self):
        settings = getenv('SUPPORT_SETTINGS')
        try:
            instanceUrl,projectId, token = settings.split('•')
        except ValueError:
            self.__configurationError(settings)

        try:
            URLValidator(instanceUrl)
        except ValidationError :
            __configurationError(settings)

        if(not unicode(projectId).isnumeric() or  token == ''):
            __configurationError(settings)

        self.API = GitlabAPI(instanceUrl, projectId, token)

        if(self.API.get('issues').status_code != 200):
            __configurationError(settings)



    def supportRequest(self, subject, issue, username, page, instance):
        quotedIssue = '>'+issue.replace('\n', '\n>')
        description = u'''
# Rapport utilisateur

{issue}

# Information techniques

+ Utilisateur•ice : `{username}`
+ Instance : `{instance}`
+ Page : [{page}]({url})
'''.format(issue=quotedIssue, username=username, instance=instance,
           page=re.sub('^https?://[^/]*', '', page), url=page)

        labels = 'inAppSupport,{username},{instance}'.format(username=username, instance=instance)

        return (self.API.post('issues', {
            'title': subject,
            'description': description,
            'labels' : labels,
            'confidential': True,
        })['iid'], '')
