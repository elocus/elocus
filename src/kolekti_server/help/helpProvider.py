# -*- coding: utf-8 -*-

# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr> (<year>)
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from abc import ABCMeta, abstractmethod
from os import getenv
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

class ProviderConfigurationError(Exception):
    def __init__(self, key, value, expect):
        super(Exception, self).__init__("Variable d'environnement incorrecte : '{key}' devrait être {expect} et non '{value}'"
                                        .format(key=key, value=value, expect=" ou ".join(["'"+v+"'" for v in expect.split('|')])))
        self.key=key

class DocumentationProvider:
    __metaclass__ = ABCMeta

    @classmethod
    def factory(cls):
        provider = getenv('DOCUMENTATION_PROVIDER')
        if(provider == 'GITLAB'):
            from gitlabHelp import GitlabDocumentation
            return GitlabDocumentation()
        else:
            raise ProviderConfigurationError('DOCUMENTATION_PROVIDER', provider, "GITLAB")

    # This method should return a dictionnary :
    # { 'FAQ' : {
    #           'url' : 'https://...'
    #           'contents' : [
    #               {'title' : 'blabla', 'contents':'some html'},
    #                ]
    #           },
    #  'Documentation' : {
    #           'url' : 'https://...'
    #           'contents' : [
    #               {'title' : 'blabla', 'url':'https://...'},
    #                ]
    #           }
    # }
    @abstractmethod
    def getDocumentation(self):
        pass

class SupportProvider:
    __metaclass__ = ABCMeta

    @classmethod
    def factory(cls):
        provider = getenv('SUPPORT_PROVIDER')
        if(provider == ''  or provider is None):
            return None
        elif(provider == 'GITLAB'):
            from gitlabHelp import GitlabSupport
            return GitlabSupport()
        else:
            raise ProviderConfigurationError('SUPPORT_PROVIDER',provider, "GITLAB|''")

    # This method should report an issue
    # returns a tuple (id, URL) representing the issue. The URL can be empty
    @abstractmethod
    def supportRequest(self, subject, issue, username, page, instance):
        pass

class HelpProvider(SupportProvider, DocumentationProvider):
    def __init__(self):
        self.documentationProvider = DocumentationProvider.factory()
        self.SupportProvider = SupportProvider.factory()

    def getDocumentation(self):
        return self.documentationProvider.getDocumentation()

    def supportRequest(self, dst, subject, issue, username, page, instance):
        (iid,url) = self.SupportProvider.supportRequest(subject, issue, username, page, instance)

        shortanswertext = u'Votre demande "{subject}" à été enregistrée sous le numéro {iid}'.format(subject=subject.encode('utf-8'),iid=iid)
        shortanswerhtml = u'<p>'+shortanswertext
        if url != '':
            shortanswertext += u", vous pouvez y accéder à l'adresse {}".format(url)
            shortanswerhtml += u', vous pouvez y accéder en cliquant <a href="{}">ici</a>'.format(url)
        shortanswertext += u"."
        shortanswerhtml += u".</p>"

        mail_params = {
            'shortanswertext': shortanswertext,
            'shortanswerhtml': shortanswerhtml,
            'request': issue.encode('utf-8'),
            'username': username.capitalize(),
        }

        mailSubject = u'[elocus] Votre demande de support numéro {}'.format(iid)

        text_content = u"""
        Bonjour %(username)s,

        %(shortanswertext)s

        Nous faisons au mieux pour résoudre votre problème et revenir vers vous au plus vite.

        Pour rappel, voici votre demande :

        %(request)s

        L'équipe elocus.
        """ % mail_params

        html_content = u"""
        <p>Bonjour %(username)s,</p>

        %(shortanswerhtml)s

        <p>Nous faisons au mieux pour résoudre votre problème et revenir vers vous au plus vite.</p>

        <p>Pour rappel, voici votre demande :</p>

        <blockquote cite=>
        %(request)s
        </blockquote>

        <p>L'équipe elocus.</p>
        """ % mail_params
        msg = EmailMultiAlternatives(mailSubject, text_content, settings.DEFAULT_FROM_EMAIL, [dst])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    def supportAvailable(self):
        return self.SupportProvider is not None
