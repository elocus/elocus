from django.conf.urls import  include, url
from views import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'^$', ElocusHomeView.as_view(), name='elocushome'),
    url(r'^public/(?P<hashid>[0-9a-f]+)/$', ElocusReportPublicView.as_view(), name='elocusreportpublic'),
    url(r'^public/(?P<hashid>[0-9a-f]+)/sharelink/$', ElocusReportPublicShareUrlView.as_view(), name='elocusreportpublicshareurl'),
    url(r'^public/(?P<hashid>[0-9a-f]+)/odt/$', ElocusPublicOdtView.as_view(), name='elocusreportpublicodt'),
    url(r'^public/(?P<hashid>[0-9a-f]+)/pdf/$', ElocusPublicPdfView.as_view(), name='elocusreportpublicpdf'),
    url(r'^public/(?P<hashid>[0-9a-f]+)/doc/$', ElocusPublicDocView.as_view(), name='elocusreportpublicdoc'),
    url(r'^public/(?P<hashid>[0-9a-f]+)/img/(?P<path>.*)', ElocusPublicImgView.as_view(), name='elocusreportpublicimg'),
    url(r'^(?P<project>[^/\?]+)/', include([
        url(r'^create/$', ElocusReportCreateView.as_view(), name='elocusreportcreate'),        
        url(r'^refparameters/$', ElocusRefParametersView.as_view(), name='elocusrefparameters'),
        url(r'^communes/$', ElocusCommunesView.as_view(), name='elocuscommunes'),
        url(r'^referentiels/$', ElocusReferentielsView.as_view(), name='elocusreferentiels'),

        
        url(r'^(?P<report>[^/\?]+)/', include([
            url(r'^$', ElocusReportView.as_view(), name='elocusreport'),
            
            url(r'^sharelink/$', ElocusReportShareUrlView.as_view(), name='elocusreportshareurl'),
            url(r'^share/$', ElocusReportShareView.as_view(), name='elocusreportshare'),
            url(r'^delete/$', ElocusReportDeleteView.as_view(), name='elocusreportdelete'),
            url(r'^publish/$', ElocusReportPublishView.as_view(), name='elocusreportpublish'),
            url(r'^update/$', ElocusReportUpdateView.as_view(), name='elocusreportupdate'),
            
            url(r'^analysis/$', ElocusReportAnalysisView.as_view(), name='elocusreportanalysis'),
            url(r'^topicsize/$', ElocusReportTopicSizeView.as_view(), name='elocusreporttopicsize'),
            url(r'^star/$', ElocusReportStarView.as_view(), name='elocusreportstar'),
            url(r'^hide/$', ElocusReportHideView.as_view(), name='elocusreporthide'),
            url(r'^chart/$', ElocusReportChartView.as_view(), name='elocusreportchart'),
            url(r'^description/$', ElocusReportDescriptionView.as_view(), name='elocusreportdescription'),
            url(r'^topic/save/$', ElocusTopicSaveView.as_view(), name='elocustopicsave'),
            url(r'^odt/$', ElocusOdtView.as_view(), name='elocusodt'),
            url(r'^pdf/$', ElocusPdfView.as_view(), name='elocuspdf'),
            url(r'^doc/$', ElocusDocView.as_view(), name='elocusdoc'),
            url(r'^odtgen/$', ElocusOdtGenView.as_view(), name='elocusodtgen'),
            url(r'^usertopic/(?P<topic>[^/\?]+)/', include([
                url(r'^newimage/$', ElocusUserTopicNewImageView.as_view(), name="elocus_user_topic_new_image"),
                url(r'^newtext/$', ElocusUserTopicNewTextView.as_view(), name="elocus_user_topic_new_text"),
                url(r'^delete/$', ElocusUserTopicDeleteView.as_view(), name="elocus_user_topic_delete"),
            ]))
        ]))
    ]))
]


            

