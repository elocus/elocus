# -*- coding: utf-8 -*-

#     kOLEKTi : a structural documentation generator
#     Copyright (C) 2007-2016 Stéphane Bonhomme (stephane@exselt.com)
import logging
logger = logging.getLogger('kolekti.'+__name__)

from django import forms
from django.utils.translation import ugettext_lazy as _
from registration.forms import RegistrationForm
from django.utils.text import get_valid_filename

from kserver.models import Project

class elocusRegistrationForm(RegistrationForm):
    pass
    # cgv = forms.BooleanField(
    #     widget = forms.CheckboxInput,
    #     label=_(u'I have read and agree to the Terms of Service'),
    #     error_messages={'required': _("You must agree to the terms to register")}
    #     )
