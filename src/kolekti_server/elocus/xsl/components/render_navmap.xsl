<?xml version="1.0" encoding="utf-8"?>
<!--
    kOLEKTi : a structural documentation generator
    Copyright (C) 2007 Stéphane Bonhomme (stephane@exselt.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="html"
    version="1.0">


  <xsl:template match="/html:div">
    <html>
      <head>
        <link rel='stylesheet' type="text/css" href="{$static}/css/elocus-rasterize.css"/>
	    <link rel='stylesheet' type="text/css" href="{$static}/css/components/navmap.css"/>
	    <script src="{$static}/jquery.js"/>
        <script src="{$static}/js/locale_fr.js"/>
	    <script src="{$static}/d3.v4.min.js"/>
        <script src="https://d3js.org/d3-scale-chromatic.v1.min.js"></script>
	    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wicket/1.3.2/wicket.min.js"></script>
	    <script src="{$static}/js/startswith.js"/>
        <script src="{$static}/js/components/navmap_functions.js"/>
	<base href="{$static}"/>
      </head>
      <body data-theme="{$theme}">
        <div class="navigation-map" id="navmap">
          <div class="map-container"></div>
          <xsl:copy-of select="*"/>
        </div>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
