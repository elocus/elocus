<?xml version="1.0" encoding="utf-8"?>
<!--
    kOLEKTi : a structural documentation generator
    Copyright (C) 2007 Stéphane Bonhomme (stephane@exselt.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"   
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml" 
    exclude-result-prefixes="html"
    version="1.0">
  
  <xsl:template match="/libs">
    <libs>
      <css>
        <xsl:choose>
          <xsl:when test="$debug = 'True'">
	        <link rel="stylesheet/less" type="text/css" href="{$static}less/components/map.less"/>
	      </xsl:when>
          <xsl:otherwise>
	        <link rel="stylesheet" type="text/css" href="{$static}css/components/map.css"/>
          </xsl:otherwise>
        </xsl:choose>
	    <link rel="stylesheet" href="https://npmcdn.com/leaflet@0.7.7/dist/leaflet.css" />
	
	    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />
      </css>
      <scripts>
        <!--	<script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>-->
	    <script src="https://npmcdn.com/leaflet@0.7.7/dist/leaflet.js"></script>
	    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
	    <!--	  <script src="{$static}leaflet.min.js"/>-->
	    <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.2.0/leaflet-omnivore.min.js'></script>
	    <script src="{$static}js/components/map.js"/>
      </scripts>
    </libs>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-map']" mode="topictitle"/>
  <xsl:template match="html:div[@class='kolekti-component-map']" mode="topicbody">
    <xsl:call-template name="kolekti-component-body">
      <xsl:with-param name="content">
	<div class="lfcontainer">
	  <div class="leafletmap" data-geojson="{string(.//html:div[@class='kolekti-sparql-result-json'])}"  style="width: 100%; height: 400px" id="{generate-id()}">
	  </div>
	  
	</div>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="html:div[@class='kolekti-component-map']" mode="topicpanelinfo">
    <div class="lfcontainer">
      <div class="leafletmappanel" data-geojson="{string(.//html:div[@class='kolekti-sparql-result-json'])}"  style="width: 100%; height: 400px" id="{generate-id()}">
      </div>
      
    </div>
  </xsl:template>
  <xsl:template match="html:div[@class='kolekti-component-map']" mode="topicpanelaction"/>
  <xsl:template match="html:div[@class='kolekti-component-map']" mode="topicpanelbutton"/>
  <xsl:template match="html:div[@class='kolekti-component-map']" mode="topicpanelbody"/>

</xsl:stylesheet>
