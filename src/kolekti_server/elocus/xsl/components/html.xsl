<?xml version="1.0" encoding="utf-8"?>
<!--
    kOLEKTi : a structural documentation generator
    Copyright (C) 2007 Stéphane Bonhomme (stephane@exselt.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="html"
    version="1.0">

  <xsl:template match="/libs">
    <libs>
      <css>
      </css>
      <scripts>        
        <script type="text/javascript" src="{$static}js/components/html.js"></script>
      </scripts>
    </libs>
  </xsl:template>


  <xsl:template match="html:div[@class='kolekti-sparql']" mode="htmlcomponent">
    <xsl:apply-templates select=".//*[@class='kolekti-sparql-result-template']/node()" mode="htmlcomponent"/>
  </xsl:template>

  <xsl:template match="node()|@*" mode="htmlcomponent">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="htmlcomponent"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="html:img[starts-with(@src, '/')]" mode="htmlcomponent">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="src">
        <xsl:if test="$share='False'">
          <xsl:text>/</xsl:text>
          <xsl:value-of select="$project"/>
          <xsl:text>/releases/</xsl:text>
          <xsl:value-of select="$report"/>
        </xsl:if>
        <xsl:value-of select="@src"/>
      </xsl:attribute>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-html']" mode="topictitle"/>
  <xsl:template match="html:div[@class='kolekti-component-html']" mode="topicbody">
    <xsl:call-template name="kolekti-component-body">
      <xsl:with-param name="content">
	<div class='kolekti-component-html'>
	  <xsl:apply-templates mode="htmlcomponent"/>
	</div>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-html']" mode="topicpanelinfo"/>
  <xsl:template match="html:div[@class='kolekti-component-html']" mode="topicpanelaction"/>
  <xsl:template match="html:div[@class='kolekti-component-html']" mode="topicpanelbutton"/>
  <xsl:template match="html:div[@class='kolekti-component-html']" mode="topicpanelbody"/>

</xsl:stylesheet>
