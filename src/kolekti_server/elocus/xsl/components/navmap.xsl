<?xml version="1.0" encoding="utf-8"?>
<!--
    kOLEKTi : a structural documentation generator
    Copyright (C) 2007 Stéphane Bonhomme (stephane@exselt.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="html"
    version="1.0">

  <xsl:template match="/libs">
    <libs>
      <css>
        <xsl:choose>
          <xsl:when test="$debug = 'True'">
	        <link rel="stylesheet/less" type="text/css" href="{$static}less/components/navmap.less"/>
          </xsl:when>
          <xsl:otherwise>
	        <link rel="stylesheet" type="text/css" href="{$static}css/components/navmap.css"/>
          </xsl:otherwise>
        </xsl:choose>
      </css>
      <scripts>
	<script src="https://d3js.org/d3-scale-chromatic.v1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wicket/1.3.2/wicket.min.js"></script>
	<script type="text/javascript" src="{$static}js/components/navmap.js"></script>
      </scripts>
    </libs>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-navmap']" mode="topictitle"/>
  <xsl:template match="html:div[@class='kolekti-component-navmap']" mode="topicbody">
    <div class="navigation-map" id="navmap">
      <div class="map-container"></div>
      <div class="map-data">
	<xsl:apply-templates select="(ancestor::html:div[@class='section']|ancestor::html:body)[last()]/html:div[@class='section']/html:div[@class='topic']//html:div[@class='kolekti-component-navitem']" mode="component_navmap"/>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-navitem']" mode="component_navmap">
    <div class="map-layer"
	 data-layer='{.//html:div[@class="kolekti-sparql-result-json"]}'>
      <xsl:attribute name="data-link">
	<xsl:choose>
	  <xsl:when test="count(ancestor::html:div[@class='section']) = 1">
	    <xsl:choose>
	      <xsl:when test="$share='False'">
		<xsl:text>/elocus/</xsl:text>
		<xsl:value-of select="$project"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="$report"/>
		<xsl:text>/?section=</xsl:text>
		<xsl:value-of select="ancestor::html:div[@class='section'][1]/@id"/>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:text>?section=</xsl:text>
		<xsl:value-of select="ancestor::html:div[@class='section'][1]/@id"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:otherwise>#<xsl:value-of select="ancestor::html:div[@class='section'][1]/@id"/></xsl:otherwise>
	</xsl:choose>
      </xsl:attribute>
    </div>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-navmap']" mode="topicpanelinfo"/>
  <xsl:template match="html:div[@class='kolekti-component-navmap']" mode="topicpanelaction"/>
  <xsl:template match="html:div[@class='kolekti-component-navmap']" mode="topicpanelbutton"/>
  <xsl:template match="html:div[@class='kolekti-component-navmap']" mode="topicpanelbody"/>

</xsl:stylesheet>
