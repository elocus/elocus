<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"   
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml" 
  
  exclude-result-prefixes="html"
  version="1.0">



  <xsl:template match="text()|@*">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*[namespace-uri(self::*)='http://www.w3.org/1999/xhtml']" priority="-10">
    <xsl:element name="{local-name()}" namespace="">
      <xsl:apply-templates select="node()|@*"/>
    </xsl:element>
  </xsl:template>
  

  
  <xsl:template name="map">
    <div class="navigation-map" id="navmap">
      
    </div>
    <div class="navigation-data">
      <xsl:apply-templates select="html:div[@class='section']/html:div[@class='topic']//html:div[@class='kolekti-component-navigation-map']"/>
    </div>
    
    <script type="text/javascript" src="/static/jquery.js"></script>
    <script src="/static/d3.v4.min.js"></script>

    <!--<script type="text/javascript" src="/static/d3.min.js"></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wicket/1.3.2/wicket.min.js"></script>

    <script type="text/javascript" src="/static/js/elocus-report-map.js"></script>
  </xsl:template>

  <xsl:template match="html:div[@class='kolekti-component-navigation-map']">
    <div class="map-layer"
	 data-layer='{.//html:div[@class="kolekti-sparql-result-json"]}'
	 data-link = '/elocus/report/?release={$path}&amp;section={ancestor::html:div[@class="section"]/@id}'>
    </div>
  </xsl:template>
</xsl:stylesheet>  
