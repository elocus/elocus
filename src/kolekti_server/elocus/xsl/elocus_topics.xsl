<?xml version="1.0" encoding="utf-8"?>
<!--
    kOLEKTi : a structural documentation generator
    Copyright (C) 2007 Stéphane Bonhomme (stephane@exselt.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    -->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:exsl="http://exslt.org/common"
    exclude-result-prefixes="html"
    version="1.0">


  <xsl:output  method="html"
               indent="yes"
	       omit-xml-declaration="yes"
	       />


  
  <xsl:param name="path"/>
  <xsl:param name="section" select="''"/>
  <xsl:param name="share" select="'False'"/>


  
  <xsl:template match = "html:div[@class='topic']" >

    <xsl:if test="$share='False' or not(@data-hidden='yes')">
      <xsl:variable name="w">
	<xsl:choose>
	  <xsl:when test="@data-display-size = 2">12</xsl:when>
	  <xsl:otherwise>6</xsl:otherwise>
	</xsl:choose>
      </xsl:variable>
      <div class="col-sm-12 col-lg-{$w} widget">
	<xsl:copy>
	  <xsl:apply-templates select="@*"/>
          <xsl:attribute name="class">
	    <xsl:text>topic</xsl:text>
	    <xsl:if test="@data-hidden = 'yes'">
	      <xsl:text> disabled</xsl:text>
	    </xsl:if>
	  </xsl:attribute>
	  <div class="panel panel-default notitle">
	    <xsl:variable name="heading">
	      <xsl:apply-templates mode="topictitle"/>
	    </xsl:variable>
	    <xsl:if test="exsl:node-set($heading)/*">
	      <xsl:attribute name="class">panel panel-default title</xsl:attribute>
	      <div class="panel-heading">
		<xsl:copy-of select="$heading"/>
	      </div>
	    </xsl:if>

	    <div class="panel-body">
	      <div class="row components">
		<!-- création du corps du topic -->

		<xsl:apply-templates
		    select=".//html:div[starts-with(@class,'kolekti-component-')]"
		  mode="topicbody">
		  <xsl:sort select = "@data-comp-position"/>
		</xsl:apply-templates>
	      </div>
	    </div>
	    <!-- pied de topic : collapse / boutons action-->
	    <div class="panel-footer">
	      <p class="text-right">&#xA0;
	      <xsl:call-template name="topic-controls"/>
	      </p>
	    </div>
	  </div>

	  <!-- modal -->
	  <div class="modal fade modal-topic-details">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title">
		    <xsl:apply-templates mode="topictitle"/>
		  </h4>
		</div>
		<div class="modal-body">
		  <div class="row">
		    <div class="col-md-4">
		      <xsl:apply-templates mode="topicpanelinfo"/>
		      <xsl:if test="$share='False'">
			<hr/>
			<div class="well well-sm">
			  <xsl:apply-templates mode="topicpanelaction"/>
			</div>
		      </xsl:if>
		    </div>
		    <div class="col-md-8">
		      <xsl:apply-templates mode="topicpanelbutton"/>
		      <xsl:apply-templates mode="topicpanelbody"/>
		    </div>
		  </div>
		</div>
		<div class="modal-footer">
		  <xsl:choose>
		    <xsl:when test="$share='True'">
		      <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
		    </xsl:when>
		    <xsl:otherwise>
		      <button type="button" class="btn btn-primary modal-topic-details-ok">Valider</button>
		      <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
		    </xsl:otherwise>
		  </xsl:choose>
		</div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	</xsl:copy>
      </div>
    </xsl:if>
  </xsl:template>





  <xsl:template name="topic-controls">
    <!--
	outputs topic control buttons, called with current element = div.topic
    -->

    <!-- buttons managed with components -->
    <!--
	<span class="btn-group">
      <xsl:apply-templates  select="html:div[starts-with(@class,'kolekti-component-')]"
			    mode="topicpanelbutton"/>

    </span>
    -->
    <span>
      <span class="btn-group hide-topicdetails">

	<xsl:if test=".//html:div[@class='kolekti-component-chart' or @class='kolekti-component-wysiwyg']">
	  <button title="Détails" class="btn btn-xs btn-default  elocus-action-showdetails">
	    <i class="fa fa-info"></i><xsl:text> Détails</xsl:text>
	  </button>
	</xsl:if>

	<xsl:if test="$share='False'">
	  <button title="A la une">
	    <xsl:attribute name="class">
	      <xsl:text>btn btn-xs btn-default  elocus-action-star </xsl:text>
	      <xsl:choose>
		<xsl:when test="ancestor-or-self::html:div[@class='topic'][@data-star]">btn-warning</xsl:when>
		<xsl:otherwise>btn-default</xsl:otherwise>
	      </xsl:choose>
	    </xsl:attribute>


	    <i class="fa fa-star-o"></i>
	  </button>

	  <button title="Masquer">
	    <xsl:attribute name="class">
	      <xsl:text>btn btn-xs elocus-action-hide </xsl:text>
	      <xsl:choose>
		<xsl:when test="@data-hidden = 'yes'">
		  <xsl:text>btn-primary ishidden</xsl:text>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:text>btn-default</xsl:text>
		</xsl:otherwise>
	      </xsl:choose>
	    </xsl:attribute>
	    <i class="fa fa-eye-slash"></i>
	    <i class="fa fa-eye"></i>
	  </button>

          <xsl:if test="@data-user">
	    <button title="Supprimer...">
	      <xsl:attribute name="class">
	        <xsl:text>btn btn-xs elocus-action-delete-topic </xsl:text>
		<xsl:text>btn-default</xsl:text>
	      </xsl:attribute>
	      <i class="fa fa-trash"></i>
	  </button>
          </xsl:if>

	  <button title="Insérer...">
	    <xsl:attribute name="class">
	      <xsl:text>btn btn-xs elocus-action-insert-topic </xsl:text>
		  <xsl:text>btn-default</xsl:text>
	    </xsl:attribute>
	    <i class="fa fa-plus"></i>
	  </button>
	</xsl:if>

	<!--
	<button title="Réduire" class="btn btn-xs btn-default  elocus-action-reduce">
	    <i class="fa fa-toggle-left"></i>
	</button>
	<button title="Étendre" class="btn btn-xs btn-default  elocus-action-extend">
	  <i class="fa fa-toggle-right"></i>
	</button>
	-->

	<!--
	    <xsl:apply-templates select="html:div[starts-with(@class,'kolekti-component-')]"
	    mode="topicpanelaction"/>
	-->
      </span>
    </span>
  </xsl:template>

  <xsl:template match="html:img/@src">
    <xsl:attribute name="src">
      <xsl:value-of select="$path"/>
      <xsl:value-of select="."/>
    </xsl:attribute>
  </xsl:template>

</xsl:stylesheet>
