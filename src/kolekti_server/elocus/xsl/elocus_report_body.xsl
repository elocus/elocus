<?xml version="1.0" encoding="utf-8"?>
<!--
    kOLEKTi : a structural documentation generator
    Copyright (C) 2007 Stéphane Bonhomme (stephane@exselt.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


-->
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:exsl="http://exslt.org/common"
  exclude-result-prefixes="html"
  version="1.0">


  <xsl:output  method="html"
               indent="yes"
	       omit-xml-declaration="yes"
	       />

  <xsl:param name="project"/>
  <xsl:param name="report"/>
  <xsl:param name="section" select="''"/>
  <xsl:param name="share" select="'False'"/>
  <xsl:param name="theme"/>

  <xsl:include href="elocus_components.xsl"/>
  <xsl:include href="elocus_topics.xsl"/>

  <xsl:template match="text()|@*">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*[namespace-uri(self::*)='http://www.w3.org/1999/xhtml']" priority="-10">
    <xsl:element name="{local-name()}" namespace="">
      <xsl:apply-templates select="node()|@*"/>
    </xsl:element>
  </xsl:template>


  <xsl:template match="html:h1">
    <xsl:element name="h{count(ancestor::html:div[@class='section']) + 1}" namespace="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="@*|node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match = "html:div[@class='section']">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="html:h1"/>
      <div class="section-content niv{count(ancestor::html:div[@class='section']) + 1}" id="section_{@id}" aria-multiselectable="true">
	  <xsl:apply-templates select="html:div"/>
      </div>
    </xsl:copy>
  </xsl:template>

  <xsl:template match = "html:div[@class='section'][count(ancestor::html:div[@class='section']) = 0]">
    <xsl:copy>
      <xsl:apply-templates select="@id"/>
      <div>
	<xsl:apply-templates select="html:h1"/>
      </div>
      <p></p>
      <div class="row intro-topics topics">
          <div class="widgets-wrapper">
    	    <xsl:apply-templates select="html:div[@class='topic']"/>
          </div>
      </div>
      <div class="panel-group" role="tablist" id="section_{@id}" aria-multiselectable="true">
	<xsl:apply-templates select="html:div[@class='section']"/>
      </div>
    </xsl:copy>
  </xsl:template>

  <xsl:template match = "html:div[@class='section'][count(ancestor::html:div[@class='section'])=1]">
    <xsl:if test="$share='False' or html:div[@class='topic'][not(@data-hidden='yes')]">
      <div class="panel panel-default" id="{@id}">
	<div class="panel-heading" role="tab" id="heading_{@id}">
	  <h3 class="panel-title {html:h1/@class}">
	    <a data-toggle="collapse" href="#collapse_section_{@id}" aria-controls="collapse_section_{@id}" data-parent="#section_{ancestor::html:div[@class='section']/@id}" >
	      <xsl:apply-templates select="html:h1/node()"/>
	    </a>
	  </h3>
	</div>
	<div  class="panel-collapse collapse section-content" role="tabpanel" aria-labelledby="heading_{@id}" id="collapse_section_{@id}">
	  <div class="panel-body topics">
          <div class="widgets-wrapper">
    	    <xsl:apply-templates select="html:div"/>
          </div>
	  </div>
	</div>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="node()|@*" mode="topicbody">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>




</xsl:stylesheet>
