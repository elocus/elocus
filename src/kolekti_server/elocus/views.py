# -*- coding: utf-8 -*-

#     kOLEKTi : a structural documentation generator
#     Copyright (C) 2007-2013 Stéphane Bonhomme (stephane@exselt.com)
import os
import json
import uuid
import base64
from lxml import etree as ET
from lxml import html as EH
from copy import deepcopy
from time import strftime
import logging
logger = logging.getLogger('kolekti.'+__name__)

from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect, StreamingHttpResponse
from django.conf import settings
from django.shortcuts import render, render_to_response
from django.views.generic import View,TemplateView, ListView
from django.views.generic.base import TemplateResponseMixin
from django.views.static import serve
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.views.decorators.http import condition
from django.template.loader import get_template
from django.template import Context
from django.utils.text import get_valid_filename

# kolekti imports
from tasks import create_report, publish_report, update_report
from kserver.forms import UploadFileForm

from kolekti.publish import Releaser
from kolekti.publish_queries import kolektiSparQL

from kserver.views import kolektiPublicMixin, LoginRequiredMixin
from kserver.models import ShareLink, UserProject
from upload_data import upload_data_available

ns = {'namespaces':{'html':'http://www.w3.org/1999/xhtml'}}

class ElocusMixin(kolektiPublicMixin):
    def render_to_response(self, context):
        context.update({'theme': settings.KOLEKTI_THEME})
        context.update({'DEBUG': settings.DEBUG})
        context.update({'request': self.request})
        return super(ElocusMixin, self).render_to_response(context)

    def get_context_data(self, data={}, **kwargs):
        context= {
            'theme': settings.KOLEKTI_THEME,
            'DEBUG': settings.DEBUG,
            'request': self.request,
        }
        context.update(data)
        return context


    def get_report_body(self, assembly, project, report, section=None, share = False):
        if section is None:
            xsl = self.get_xsl('elocus_report_home')
            body = assembly.getroot()
        else:
            xsl = self.get_xsl('elocus_report_body')
            body = assembly.xpath('/html:html/html:body/html:div[@class="section"][@id="%s"]'%section,
                                  **ns)
                
        content = ''.join([str(xsl(t, project="'%s'"%project, report="'%s'"%report,
                                   share="'%s'"%share, theme="'%s'"%settings.KOLEKTI_THEME))
                           for t in body])
        return content

    def get_report_menu(self, assembly, project, report, section=None, share = False):
        body = assembly.xpath('/html:html/html:body/*', **ns)
        xsl = self.get_xsl('elocus_report_menu')
        content = ''.join([str(xsl(t, project="'%s'"%project, report="'%s'"%report,
                                   share="'%s'"%share, theme="'%s'"%settings.KOLEKTI_THEME))
                           for t in body])
        return content

    def get_report_sections(self, assembly):
        body = assembly.xpath('/html:html/html:body/html:div[@class="section"]', **ns)
        for section in body:
            yield {
                'id':section.get('id'),
                'title':section.xpath('string(html:h1)', **ns)
            }


    def get_report_section_title(self, assembly, section):
        sect_title = assembly.xpath('string(/html:html/html:body/html:div[@class="section"][@id="%s"]/html:h1[1])'%section, **ns)
        return sect_title

    def get_report_libs(self, assembly, section = None, starred = False):
        if section is None:
            root = assembly.xpath('/html:html/html:body',
                                   **ns)
        else:
            root = assembly.xpath('/html:html/html:body/html:div[@class="section"][@id="%s"]'%section, **ns)

        libs = {'css':'', 'scripts':''}
        for component in self.collect_components(root[0], starred):
            xsl = self.get_xsl('components/%s'%component)
            xlibs = xsl(self.parse_string('<libs/>'), theme="'%s'"%settings.KOLEKTI_THEME, static="'%s'"%settings.STATIC_URL, debug = "'%s'"%str(settings.DEBUG)).getroot()
            if not xlibs is None:
                for css in xlibs.xpath('html:css/*',**ns):
                    libs['css'] += ET.tostring(css, method="html")+'\n'
                for scr in xlibs.xpath('html:scripts/*',**ns):
                    libs['scripts'] += ET.tostring(scr, method="html")+'\n'
        return libs

    def collect_components(self, root, starred):
        comps = set()
        if starred:
            xpathexpr = ".//html:div[@class='topic'][@data-star='yes']//html:div[starts-with(@class,'kolekti-component-')]/@class|./html:div[@class='topic']//html:div[starts-with(@class,'kolekti-component-')]/@class"
        else:
            xpathexpr = ".//html:div[starts-with(@class,'kolekti-component-')]/@class"
        for c in root.xpath(xpathexpr,**ns):
            comps.add(c[18:])
        return list(comps)

    def populate_sparql_data(self, assembly, ospath, section = None):
        if section is None:
            path_query = '//html:div[@class="kolekti-sparql"]'
        else:
            path_query = '//html:div[@id="%s"]//html:div[@class="kolekti-sparql"]'%(section,)
        for query in assembly.xpath(path_query, **ns):
            qid = query.get('id', None)
            if not qid is None:
                resdiv = query.xpath('.//html:div[@class="kolekti-sparql-result"]', **ns)
                if len(resdiv):
                    resjson = ET.SubElement(resdiv[0],'{http://www.w3.org/1999/xhtml}div', attrib = {"class":"kolekti-sparql-result-json"})
                    with open(os.path.join(ospath, 'json', qid+'.json'),'r') as jsonf:
                        resjson.text = jsonf.read()

class ElocusPublicMixin(ElocusMixin):

    def set_project(self, username, project):
        project_path = os.path.join(settings.KOLEKTI_BASE, username, project)
        super(ElocusPublicMixin, self).set_project(project_path)

    def get_assembly(self, username, project, report):
        assembly_dir = "/".join(["/releases", report, "sources","fr","assembly"])
        assembly_path = "/".join([assembly_dir, report + "_asm.html"])
        self.set_project(username, project)
        assembly = self.parse(assembly_path)
        return assembly

    def get_job(self, username, project, report):
        job_path = "/".join(["/releases", report, "kolekti", "publication-parameters", report + "_asm.xml"])
        self.set_project(username,project)
        return self.parse(job_path)

    def get_report_name(self, username, project, report):
        job = self.get_job(username, project, report)
        return job.xpath("string(/job/@pubname)")



class ElocusPrivateMixin(LoginRequiredMixin, ElocusMixin):

    def set_project(self, project):
        project_path = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        super(ElocusPrivateMixin, self).set_project(project_path)


    def get_project_reports(self, project):
        releasespath = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project, 'releases')
        for report in os.listdir(releasespath):
            try:
                job = ET.parse(os.path.join(releasespath, report, 'kolekti', 'publication-parameters', report + '_asm.xml' ))
            except:
                logger.exception('could not parse release assembly')
                continue
            report_title = job.xpath("string(/job/@pubname)")
            report_url = reverse('elocusreport', kwargs = {"project":project, "report":report})

            yield {'title': report_title, 'url': report_url, 'dir':report}
        return

    def get_assembly(self, project, report):
        assembly_dir = "/".join(["/releases", report, "sources","fr","assembly"])
        assembly_path = "/".join([assembly_dir, report + "_asm.html"])
        self.set_project(project)
        assembly = self.parse(assembly_path)
        return assembly

    def get_job(self, project, report):
        job_path = "/".join(["/releases", report, "kolekti", "publication-parameters", report + "_asm.xml"])
        self.set_project(project)
        return self.parse(job_path)

    def write_assembly(self, assembly, project, report, extra_commit = []):
        report_path = "/".join(["/releases", report, "sources", "fr", "assembly", report + "_asm.html"])
        self.set_project(project)
        self.xwrite(assembly, report_path, sync = False)
        if self.is_shared(project, report):
            try:
                self.syncMgr.commit([report_path] + extra_commit, "report update")
            except:
                logger.exception('commit failed')

    def _checkout(self, release):
        pass

    def get_report_name(self, project, report):
        job = self.get_job(project, report)
        return job.xpath("string(/job/@pubname)")

    def get_report_list(self, project):
        self.set_project(project)
        reports = self.get_directory('/releases')
        try:
            for report in reports:
                report.update({'label':self.get_report_name(report.get('name'))})
        except:
            logger.exception('')
        return reports


    def assembly_user_vars(self, assembly, section = None):
        varset = set()
        if section is None:
            varxpath = '/html:html/html:body//html:var[startswith(@class,"uservar:")]'
        else:
            varxpath = '/html:html/html:body//html:div[@class="section"][@id="%s"]//html:var[startswith(@class,"uservar:")]'%section

        var_list = assembly.xpath(varxpath, **ns)

        for v in var_list:
            varset.add(v.get('class')[8:])

        return list(varset)

    def toc_user_vars(self, toc):
        mods = []
        varset = set()
        for refmod in toc.xpath('/html:html/html:body//html:a[@rel="kolekti:topic"]',
                        **ns):
            moduri = refmod.get('href').split('?')[0]
            if not moduri in mods:
                mods.append(moduri)
                xmod = self.parse(moduri)
                varxpath = '/html:html/html:body//html:var[starts-with(@class,"uservar:")]'
                var_list = xmod.xpath(varxpath, **ns)
                for v in var_list:
                    varset.add(v.get('class')[8:])
        return list(varset)

    def is_shared(self, project, report):
        self.set_project(project)
        status = self.syncMgr.statuses('/releases/' + report)
        return not(len(status['unversioned']) == 1 and (status['unversioned'][0]['basename'] == report))


class ElocusHomeView(ElocusPrivateMixin, View):
    template_name = "home.html"
    def get(self, request):
        projects = []
        for up in request.user.userproject_set.all():
            p = {'id':up.project.directory,
                 'title' :up.project.name ,
                 'reports':self.get_project_reports(up.project.directory)}
            projects.append(p)

        ariane = [{'label':'eLocus','url':'/'}]


        context = self.get_context_data({
            'projects':projects,
            'upload_enabled': upload_data_available(),
        })
        return self.render_to_response(context)


class ElocusReportCreateView(ElocusPrivateMixin, TemplateView):
    template_name = "home.html"

    def get(self, request, project):
        userproject = UserProject.objects.get(user = self.request.user, project__directory = project)

        self.set_project(project)

        projects = []
        for up in request.user.userproject_set.all():
            p = {'id':up.project.directory,
                 'title' :up.project.description ,
                 'reports':self.get_project_reports(up.project.directory)}
            projects.append(p)

        context = self.get_context_data({
            'projects':projects,
            'project':project,
            'report_create':True,
        })
        return self.render_to_response(context)

    def post(self, request, project):
        self.set_project(project)
        try:
            title = request.POST.get('title','')
            toc = request.POST.get('toc')
            lang = 'fr'

            xjob = self.parse('/kolekti/publication-parameters/report.xml')
            report = get_valid_filename(title)
            reportdir = "%06d_%s"%(request.user.id, report)
            xjob.getroot().set('pubdir', reportdir)
            xjob.getroot().set('pubname', title)
            criteria = xjob.xpath('/job/criteria')[0]

            for uservar in request.POST.keys():
                if uservar[:8] == 'uservar_' and uservar[-4:] == '[id]':
                    ET.SubElement(criteria, 'criterion', attrib={"code":"uservar:%s"%uservar[8:-4],"value":request.POST.get(uservar)})

            r = create_report.delay(project, request.user.username, ET.tostring(xjob), title, toc, lang)

            try:
                events = json.loads(r.get(timeout = 25))
                logger.debug(events)
            except r.TimeoutError as exc:
                logger.exception('get task raised: %r', exc)
                return HttpResponse(json.dumps({'status':'timeout'}),content_type="application/json")

            pp = {"status":"ok", "msg":"report created", "events":events}

        except:
            import traceback
            logger.exception('Create Report Error')
            pp = {"status":"error", "msg":traceback.format_exc()}
        return HttpResponse(json.dumps(pp),content_type="application/json")

class ElocusCommunesView(ElocusPrivateMixin, View):
    def get(self, request, project):
        self.set_project(project)
        referentiel = request.GET.get('referentiel','')
        xtoc = self.parse('/sources/fr/tocs/elocus/'+referentiel)
        sparqlserver = self._project_settings.find('sparql').get('endpoint')
        endpoint = endpoint + xtoc.xpath('string(/html:html/html:head/html:meta[@name="kolekti.sparql.endpoint"]/@content)',**ns)
        sp = kolektiSparQL(sparqlserver)
        communes = sp.get_communes(endpoint)
        return HttpResponse(json.dumps(communes),content_type="application/json")

class ElocusReferentielsView(ElocusPrivateMixin, View):
    def get(self, request, project):
        self.set_project(project)
        try:
            referentiels = self.get_directory('/sources/fr/tocs/elocus')
            return HttpResponse(json.dumps([r.get('name') for r in referentiels]),content_type="application/json")
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

# unused
class ElocusGenerateImagesView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        try:
            report = self.get_report(release_path)

        except:
            import traceback
            logger.exception('erreur lors de conversion des visuels')
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")
        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")

class ElocusReportUpdateView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        self.set_project(project)
        try:
            sparqlserver = self._project_settings.find('sparql').get('endpoint')

            r = update_report.delay(project, request.user.username, report, sparqlserver)
            try:
                r.get(timeout=25)
            except r.TimeoutError as exc:
                logger.exception('get task raised: %r', exc)
                return HttpResponse(json.dumps({'status':'timeout'}),content_type="application/json")

        except:
            import traceback
            logger.exception('** Report Update error')
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")
        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")

class ElocusReportAnalysisView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        topicid =  request.POST.get('topic','')
        data =  request.POST.get('data','')

        try:
            xdata = self.parse_html_string(data)
            assembly = self.get_assembly(project, report)
            ana = assembly.xpath("//html:div[@id = '%s']/html:div[@class='kolekti-component-wysiwyg']"%topicid, **ns)[0]
            for child in ana:
                ana.remove(child)
            for elt in xdata.xpath('/html/body/*'):
                ana.append(elt)
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")


class ElocusReportDescriptionView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        topicid =  request.POST.get('topic','')
        data =  request.POST.get('data','')

        try:
            xdata = self.parse_html_string(data)
            assembly = self.get_assembly(project, report)
            ana = assembly.xpath("//html:div[@id = '%s']/html:div[@class='kolekti-component-description']"%topicid, **ns)[0]
            for child in ana:
                ana.remove(child)
            for elt in xdata.xpath('/html/body/*'):
                ana.append(elt)
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")

class ElocusReportTopicSizeView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        topicid =  request.POST.get('topic','')
        size =  request.POST.get('size','1')
        try:
            assembly = self.get_assembly(project, report)
            topic = assembly.xpath("//html:div[@id = '%s']"%topicid,
                                   **ns)[0]
            topic.set('data-display-size',size)
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")


class ElocusReportStarView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        topicid =  request.POST.get('topic','')
        state =  request.POST.get('state','')
        try:
            assembly = self.get_assembly(project, report)
            topic = assembly.xpath("//html:div[@id = '%s']"%topicid,
                                 **ns)[0]
            if(state == 'true'):
                topic.set('data-star','yes')
            else:
                del topic.attrib['data-star']
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")

class ElocusReportHideView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        topicid =  request.POST.get('topic','')
        state =  request.POST.get('state','')
        try:
            assembly = self.get_assembly(project, report)
            topic = assembly.xpath("//html:div[@id = '%s']"%topicid,
                                 **ns)[0]
            if(state == 'true'):
                topic.set('data-hidden','yes')
            else:
                del topic.attrib['data-hidden']
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")

class ElocusReportChartView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        topicid =  request.POST.get('topic','')
        kind =  request.POST.get('chartkind','bar')
        try:
            assembly = self.get_assembly(project, report)
            chart = assembly.xpath("//html:div[@id = '%s']//html:div[@class='kolekti-component-chart']"%topicid,
                                    **ns)[0]
            chart.set('data-chartkind', kind )
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")
        return HttpResponse(json.dumps({'status':'ok','chart':kind}),
                            content_type="application/json")


class ElocusTopicSaveView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        topicid =  request.POST.get('topic','')
        chartkind =  request.POST.get('chartkind',None)
        chartopts =  request.POST.get('chartopts',None)
        wdata =  request.POST.get('wysiwygdata',None)
        try:
            assembly = self.get_assembly(project, report)
            if not chartkind is None:
                chart = assembly.xpath("//html:div[@id = '%s']//html:div[@class='kolekti-component-chart']"%topicid,
                                        **ns)[0]
                chart.set('data-chartkind', chartkind )

            if not chartopts is None:
                chart = assembly.xpath("//html:div[@id = '%s']//html:div[@class='kolekti-component-chart']"%topicid,
                                        **ns)[0]
                chart.set('data-chartopts', chartopts )

            if not wdata is None:
                try:
                    xdata = self.parse_html_string(wdata)

                    ana = assembly.xpath("//html:div[@id = '%s']/html:div[@class='kolekti-component-wysiwyg']"%topicid, **ns)[0]
                    for child in ana:
                        ana.remove(child)
                    for elt in xdata.xpath('/html/body/*'):
                        ana.append(elt)
                except ET.XMLSyntaxError:
                    pass
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")
        return HttpResponse(json.dumps({'status':'ok'}),
                            content_type="application/json")


class ElocusReportView(ElocusPrivateMixin, View):
    template_name = "report/report.html"
    def get(self, request, project, report):
        user_project = UserProject.objects.get(user = request.user, project__directory = project)
        section = request.GET.get('section')
        reports = self.get_project_reports(project)
        context = {
            "report":report,
            "reports":reports,
            "project":project,
            "projects":request.user.userproject_set.all(),
            }
        try:
            self._checkout('/releases/%s'%report)
            assembly_dir = "/".join(["/releases", report, "sources","fr","assembly"])
            assembly_path = "/".join([assembly_dir, report + "_asm.html"])

            #            assembly_path = "/".join(["/releases",report,"sources","fr","assembly",report+"_asm.html"])

            try:
                assembly = self.get_assembly(project, report)
            except IOError:
                raise Http404
            logger.debug(assembly)

            try:
                self.populate_sparql_data(assembly, self.getOsPath(assembly_dir), section = section)
            except:
                logger.exception('unbale to populate')

            content = self.get_report_body(assembly, project, report, section = section)
            sections = self.get_report_sections(assembly)
            reportname = self.get_report_name(project, report)
            ariane = [{'label':'eLocus','url':'/'},
                      {'label': user_project.project.name,'url':reverse('elocushome')},
                      {'label':reportname,'url':reverse('elocusreport', kwargs={"project": project, "report":report})}]

            if section is None:
                if len(assembly.xpath("/html:html/html:body/html:div[@class='topic']/html:div[@class='kolekti-component-navmap']",
                                           **ns)):
                    libs = self.get_report_libs(assembly, section = section, starred = False)
                else:
                    libs = self.get_report_libs(assembly, section = section, starred = True)
            else:
                libs = self.get_report_libs(assembly, section = section)
                sect_title = self.get_report_section_title(assembly, section)
                ariane.append({'label':sect_title,'url':reverse('elocusreport', kwargs = {'project':project, 'report':report}) + '?section=' + section})

            context.update({
                "shared": self.is_shared(project, report),
                "ariane":ariane,
                "content":content,
                "current":report,
                "reportname":self.get_report_name(project, report),
                "sections":sections,
                "current_section":section,
                "libs":libs,
                "title":reportname,
                })

        except IndexError:
            context.update({
                "content":"",
                "menu":None,
                "libs":{'css':'', 'scripts':''},
                "title":"",
                })

        except:
            logger.exception("Erreur lors de du formatage du rapport")
            raise

        return self.render_to_response(context)

class ElocusReportShareView(ElocusPrivateMixin, View):
    def get(self, request, project, report):
        self.set_project(project)
        try:
            self.syncMgr.add_resource("/releases/%s/sources"%report)
            self.syncMgr.add_resource("/releases/%s/kolekti"%report)
            self.syncMgr.commit(["/releases/%s"%report], "elocus share")
        except:
            logger.exception("cannot share report")
            return HttpResponse(json.dumps({'status':'error'}),content_type="application/json")
        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")

class ElocusReportDeleteView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        self.set_project(project)
        try:
            self.delete_resource('/releases/%s'%report)
        except:
            logger.exception("cannot delete report")
            return HttpResponse(json.dumps({'status':'error'}),content_type="application/json")
        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")

class ElocusReportShareUrlView(ElocusPrivateMixin, View):
    def get(self, request, project, report):
        user_project = UserProject.objects.get(user = request.user, project__directory = project)
        try:
            sl = ShareLink.objects.get(project = user_project, reportname = report)
        except ShareLink.DoesNotExist:
            import md5
            hstr = (unicode(user_project) + project + report).encode('utf-8')
            h = md5.md5(hstr)
            reportid = h.hexdigest()
            sl = ShareLink(project = user_project,
                           reportname = report,
                           hashid = reportid)
            sl.save()
        scheme = "http"
        if settings.SECURE_SSL_REDIRECT:
            scheme = 'https'
        url = "%s://%s%s"%(scheme,request.META['HTTP_HOST'],reverse('elocusreportpublic', kwargs = {'hashid':str(sl.hashid)}))
        return HttpResponse(json.dumps({'url':url}),content_type="application/json")

class ElocusReportPublicShareUrlView(ElocusPublicMixin, View):
    def get(self, request, hashid):
        sl = ShareLink.objects.get(hashid = hashid)
        scheme = "http"
        if settings.SECURE_SSL_REDIRECT:
            scheme = 'https'
        url = "%s://%s%s"%(scheme,request.META['HTTP_HOST'],reverse('elocusreportpublic', kwargs = {'hashid':str(sl.hashid)}))
        return HttpResponse(json.dumps({'url':url}),content_type="application/json")

class ElocusReportPublicView(ElocusPublicMixin, TemplateView):
    template_name = "report/report.html"
    def get(self, request, hashid):
        sl = ShareLink.objects.get(hashid = hashid)
        report = sl.reportname
        project = sl.project.project.directory
        username = sl.project.user.username
        section = request.GET.get('section')

        assembly = self.get_assembly(username, project, report )

        for img in assembly.xpath('//html:img', **ns):
            imgsrc = img.get('src')
            img.set('src', '/elocus/public/' + hashid + '/img/releases/'+ report + imgsrc)
            
        assembly_dir = "/".join(["/releases", report, "sources","fr","assembly"])
        assembly_path = "/".join([assembly_dir, report + "_asm.html"])
        self.populate_sparql_data(assembly, self.getOsPath(assembly_dir), section = section)
        content = self.get_report_body(assembly, project, report, section = section, share = True)
        menu = self.get_report_menu(assembly, project, report, section = section, share = True)
        libs = self.get_report_libs(assembly, section = section)
        title = self.get_report_name(username, project, report)
        
        sections = self.get_report_sections(assembly)

        return self.render_to_response({"content":content,
                                        "public":True,
                                        "current":report,
                                        "report":report,
                                        "reportname":title,
                                        "project":project,
                                        "sections":sections,
                                        "current_section":section,
                                        "menu":menu,
                                        "libs":libs,
                                        "title":title,
                                        "hashid":hashid,
                                    })



class ElocusRefParametersView(ElocusPrivateMixin, View):
    def get(self, request, project):
        self.set_project(project)
        try:
            result = []
            parameters = ET.Element ('uservariables');
            referentiel = request.GET.get('referentiel','')
            xtoc = self.parse('/sources/fr/tocs/elocus/'+referentiel)
            parameters_path = xtoc.xpath('/html:html/html:head/html:meta[@name="kolekti.parameters"]/@content', **ns)
            if len(parameters_path):
                parameters_def = self.parse(parameters_path[0])
                found_parameters = self.toc_user_vars(xtoc)
                for query in parameters_def.xpath('/uservariables/query'):
                    parameters.append(deepcopy(query))
                for param in parameters_def.xpath('/uservariables/variable'):
                    param_name = param.get("name")
                    if param_name in found_parameters:
                        parameters.append(deepcopy(param))

                sparqlserver = self._project_settings.find('sparql').get('endpoint')
                try:
                    endpoint = xtoc.xpath('/html:html/html:head/html:meta[@name="kolekti.uservars.sparql.endpoint"]/@content',**ns)
                    if len(endpoint) == 0:
                        endpoint = xtoc.xpath('/html:html/html:head/html:meta[@name="kolekti.sparql.endpoint"]/@content',**ns)
                    endpoint = str(endpoint[0])
                except IndexError:
                    logger.exception('unable to query parameters : no endpoint')
                    result = []
                    return HttpResponse(json.dumps(result),content_type="application/json")

                sp = kolektiSparQL(sparqlserver)
                sp.instanciate_parameters(parameters, endpoint)
                for param in parameters.xpath('variable'):
                    result.append(
                        {'id':param.get('name'),
                        'label':param.get('label'),
                        'type':param.find('values').get('type',''),
                        'values':[{'name':val.get('label'),'id':val.get('data')} for val in param.xpath('values/value')]}
                        )
        except:
            logger.exception('unable to query parameters')
            result = []
        return HttpResponse(json.dumps(result),content_type="application/json")


class ElocusReportPublishView(ElocusPrivateMixin, View):
    def post(self, request, project, report):
        try:
            doctype = request.POST.get('document_type', 'pdf')
            r = publish_report.delay(project, request.user.username, report, "odt", ['fr'], doctype)
            try:
                events = json.loads(r.get(timeout=25))
            except r.TimeoutError as exc:
                logger.exception('get task raised: %r', exc)
                return HttpResponse(json.dumps({'status':'timeout'}),content_type="application/json")
            return HttpResponse(json.dumps({'status':'ok',
                                            'events':events,
                                            'msg':'publication ready'}),content_type="application/json")

        except:
            import traceback
            logger.exception('Erreur lors de la publication')
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

class ElocusOdtGenView(ElocusPrivateMixin, View):
    def get(self, request, project, report):
        from kolekti.publish import ReleasePublisher
        script = 'odt'
        langs = ['fr']
        project_path = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        release_path = "/releases/%s"%(report,)
        xjob = ET.parse(os.path.join(project_path,"releases",report,"kolekti","publication-parameters",report+"_asm.xml"))
        assembly_dir = os.path.join(project_path, 'releases', report, "sources", "fr", "assembly")
        assembly_path = os.path.join(assembly_dir, report+"_asm.html")
        assembly = ET.parse(assembly_path)
        report+"_asm.html"
        title = assembly.xpath('string(/html:html/html:head/html:title)',**ns)
        jscripts = xjob.getroot().find('scripts')
        for jscript in jscripts:
            if jscript.get('name') != script:
                jscripts.remove(jscript)
        r = ReleasePublisher(release_path, project_path, langs = langs)
        for r in r.publish_assembly(report + "_asm", xjob):
            logger.debug(r)
        return HttpResponse('ok')

class ElocusOdtView(ElocusPrivateMixin, View):
    def get(self, request, project, report):
        root = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        assembly_path = os.path.join(root,'releases', report,"sources","fr","assembly",report+"_asm.html")
        odt_path = os.path.join(root,'releases', report, "report","report","report.odt")
        assembly = ET.parse(assembly_path)

        title = assembly.xpath('string(/html:html/html:head/html:title)',**ns)

        with open(odt_path, 'rb') as document:
            response = HttpResponse(document.read(), content_type='application/vnd.oasis.opendocument.text')
            response['Content-Disposition'] = "attachment; filename=\"%s_%s.odt\""%(title, strftime("%Y%m%d-%H%M"))
        return response

class ElocusDocView(ElocusPrivateMixin, View):
    def get(self, request, project, report):
        root = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        assembly_path = os.path.join(root,'releases', report,"sources","fr","assembly",report+"_asm.html")
        odt_path = os.path.join(root,'releases', report, "report","report","report.doc")
        assembly = ET.parse(assembly_path)

        title = assembly.xpath('string(/html:html/html:head/html:title)',**ns)

        with open(odt_path, 'rb') as document:
            response = HttpResponse(document.read(), content_type='application/msword')
            response['Content-Disposition'] = "attachment; filename=\"%s_%s.doc\""%(title, strftime("%Y%m%d-%H%M"))
        return response

class ElocusPdfView(ElocusPrivateMixin, View):
    def get(self, request, project, report):
        root = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        assembly_path = os.path.join(root,'releases', report,"sources","fr","assembly",report+"_asm.html")
        odt_path = os.path.join(root,'releases', report, "report","report","report.pdf")
        assembly = ET.parse(assembly_path)

        title = assembly.xpath('string(/html:html/html:head/html:title)',**ns)

        with open(odt_path, 'rb') as document:
            response = HttpResponse(document.read(), content_type='application/pdf')
            response['Content-Disposition'] = "attachment; filename=\"%s_%s.pdf\""%(title, strftime("%Y%m%d-%H%M"))
        return response

class ElocusPublicOdtView(ElocusPublicMixin, View):
    def get(self, request, hashid):
        root = None
        sl = ShareLink.objects.get(hashid = hashid)
        report = sl.reportname
        project = sl.project.project.directory
        username = sl.project.user.username
        root = os.path.join(settings.KOLEKTI_BASE, username, project)
        assembly_path = os.path.join(root,'releases', report,"sources","fr","assembly",report+"_asm.html")
        odt_path = os.path.join(root,'releases', report, "report","report","report.odt")
        assembly = ET.parse(assembly_path)
        title = assembly.xpath('string(/html:html/html:head/html:title)',**ns)

        with open(odt_path, 'rb') as document:
            response = HttpResponse(document.read(), content_type='application/vnd.oasis.opendocument.text')
            response['Content-Disposition'] = "attachment; filename=\"%s_%s.odt\""%(title, strftime("%Y%m%d-%H%M"))
        return response

class ElocusPublicPdfView(ElocusPublicMixin, View):
    def get(self, request, hashid):
        root = None
        sl = ShareLink.objects.get(hashid = hashid)
        report = sl.reportname
        project = sl.project.project.directory
        username = sl.project.user.username
        root = os.path.join(settings.KOLEKTI_BASE, username, project)
        assembly_path = os.path.join(root,'releases', report,"sources","fr","assembly",report+"_asm.html")
        pdf_path = os.path.join(root,'releases', report, "report","report","report.pdf")
        assembly = ET.parse(assembly_path)
        title = assembly.xpath('string(/html:html/html:head/html:title)',**ns)

        with open(pdf_path, 'rb') as document:
            response = HttpResponse(document.read(), content_type='application/pdf')
            response['Content-Disposition'] = "attachment; filename=\"%s_%s.pdf\""%(title, strftime("%Y%m%d-%H%M"))
        return response

class ElocusPublicDocView(ElocusPublicMixin, View):
    def get(self, request, hashid):
        root = None
        sl = ShareLink.objects.get(hashid = hashid)
        report = sl.reportname
        project = sl.project.project.directory
        username = sl.project.user.username
        root = os.path.join(settings.KOLEKTI_BASE, username, project)
        assembly_path = os.path.join(root,'releases', report,"sources","fr","assembly",report+"_asm.html")
        doc_path = os.path.join(root,'releases', report, "report","report","report.doc")
        assembly = ET.parse(assembly_path)
        title = assembly.xpath('string(/html:html/html:head/html:title)',**ns)

        with open(doc_path, 'rb') as document:
            response = HttpResponse(document.read(), content_type='application/msword')
            response['Content-Disposition'] = "attachment; filename=\"%s_%s.doc\""%(title, strftime("%Y%m%d-%H%M"))
        return response

class ElocusPublicImgView(ElocusPublicMixin, View):
    def get(self, request, hashid, path):
        root = None
        sl = ShareLink.objects.get(hashid = hashid)
        report = sl.reportname
        project = sl.project.project.directory
        username = sl.project.user.username
        basepath = os.path.join(settings.KOLEKTI_BASE, username, project)
        return serve(request, path, basepath)
        
class ElocusUserTopicNewImageView(ElocusPrivateMixin, View):
    def post(self, request, project, report, topic):
        self.set_project(project)
        list_commit= []
        root = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        release_path = os.path.join(root,'releases', report)
        assembly_path = os.path.join(release_path,"sources","fr","assembly",report+"_asm.html")
        assembly = ET.parse(assembly_path)
        if 'file' in request.POST:
            name = request.POST['name']
            payload = request.POST['file']
            data = base64.decodestring(payload)
        elif 'file' in request.FILES:
            myfile = request.FILES['file']
            name = myfile.name
            data = myfile.read()
        else:
            logger.debug("no file found")
            topic_kind = "description"
            component_content = ""
            return HTTPResponse(json.dumps({'status':'fail',
                                                'msg':traceback.format_exc()}),
                                    content_type="application/json")

        user_resources_path = os.path.join(release_path,"sources","fr","userdata")
        local_user_resources_path = '/'.join(['', 'releases', report ,"sources","fr","userdata"])
        report_shared = self.is_shared(project, report)
        if not os.path.exists(user_resources_path):
            os.makedirs(user_resources_path)
            if report_shared:
                self.syncMgr.add_resource(local_user_resources_path)

        resources_path = os.path.join(release_path,"sources","fr","userdata",self.request.user.username)
        local_resources_path = '/'.join(['', 'releases', report ,"sources","fr","userdata",self.request.user.username])
        if not os.path.exists(resources_path):
            os.makedirs(resources_path)
            if report_shared:
                self.syncMgr.add_resource(local_resources_path)

        with open(resources_path +'/'+ name, mode = "wb") as pf:
            pf.write(data)
        logger.debug("upload file %s/%s", local_resources_path, name)
        if report_shared:
            self.syncMgr.add_resource(local_resources_path +'/'+ name)
        list_commit.append(local_resources_path +'/'+ name)
        component_content = "<img class='img-responsive' src='/sources/fr/userdata/%(username)s/%(filename)s'/>" %  {
            'project':project,
            'report':report,
            'username':request.user.username,
            'filename':name,
            }
        topic_kind = "html"
        id_new_topic = "idm" + str(uuid.uuid1().int)
        source_new_topic = """
        <div class='topic' id='%(idtopic)s' data-user='%(user)s'>
          <div class='topicinfo'></div>
          <div class="kolekti-component-title"></div>
          <div class="kolekti-component-%(kind)s">
            %(content)s
          </div>
        </div>
        """ %{
            'idtopic':id_new_topic,
            'user':self.request.user.username,
            'kind':topic_kind,
            'content':component_content,
            }
        newtopic = ET.fromstring(source_new_topic)


        result = """
        <div class="col-sm-12 col-lg-6 widget">
          <div class="topic" id="%(idtopic)s" data-user="%(user)s">
            <div class="panel panel-default notitle">
              <div class="panel-body">
                <div class="row components">
                  <div class="col-sm-12 col-lg-12">
                    <div class="kolekti-component-html">
                      <img class="img-responsive" src="/%(project)s/releases/%(report)s/sources/fr/userdata/%(username)s/%(filename)s"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-footer">
               <p class="text-right">
                 <span>
                   <span class="btn-group hide-topicdetails"><button title="A la une" class="btn btn-xs btn-default  elocus-action-star btn-default"><i class="fa fa-star-o"/></button><button title="Masquer" class="btn btn-xs elocus-action-hide btn-default"><i class="fa fa-eye-slash"/><i class="fa fa-eye"/></button>
<button title="Supprimer..." class="btn btn-xs elocus-action-delete-topic btn-default"><i class="fa fa-trash"/></button><button title="Ins&#xE9;rer..." class="btn btn-xs elocus-action-insert-topic btn-default"><i class="fa fa-plus"/></button></span></span></p></div></div> """%  {
    'project':project,
    'report':report,
    'username':request.user.username,
    'filename':name,
    'idtopic':id_new_topic,
    'user':self.request.user.username,
    'kind':topic_kind,
    }

        logger.debug(topic)
        prevtopic = assembly.xpath("//html:div[@id=$topic]", topic = topic, **ns)[0]
        prevtopic.addnext(deepcopy(newtopic))

        self.write_assembly(assembly, project, report, extra_commit = list_commit)

        return HttpResponse(result, content_type="text/html")

class ElocusUserTopicNewTextView(ElocusPrivateMixin, View):
    def post(self, request, project, report, topic):
        self.set_project(project)
        root = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        release_path = os.path.join(root,'releases', report)
        assembly_path = os.path.join(release_path,"sources","fr","assembly",report+"_asm.html")
        assembly = ET.parse(assembly_path)
        topic_kind = "description"
        id_new_topic = "idm" + str(uuid.uuid1().int)
        source_new_topic = """
        <div class='topic' id='%(idtopic)s' data-user='%(user)s'>
          <div class='topicinfo'></div>
          <div class="kolekti-component-title"></div>
          <div class="kolekti-component-%(kind)s">
          </div>
        </div>
        """ %{
            'idtopic':id_new_topic,
            'user':self.request.user.username,
            'kind':topic_kind,
            }
        newtopic = ET.fromstring(source_new_topic)


        result = """
        <div class="col-sm-12 col-lg-6 widget">
          <div class="topic" id="%(idtopic)s" data-user="%(user)s">
            <div class="panel panel-default notitle">
              <div class="panel-body overflow-x-hidden" style="overflow-y: auto; overflow-x: hidden;">
                <div class="row components">
                  <div class="col-sm-12 col-lg-12">
                    <div class="wysiwyg-wrapper">
                      <div contenteditable="true" class="description-editor" id="editor%(idtopic)s"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-footer">
               <p class="text-right">
                 <span>
                   <span class="btn-group hide-topicdetails"><button title="A la une" class="btn btn-xs btn-default  elocus-action-star btn-default"><i class="fa fa-star-o"/></button><button title="Masquer" class="btn btn-xs elocus-action-hide btn-default"><i class="fa fa-eye-slash"/><i class="fa fa-eye"/></button>
<button title="Supprimer..." class="btn btn-xs elocus-action-delete-topic btn-default"><i class="fa fa-trash"/></button><button title="Ins&#xE9;rer..." class="btn btn-xs elocus-action-insert-topic btn-default"><i class="fa fa-plus"/></button></span></span></p></div></div> """%  {
    'project':project,
    'report':report,
    'username':request.user.username,
    'idtopic':id_new_topic,
    'user':self.request.user.username,
    'kind':topic_kind,
    }

        logger.debug(topic)
        prevtopic = assembly.xpath("//html:div[@id=$topic]", topic = topic, **ns)[0]
        prevtopic.addnext(deepcopy(newtopic))

        self.write_assembly(assembly, project, report)

        return HttpResponse(result, content_type="text/html")



class ElocusUserTopicDeleteView(ElocusPrivateMixin, View):
    def post(self, request, project, report, topic):
        root = os.path.join(settings.KOLEKTI_BASE, self.request.user.username, project)
        release_path = os.path.join(root,'releases', report)
        assembly_path = os.path.join(release_path,"sources","fr","assembly",report+"_asm.html")
        try:
            assembly = self.get_assembly(project, report)
            topic_elt = assembly.xpath("//html:div[@id = '%s']"%topic,
                                 **ns)[0]
            topic_elt.getparent().remove(topic_elt)
            self.write_assembly(assembly, project, report)
        except:
            import traceback
            logger.exception('error delete topic %s', topic)
            return HttpResponse(json.dumps({'status':'fail',
                                            'msg':traceback.format_exc()}),content_type="application/json")

        return HttpResponse(json.dumps({'status':'ok'}),content_type="application/json")



class PDCView(TemplateView):
        template_name = "pdc/pdc.html"
