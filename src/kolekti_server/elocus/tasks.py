# -*- coding: utf-8 -*-
#     kOLEKTi : a structural documentation generator
#     Copyright (C) 2007-2011 Stéphane Bonhomme (stephane@exselt.com)
#     Author Stéphane Bonhomme (stephane@exselt.com)
#     Author David Beniamine (david.beniamine@tetras-libre.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


from kolekti_server.celeryconf import app
import os
import json
from lxml import etree as ET
import traceback

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from kolekti.publish_queries import kolektiSparQL
from kolekti.publish import Releaser, ReleasePublisher
from kolekti.synchro import SynchroManager
from elocus.notifications import notify_send
from kserver.models import Project, UserProject
import logging
logger = logging.getLogger('kolekti.'+__name__)

def _email(dst, subject,text_content, html_content):
        msg = EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL, dst)
        msg.attach_alternative(html_content, "text/html")
        msg.send()

def _email_report_create(project, dst, report, title):
        mail_params = {
            'hostname':settings.HOSTNAME,
            'project':project,
            'report':report,
            'title':title,
        }

        subject = u'[elocus] nouveau rapport %s'%(title)

        text_content = u"""
        Cher utilisateur elocus,

        Le rapport %(title)s a été créé, ile st disponible à l'adresse suivante :
        https://%(hostname)s/elocus/%(project)s/%(report)s/

        L'équipe elocus.
        """%mail_params

        html_content = u"""
        <p>Cher utilisateur elocus,</p>

        <p>Le rapport
        <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/">
        %(title)s
        </a>
        a été créé.</em>.

        <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/">Ouvrir dans eLocus</a> ou copier l'adresse suivante dans votre navigateur web.</p>
        <pre>https://%(hostname)s/elocus/%(project)s/%(report)s/</pre>

        <p>L'équipe elocus</p>.
        """%mail_params
        _email(dst, subject, text_content, html_content)


def _email_report_update(dst, project, report, title):
        #    dst = [up.user.email for up in UserProject.objects.filter(project__directory = project, is_admin = True)]
        mail_params = {
                'hostname':settings.HOSTNAME,
                'project':project,
                'report':report,
                'title':title,
        }
        subject = u'[elocus] rapport mis à jour %(title)s'%mail_params

        text_content = u"""
        Cher utilisateur elocus,

        Le rapport %(title)s a été mis à jour, i est disponible à l'adresse suivante :
        https://%(hostname)s/elocus/%(project)s/%(report)s/

        L'équipe elocus.
        """%mail_params

        html_content = u"""
        <p>Cher utilisateur elocus,</p>

        <p>Le rapport
        <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/">
        %(title)s
        </a>
        a été mis à jour.</em>.

        <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/">Ouvrir dans eLocus</a> ou copier l'adresse suivante dans votre navigateur web.</p>
        <pre>https://%(hostname)s/elocus/%(project)s/%(report)s/</pre>

        <p>L'équipe elocus</p>.
        """%mail_params
        _email(dst, subject, text_content, html_content)

def _email_report_publish(project, dst, report, title):
        #    dst = [up.user.email for up in UserProject.objects.filter(project__directory = project, is_admin = True)]
        mail_params = {
                'hostname':settings.HOSTNAME,
                'project':project,
                'report':report,
                'title':title,
            }
        subject = u'[elocus] rapport publié %s'%(title)

        text_content = u"""
        Cher utilisateur elocus,

        Le rapport %(title)s a été publié
        https://%(hostname)s/elocus/%(project)s/%(report)s/

        Vous pouvez le télécharger aux addresses suivantes (selon le format):

        Odt (Open / Libre Office) :
            https://%(hostname)s/elocus/%(project)s/%(report)s/odt/
        Doc (Microsoft Office) :
            https://%(hostname)s/elocus/%(project)s/%(report)s/doc/
        Pdf (format portable, lecture seule) :
            https://%(hostname)s/elocus/%(project)s/%(report)s/pdf/

        L'équipe elocus.
        """%mail_params

        html_content = u"""
        <p>Cher utilisateur elocus,</p>

        <p>Le rapport
        <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/">
        %(title)s
        </a>
        a été publié.</em>.

        Vous pouvez le télécharger en cliquant sur l'un des liens suivants (selon le format):

        <ul>
        <li> <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/odt/">Odt (Open / Libre Office)</a></li>
        <li> <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/doc/">Doc (Microsoft Office)</a></li>
        <li> <a href="https://%(hostname)s/elocus/%(project)s/%(report)s/pdf/">Pdf (format portable, lecture seule)</a></li>
        </lu>

        <p>L'équipe elocus</p>.
        """%mail_params
        _email(dst, subject, text_content, html_content)

def _email_report_error(project, user, report, msg):
        #    dst = [up.user.email for up in UserProject.objects.filter(project__directory = project, is_admin = True)]
        mail_params = {
            'hostname':settings.HOSTNAME,
            'project':project,
            'report':report,
            'msg':msg,
            }
        subject = u'[elocus] Erreur de mise à jour %s'%(report)

        text_content = u"""
        instance : %(hostname)s
        project ; %(project)s
        report : %(report)s

        %(msg)s
        """%mail_params

        html_content = u"""
        instance : %(hostname)s<br />
        project ; %(project)s<br />
        report : %(report)s<br />

        <pre>%(msg)s</pre>
        """%mail_params
        dst = [settings.ADMIN_EMAIL]
        logger.debug(dst)
        _email(dst, subject, text_content, html_content)

def commit_report(project, user, report):
    try:
        base_path = os.path.join(settings.KOLEKTI_BASE, user, project)
        sync = SynchroManager(base_path)
        status = sync.statuses('/releases/' + report)
        if len(status['unversioned']) == 1 and (status['unversioned'][0]['path'] ==  u'/releases/' + report):
            return
        for item in status['unversioned']:
            pathitems = item['path'].split('/')
            logger.debug(pathitems)
            if pathitems[3] in ['sources', 'kolekti']:
                sync.add_resource(item['path'])
        sync.commit(['/releases/' + report], "report data update")

    except:
        logger.exception('** Synchro report error')
        _email_report_error(project, user, report, "action : Update report\nStacktrace:%s" %(traceback.format_exc(),))


def do_notify(user_name, project_name, verb, report, url = None, text = None):
    project = Project.objects.get(directory=project_name)
    user = User.objects.get(username=user_name)
    if url is None:
        url = reverse('elocusreport', kwargs = {"project":project_name, "report":report})
    if text is None:
        text = u' le rapport %s' % (report)
    notify_send(user, to=project, verb=verb, target_text=text, target_url=url)


@app.task(bind = True)
def create_report(self, project, user, job, title, toc, lang):
    try:
        project_path = os.path.join(settings.KOLEKTI_BASE, user, project)
        tocpath = '/sources/%s/tocs/elocus/%s'%(lang,toc)

        xjob = ET.ElementTree(ET.fromstring(job))

        r = Releaser(project_path, lang = lang, sync = False)
        res = []
        maildst =  [User.objects.get(username = user).email]
        release = ""
        for ev in r.make_release(tocpath, xjob, release_title = title):
            logger.debug('PUBLISH EVENT *************** %s', str(ev))
            if ev['event']=='release_creation':
                release = ev['releasename']
                commit_report(project, user, release)
                _email_report_create(project, maildst, release, title)
                do_notify(user, project, u'a crée', release)
            if ev["event"]=='error':
                _email_report_error(project, user, release, "action : Create report\nStacktrace:%s" %(ev['stacktrace'],))
            res.append(ev)

        return json.dumps(res)
    except:
        logger.exception('** Generate report error')
        _email_report_error(project, user, release, "action : Update report\nStacktrace:%s" %(traceback.format_exc(),))

@app.task(bind = True)
def update_report(self, project, user, report, sparqlserver):
    #    settings.KOLEKTI_BASE, self.request.user.username, self.kolekti_userproject.project.directory
    try:
        base_path = os.path.join(settings.KOLEKTI_BASE, user, project)
        assembly_dir = os.path.join(base_path, 'releases', report,"sources","fr","assembly")
        assembly_path = os.path.join(assembly_dir, report+"_asm.html")
        assembly_ref = '/'.join(['/releases', report,"sources","fr","assembly",report+"_asm.html"])
        assembly = ET.parse(assembly_path)
        title = assembly.xpath('string(/html:html/html:head/html:title)',namespaces={'html':'http://www.w3.org/1999/xhtml'})
        sp = kolektiSparQL(sparqlserver)
        sp.process_queries(assembly, assembly_dir)
        with open(assembly_path,"w") as rf:
            rf.write(ET.tostring(assembly, encoding="utf-8", pretty_print=True, xml_declaration=True))
        commit_report(project, user, report)
        maildst =  [User.objects.get(username = user).email]
        _email_report_update(maildst, project, report, title)
        logger.info('** Update report terminated')
        do_notify(user, project, u'a mis à jour', report)
    except:
        logger.exception('** Update report error')
        _email_report_error(project, user, report, "action : Update report\nStacktrace:%s" %(traceback.format_exc(),))

@app.task(bind = True)
def publish_report(self, project, user, report, script, langs, publication_type, sendmail = True):
    #    settings.KOLEKTI_BASE, self.request.user.username, self.kolekti_userproject.project.directory
    res = []
    try:
        project_path = os.path.join(settings.KOLEKTI_BASE, user, project)
        release_path = "/releases/%s"%(report,)
        xjob = ET.parse(os.path.join(project_path,"releases",report,"kolekti","publication-parameters",report+"_asm.xml"))
        assembly_path = os.path.join(settings.KOLEKTI_BASE, user, project, 'releases', report,"sources","fr","assembly",report+"_asm.html")
        assembly = ET.parse(assembly_path)
        title = assembly.xpath('string(/html:html/html:head/html:title)',namespaces={'html':'http://www.w3.org/1999/xhtml'})
        jscripts = xjob.getroot().find('scripts')
        for jscript in jscripts:
            if jscript.get('name') != script:
                jscripts.remove(jscript)
        r = ReleasePublisher(release_path, project_path, langs = langs)
        maildst =  [User.objects.get(username = user).email]
        for ev in r.publish_assembly(report + "_asm", xjob):
            if ev["event"]=='error':
                _email_report_error(project, user, report, "action : Publish report\nStacktrace:%s" %(ev['stacktrace'],) )
            if ev["event"]=='result' and sendmail:
                url =  reverse('elocus'+publication_type, kwargs = {"project":project, "report":report})
                _email_report_publish(project, maildst, report, title)
                do_notify(user, project, u'a mis à jour', report, url)
            res.append(ev)
    except:
        logger.exception('** Word export  error')
        _email_report_error(project, user, report, "action : Publish report\nStacktrace:%s" %(traceback.format_exc(),))

    return json.dumps(res)

@app.task(bind = True)
def generate_odt(self, project, user, report):
    logger.debug('generate odt')
    res = []
    script = 'odt'
    langs=['fr']
    try:
        project_path = os.path.join(settings.KOLEKTI_BASE, user, project)
        release_path = "/releases/%s"%(report,)
        xjob = ET.parse(os.path.join(project_path,"releases",report,"kolekti","publication-parameters",report+"_asm.xml"))
        assembly_path = os.path.join(settings.KOLEKTI_BASE, user, project, 'releases', report,"sources","fr","assembly",report+"_asm.html")
        assembly = ET.parse(assembly_path)
        title = assembly.xpath('string(/html:html/html:head/html:title)',namespaces={'html':'http://www.w3.org/1999/xhtml'})
        jscripts = xjob.getroot().find('scripts')
        for jscript in jscripts:
            if jscript.get('name') != script:
                jscripts.remove(jscript)
        r = ReleasePublisher(release_path, project_path, langs = langs)
        r.publish_assembly(report + "_asm", xjob)
    except:
        logger.exception('** Word export error')
        _email_report_error(project, user, report, "action : Generate odt\nStacktrace:%s" %(traceback.format_exc(),))

