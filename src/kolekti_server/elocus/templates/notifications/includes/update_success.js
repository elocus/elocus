var updateSuccess = function (response) {
    var notification_box = $(nfBoxListClassSelector);
    var notifications = response.notifications;
    $.each(notifications, function (i, notification) {
        notification_box.prepend(notification.html);
    });
	if (response.unread_count) {
		$(".notifications-trigger").addClass('active');
		//console.log("notif not read: " + response.unread_count);
	} else {
		$(".notifications-trigger").removeClass('active');
	}
};
