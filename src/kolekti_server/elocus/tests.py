from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase

from .models import Template, Project

# Create your tests here.
class ProjectMethodTests(TestCase):
    def setUp(self):
        Template.objects.create(
            name="elocus_test",
            description="elocus_tests",
            svn="https://dev.elocus.fr/svn/elocus/test",
            svn_user = "test",
            svn_pass = "top_secret",
        )
        self.user = User.objects.create_user(username='test', email='test@elocus.fr', password='top_secret')

    def project_create_test(self):
        name = "test"
        directory = "test"
        owner = User.objects.get(username='test')
        template = Template.objects.get(name = 'elocus_tests')
        p = Project(name, directory, owner, template)
        assert (os.path.exists('/svn/test'))

    
