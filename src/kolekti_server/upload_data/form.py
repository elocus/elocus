# -*- coding: utf-8 -*-

# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr> (<year>)
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from django import forms

import os
import tempfile
import re
from django.utils.safestring import mark_safe

from importer import available_scripts

from .tasks import upload_data

import logging
logger = logging.getLogger('kolekti.'+__name__)


class UploadForm(forms.Form):

    # Prepare form
    # EndPointAddress = forms.CharField(label='SparQL Server', max_length=199, initial="localhost")
    # EndPointPort = forms.IntegerField(label='SparQL Port', min_value=1000, max_value=9999, initial=3030)
    # EndPointUser = forms.CharField(label='SparQL user', max_length=200)
    # EndPointPass = forms.CharField(label='SparQL password', max_length=200, widget=forms.PasswordInput)
    script = forms.ChoiceField(label="Méthode d'import",
                               choices=tuple([(k, mark_safe(v)) for k, v in available_scripts().iteritems()]),
                               widget=forms.RadioSelect())
    File = forms.FileField(label='Sélectionner un fichier de donnée')

    def process_data(self, uid):
        # Read request
        script = self.cleaned_data['script']
        # save temp file
        f = self.cleaned_data['File']
        (fd, path) = tempfile.mkstemp(dir="/opt/elocus/", suffix="_"+f.name)
        for chunk in f.chunks():
            os.write(fd, chunk)
        os.close(fd)
        logger.info("File uploaded %s to %s", f.name, path)
        # Trigger thetask
        upload_data.delay(script, path, uid, f.name)

