# -*- coding: utf-8 -*-

# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr> (<year>)
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.models import User
from kolekti_server.celeryconf import app
from importer import run_script
from django.contrib.auth.models import User
# from kserver.models import Project, UserProject
from elocus.notifications import notify_send
import logging
logger = logging.getLogger('kolekti.'+__name__)


def email(dst, subject, text_content, html_content):
    msg = EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL, dst)
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def email_success(dst, filename):
    mail_params = {
        'hostname': settings.HOSTNAME,
        'filename': filename,
    }

    subject = u'[elocus] Données ajoutées'

    text_content = u"""
    Cher utilisateur elocus,

    Les données ont été importées avec succès à partir du fichier %(filename)s.

    L'équipe elocus.
    """ % mail_params

    html_content = u"""
    <p>Cher utilisateur elocus,</p>

    <p>Les données ont été importées avec succès à partir du fichier %(filename)s.</p>

    <p>L'équipe elocus.</p>
    """ % mail_params
    email(dst, subject, text_content, html_content)


def email_error(dst, filename, script, error):
    mail_params = {
        'hostname': settings.HOSTNAME,
        'filename': filename,
        'error': error,
        'script': script,
    }

    subject = u'[elocus] Échec lors de l\'ajout de données'

    text_content = u"""
    Cher utilisateur elocus,

    Nous avons le regret de vous annoncer l'échec de l'import des données à partir du fichier %(filename)s et avec la
    méthode d'import %(script)s.

    L'erreur suivante a été rencontrée :

        %(error)s

    Nous vous invitons à transférer ce mail à votre administrateur pour résolution du problème.

    L'équipe elocus.
    """ % mail_params

    html_content = u"""
    <p>Cher utilisateur elocus,</p>

    <p>Nous avons le regret de vous annoncer l'échec de l'import des données à partir du fichier %(filename)s et avec la
    méthode d'import %(script)s.</p>

    <p>
    L'erreur suivante a été rencontrée :</p>

    <pre><code>
        %(error)s
    </pre></code>

    <p>Nous vous invitons à transférer ce mail à votre administrateur pour résolution du problème.</p>

    <p>L'équipe elocus.</p>
    """ % mail_params
    email(dst, subject, text_content, html_content)


@app.task(bind=True)
def upload_data(self, script, path, uid, fname):
    user = User.objects.get(pk=uid)
    try:
        run_script(script, path)
        notify_send(user, to=user, verb="a téléversé des données depuis le fichier : '{}'".format(fname))
        logger.debug("sending mail to '%s'", user.email)
        email_success([user.email], fname)
    except Exception as e:
        logger.debug("Failed to upload data using '%s', file '%s', error '%s'", script, path, e)
    # target = UserProject.objects.filter(user=uid)[0]
    # logger.debug("testing target %s", target)
