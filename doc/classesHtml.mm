<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1496415159954" ID="ID_652150829" MODIFIED="1496415239425" TEXT="classesHtml">
<node CREATED="1496415443966" ID="ID_1224379096" MODIFIED="1496415455436" POSITION="right" TEXT="#reportContent">
<node CREATED="1496415262993" ID="ID_680721639" MODIFIED="1498660937406" TEXT="home-general">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      accueil-general
    </p>
    <p>
      C'est la page d'accueil avant les rapport
    </p>
  </body>
</html></richcontent>
<node CREATED="1496416931861" ID="ID_896988895" MODIFIED="1498661712474" TEXT="teasers-project teasers">
<node CREATED="1496415337030" ID="ID_1964354564" MODIFIED="1498661706106" TEXT="teaser-project teaser ">
<icon BUILTIN="family"/>
</node>
</node>
</node>
<node CREATED="1496415294127" ID="ID_542780799" MODIFIED="1498660953669" TEXT="home-report">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      accueil-rapport
    </p>
    <p>
      Page d'arriv&#233;e dans le rapport
    </p>
  </body>
</html></richcontent>
<node CREATED="1496417131219" ID="ID_1299342256" MODIFIED="1496417134073" TEXT="header">
<node CREATED="1496417314306" ID="ID_568799355" MODIFIED="1496417356374" TEXT="page-title"/>
<node CREATED="1496415584717" ID="ID_2329045" MODIFIED="1496417140337" TEXT="page-header">
<node CREATED="1496415590757" ID="ID_1778926635" MODIFIED="1496415600465" TEXT="breadcrumb"/>
</node>
</node>
<node CREATED="1496417296698" ID="ID_829147085" MODIFIED="1496417384170" TEXT="intro">
<node CREATED="1496417074236" ID="ID_1002176595" MODIFIED="1496417375694" TEXT="intro-topics">
<node CREATED="1496417145691" ID="ID_1787085380" MODIFIED="1496417397737" TEXT="intro-topic topic"/>
</node>
</node>
<node CREATED="1496416876445" ID="ID_399145401" MODIFIED="1498662317960" TEXT="teasers-section teasers">
<node CREATED="1496415825796" ID="ID_652707178" MODIFIED="1498662197728" TEXT="teaser-section teaser">
<icon BUILTIN="group"/>
</node>
</node>
<node CREATED="1496417657409" ID="ID_990802624" MODIFIED="1498660907749" TEXT="star-topics">
<node CREATED="1496417869287" ID="ID_707446789" MODIFIED="1496417882173" TEXT="front-topic topic"/>
</node>
</node>
<node CREATED="1496415294127" ID="ID_681681685" MODIFIED="1498663639181" TEXT="report-page">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      accueil-rapport
    </p>
    <p>
      Page d'arriv&#233;e dans le rapport
    </p>
  </body>
</html></richcontent>
<node CREATED="1496417131219" ID="ID_1224061964" MODIFIED="1496417134073" TEXT="header">
<node CREATED="1496417314306" ID="ID_193600671" MODIFIED="1496417356374" TEXT="page-title"/>
<node CREATED="1496415584717" ID="ID_1614131889" MODIFIED="1496417140337" TEXT="page-header">
<node CREATED="1496415590757" ID="ID_373944732" MODIFIED="1496415600465" TEXT="breadcrumb"/>
</node>
</node>
<node CREATED="1496417296698" ID="ID_1156864422" MODIFIED="1496417384170" TEXT="intro">
<node CREATED="1496417074236" ID="ID_843407817" MODIFIED="1496417375694" TEXT="intro-topics">
<node CREATED="1496417145691" ID="ID_540534937" MODIFIED="1496417397737" TEXT="intro-topic topic"/>
</node>
</node>
</node>
<node CREATED="1496416702286" ID="ID_441347433" MODIFIED="1496417914076" TEXT="topic">
<node CREATED="1496417915990" ID="ID_1997291662" MODIFIED="1496417928372" TEXT="components">
<node CREATED="1496417930862" ID="ID_1741178354" MODIFIED="1496418041923" TEXT="component component-html"/>
</node>
</node>
</node>
<node CREATED="1496415896891" ID="ID_1730018927" MODIFIED="1496417053130" POSITION="left" TEXT="trame ou assemblage">
<node CREATED="1496415900891" ID="ID_1645546522" MODIFIED="1496415907122" TEXT="topic">
<node CREATED="1496415909275" ID="ID_1081690983" MODIFIED="1496415916288" TEXT="composants"/>
</node>
</node>
<node CREATED="1496416403240" ID="ID_720612718" MODIFIED="1496417453401" POSITION="left" TEXT="application">
<node CREATED="1496416310000" ID="ID_1613674178" MODIFIED="1496416530725" TEXT="project">
<icon BUILTIN="group"/>
<node CREATED="1496416315520" ID="ID_71998041" MODIFIED="1496416518773" TEXT="report">
<icon BUILTIN="group"/>
<node CREATED="1496416354246" ID="ID_1523923230" MODIFIED="1496416374039" TEXT="section">
<icon BUILTIN="group"/>
<node CREATED="1496415900891" ID="ID_1524562790" MODIFIED="1496416376239" TEXT="topic">
<icon BUILTIN="group"/>
<node CREATED="1496415909275" ID="ID_168569445" MODIFIED="1496416543732" TEXT="component">
<icon BUILTIN="group"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
