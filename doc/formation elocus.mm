<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Formation Elocus" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1494495206714"><hook NAME="MapStyle" background="#faf7f4" zoom="3.0">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating" BACKGROUND_COLOR="#f0f0f0">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode TEXT="exemple" BACKGROUND_COLOR="#d2e5f5" MAX_WIDTH="800" MIN_WIDTH="1">
<font NAME="Courier New" SIZE="9"/>
<edge STYLE="hide_edge"/>
</stylenode>
<stylenode TEXT="path" COLOR="#af600c">
<icon BUILTIN="folder"/>
<font NAME="Courier New" SIZE="8" BOLD="true"/>
<edge STYLE="horizontal"/>
</stylenode>
<stylenode TEXT="srcpath" COLOR="#468e0b">
<icon BUILTIN="folder"/>
<font NAME="Courier New" SIZE="8" BOLD="true"/>
<edge STYLE="horizontal"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="1"/>
<node TEXT="Contexte et utilisation" POSITION="right" ID="ID_927787923" CREATED="1494495209416" MODIFIED="1494495224518">
<edge COLOR="#ff0000"/>
<node TEXT="Plateforme de production de rapports" ID="ID_1161686478" CREATED="1494495642464" MODIFIED="1494495661549">
<node TEXT="informatique d&#xe9;cisionelle et collaborative" ID="ID_806451917" CREATED="1494835824957" MODIFIED="1494835846501"/>
<node ID="ID_1888073820" CREATED="1494835849126" MODIFIED="1494835855756"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <i>business intelligence</i>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="sources" ID="ID_735556820" CREATED="1494495662127" MODIFIED="1494495664730">
<node TEXT="Donn&#xe9;es OpenData" ID="ID_1132321432" CREATED="1494495667245" MODIFIED="1494495678547"/>
<node TEXT="Donn&#xe9;es Clients" ID="ID_1916262898" CREATED="1494495678917" MODIFIED="1494495684490"/>
<node TEXT="Analyses experts m&#xe9;tiers" ID="ID_382754143" CREATED="1494599815045" MODIFIED="1494599830739"/>
</node>
<node TEXT="Ecosysteme de logiciels" ID="ID_208976585" CREATED="1494835869641" MODIFIED="1494835876449">
<node TEXT="scrapper" ID="ID_929929333" CREATED="1494835876460" MODIFIED="1494835882210">
<node TEXT="selenium" ID="ID_1545535980" CREATED="1494835893738" MODIFIED="1494835896705"/>
<node TEXT="adhoc en python" ID="ID_78107244" CREATED="1494835898656" MODIFIED="1494835902344"/>
</node>
<node TEXT="modeles de donn&#xe9;es" ID="ID_1617497562" CREATED="1494835918059" MODIFIED="1494835921808">
<node TEXT="webproteg&#xe9;" ID="ID_1597639561" CREATED="1494835923281" MODIFIED="1494835927182"/>
<node TEXT="RDF" ID="ID_973799220" CREATED="1494835980300" MODIFIED="1494835982306"/>
</node>
<node TEXT="entrepots de donn&#xe9;es" ID="ID_1860954822" CREATED="1494835968917" MODIFIED="1494835978295">
<node TEXT="sparQL endpoints" ID="ID_1712742133" CREATED="1494835990604" MODIFIED="1494836001696">
<node TEXT="rdf4j" ID="ID_119227461" CREATED="1494836001708" MODIFIED="1494836004704"/>
<node TEXT="fuseki" ID="ID_1887939891" CREATED="1494836005034" MODIFIED="1494836008590"/>
<node TEXT="parliament" ID="ID_1514942176" CREATED="1494836009014" MODIFIED="1494836012802"/>
</node>
</node>
<node TEXT="Plateforme documentaire" ID="ID_915598678" CREATED="1494836513975" MODIFIED="1494836589474">
<node TEXT="&quot;dashboard&quot;" ID="ID_1651871792" CREATED="1494836541042" MODIFIED="1494836547732"/>
<node TEXT="cr&#xe9;ation de rapports" ID="ID_902245449" CREATED="1494836535297" MODIFIED="1494836539900"/>
</node>
<node TEXT="Infrastructure" ID="ID_1420378908" CREATED="1494836615625" MODIFIED="1494836632515">
<node TEXT="microservices docker" ID="ID_931531808" CREATED="1494836620868" MODIFIED="1494836638393"/>
</node>
</node>
<node TEXT="mise en perspective" ID="ID_1526318149" CREATED="1494495686082" MODIFIED="1494495739680">
<node TEXT="estimation d&apos;indicateurs de march&#xe9;" ID="ID_504000323" CREATED="1494495746403" MODIFIED="1494495786061"/>
<node TEXT="donn&#xe9;es effectives client" ID="ID_1437312670" CREATED="1494495795145" MODIFIED="1494495888265"/>
</node>
<node TEXT="Composants de visualisation des indicateurs" ID="ID_1955980499" CREATED="1494495890379" MODIFIED="1494495909054"/>
<node TEXT="Saisie des analyses" ID="ID_581698992" CREATED="1494495909627" MODIFIED="1494495927487"/>
<node TEXT="P&#xe9;riodicit&#xe9;, mise &#xe0; jour" ID="ID_1522316533" CREATED="1494495928062" MODIFIED="1494495933835"/>
<node TEXT="Productions" ID="ID_320226027" CREATED="1494495938095" MODIFIED="1494495943459"/>
</node>
<node TEXT="Ing&#xe9;nierie documentaire" POSITION="right" ID="ID_140021437" CREATED="1494495217402" MODIFIED="1494495237000">
<edge COLOR="#0000ff"/>
<node TEXT="automatismes de production de documents composites, multilingues, multisupports." ID="ID_659155415" CREATED="1494495946698" MODIFIED="1494495998490"/>
<node TEXT="kolekti" ID="ID_1836704068" CREATED="1494496007214" MODIFIED="1494496009609">
<node TEXT="configuration de documents modulaires, dynamiques et personnalis&#xe9;s" ID="ID_1674765279" CREATED="1494496011926" MODIFIED="1494496064290"/>
<node TEXT="Kolekti est un environnement de gestion de sources, permettant la production automatis&#xe9;e de gammes de documents, &#xe0; partir de sources unifi&#xe9;es" ID="ID_300634818" CREATED="1494599856606" MODIFIED="1494599908109"/>
<node TEXT="La vari&#xe9;t&#xe9; des documents produits se fonde sur" ID="ID_1765119034" CREATED="1494599908627" MODIFIED="1494599959663">
<node TEXT="une construction modulaire et structur&#xe9;e de ces derniers." ID="ID_1919613867" CREATED="1494599961567" MODIFIED="1494599963213"/>
<node TEXT="l&apos;interpr&#xe9;tation de partie conditionnelles" ID="ID_1508556602" CREATED="1494599964230" MODIFIED="1494599986796"/>
<node TEXT="la r&#xe9;f&#xe9;rence &#xe0; des variables" ID="ID_206823177" CREATED="1494599987134" MODIFIED="1494599995701">
<node TEXT="Traduction de lib&#xe9;ll&#xe9;s" ID="ID_1905590638" CREATED="1494600039393" MODIFIED="1494600046902"/>
<node TEXT="Codification produits" ID="ID_850157335" CREATED="1494600047275" MODIFIED="1494600053035"/>
</node>
</node>
<node TEXT="processus de publication des documents" ID="ID_1563126050" CREATED="1494599944261" MODIFIED="1494600089618">
<node TEXT="Point de d&#xe9;part" ID="ID_1863339803" CREATED="1494600091512" MODIFIED="1494600105379">
<node TEXT="une trame" ID="ID_473774840" CREATED="1494600107283" MODIFIED="1494600109881">
<node TEXT="fichier xhtml" ID="ID_533504013" CREATED="1494600194799" MODIFIED="1494600207573"/>
<node TEXT="structure de sections impriqu&#xe9;es" ID="ID_1709798058" CREATED="1494600249311" MODIFIED="1494600266039"/>
<node TEXT="r&#xe9;f&#xe9;rence des modules" ID="ID_363863012" CREATED="1494600201696" MODIFIED="1494600248528"/>
</node>
<node TEXT="des parametres de publication" ID="ID_1964394382" CREATED="1494600110235" MODIFIED="1494600118276">
<node TEXT="un ensemble de profils" ID="ID_874134021" CREATED="1494600304607" MODIFIED="1494600317548"/>
<node TEXT="un ensemble des scripts" ID="ID_1300119013" CREATED="1494600318229" MODIFIED="1494600326666"/>
</node>
</node>
<node TEXT="&#xe9;tapes" ID="ID_388692266" CREATED="1494600119459" MODIFIED="1494600126403">
<node TEXT="Assemblage du document" ID="ID_236010692" CREATED="1494600128276" MODIFIED="1494600141623"/>
<node TEXT="filtrage global" ID="ID_627226399" CREATED="1494600291809" MODIFIED="1494600297298"/>
<node TEXT="Pour chacun des profils" ID="ID_1878498708" CREATED="1494600142105" MODIFIED="1494600224875">
<node TEXT="filtrage de l&apos;assemblage" ID="ID_1457405267" CREATED="1494600165035" MODIFIED="1494600173842"/>
<node TEXT="resolution des variables" ID="ID_660097593" CREATED="1494600280851" MODIFIED="1494600289595"/>
<node TEXT="application des scripts de publication" ID="ID_964393598" CREATED="1494600174325" MODIFIED="1494600185618"/>
</node>
</node>
</node>
<node TEXT="Projets" ID="ID_1085421847" CREATED="1494496133784" MODIFIED="1494496327659">
<node TEXT="Structure fichiers" ID="ID_814913947" CREATED="1494495237575" MODIFIED="1494496344558">
<node TEXT="Trames" ID="ID_717820920" CREATED="1494496481842" MODIFIED="1494579724472">
<node TEXT="Structure type d&apos;un rapport" ID="ID_523799274" CREATED="1494496544613" MODIFIED="1494496552066">
<node TEXT="sections, sur plusieurs niveaux" ID="ID_557576159" CREATED="1494530102499" MODIFIED="1494530121163"/>
<node TEXT="R&#xe9;f&#xe9;rence un ensemble de topics" ID="ID_1631634402" CREATED="1494496552532" MODIFIED="1494529642490"/>
</node>
<node TEXT="/sources/fr/tocs/elocus/" STYLE_REF="path" ID="ID_930336377" CREATED="1494529649682" MODIFIED="1494579724472"/>
<node TEXT="Edition dans kolekti" ID="ID_1233633067" CREATED="1494529911725" MODIFIED="1494529961536">
<node TEXT="Menu Trames" ID="ID_1374626494" CREATED="1494529963335" MODIFIED="1494529965622"/>
<node TEXT="Interface d&#xe9;di&#xe9; pour ajouter modules / sections / g&#xe9;rer les titres" ID="ID_572058511" CREATED="1494530010337" MODIFIED="1494530026601"/>
</node>
</node>
<node TEXT="Topics" ID="ID_1779319077" CREATED="1494496488701" MODIFIED="1494496491916">
<node TEXT="Documents xhtml" ID="ID_857122932" CREATED="1494496860110" MODIFIED="1494496870796"/>
<node TEXT="micro-structures types" ID="ID_1920343058" CREATED="1494496875572" MODIFIED="1494496883799">
<node TEXT="composants" ID="ID_1466696617" CREATED="1494496537756" MODIFIED="1494496540252"/>
<node TEXT="requetes" ID="ID_427053113" CREATED="1494496838505" MODIFIED="1494496841304"/>
</node>
<node TEXT="/sources/fr/topics/templates/" STYLE_REF="path" ID="ID_1955163983" CREATED="1494529649682" MODIFIED="1494529907098"/>
<node TEXT="Edition dans kolekti" ID="ID_340098171" CREATED="1494529932306" MODIFIED="1494529940129">
<node TEXT="Menu Modules" ID="ID_692078934" CREATED="1494529969070" MODIFIED="1494529975573"/>
<node TEXT="Ckeditor" ID="ID_1227031746" CREATED="1494529975939" MODIFIED="1494529981805">
<node TEXT="Attention aux problemes d&apos;&#xe9;chappement des &apos;&lt;&apos; dans les requetes sparql" ID="ID_1627398542" CREATED="1494529982790" MODIFIED="1494530004024"/>
</node>
</node>
</node>
<node TEXT="Variables" ID="ID_1058826519" CREATED="1494496492389" MODIFIED="1494496497676">
<node TEXT="Publication personnalis&#xe9;e" ID="ID_1552089935" CREATED="1494496901945" MODIFIED="1494496911966"/>
<node TEXT="Traduction" ID="ID_211775572" CREATED="1494496912434" MODIFIED="1494496916676"/>
<node TEXT="valeurs relatives &#xe0; un ensemble de crit&#xe8;res." ID="ID_1607086114" CREATED="1494496917146" MODIFIED="1494496942588"/>
<node TEXT="/sources/fr/variables" STYLE_REF="path" ID="ID_1168227970" CREATED="1494529649682" MODIFIED="1494530171038"/>
<node TEXT="/sources/share/Variables" STYLE_REF="path" ID="ID_415571441" CREATED="1494529649682" MODIFIED="1494530190187"/>
<node TEXT="D&#xe9;finition de variables utilisateur, saisie &#xe0; la cr&#xe9;ation d&apos;un rapport" ID="ID_923539684" CREATED="1494530191072" MODIFIED="1494530212716"/>
</node>
<node TEXT="Versions" ID="ID_1633246901" CREATED="1494496498019" MODIFIED="1494508363374">
<node TEXT="Base du rapport eLocus" ID="ID_209494304" CREATED="1494496983504" MODIFIED="1494496995730"/>
<node TEXT="Document r&#xe9;sultant de l&apos;application de parametres de publication &#xe0; une trame" ID="ID_1309368838" CREATED="1494496950324" MODIFIED="1494530295867">
<node TEXT="assemblage xhtml" ID="ID_1433405466" CREATED="1494508372132" MODIFIED="1494508376143"/>
<node TEXT="resultats des requ&#xea;tes" ID="ID_277210025" CREATED="1494508404873" MODIFIED="1494508410762"/>
<node TEXT="profils de publication" ID="ID_559602209" CREATED="1494508376510" MODIFIED="1494508385691"/>
<node TEXT="variables" ID="ID_1233936591" CREATED="1494508386743" MODIFIED="1494508395453"/>
<node TEXT="ressources" ID="ID_34605604" CREATED="1494508395842" MODIFIED="1494508400654"/>
</node>
<node TEXT="Resolution des requ&#xea;tes sparql" ID="ID_695446605" CREATED="1494496968553" MODIFIED="1494506957353"/>
<node TEXT="/releases" STYLE_REF="path" ID="ID_231411297" CREATED="1494529649682" MODIFIED="1494530497554"/>
</node>
</node>
<node TEXT="Depots svn" ID="ID_943168500" CREATED="1494496346892" MODIFIED="1494496351010">
<node TEXT="Projet == d&#xe9;pot svn" ID="ID_1034391978" CREATED="1494497002343" MODIFIED="1494497007758">
<node TEXT="/svn/[project]" STYLE_REF="srcpath" ID="ID_1637253347" CREATED="1494529649682" MODIFIED="1494600406433"/>
</node>
<node TEXT="un projet est partag&#xe9; par plusieurs utilisateurs" ID="ID_610906162" CREATED="1494497008180" MODIFIED="1494530410560">
<node TEXT="chaque utilisateur dispose d&apos;une copie du d&#xe9;pot" ID="ID_1186445954" CREATED="1494497017601" MODIFIED="1494497033167">
<node TEXT="/projects/[user]/[project]" STYLE_REF="srcpath" ID="ID_907223565" CREATED="1494529649682" MODIFIED="1494600428716"/>
</node>
<node TEXT="gestion des droits d&apos;acc&#xe8;s" ID="ID_1349846703" CREATED="1494497051192" MODIFIED="1494497065357">
<node TEXT="Objet userproject" ID="ID_1406215786" CREATED="1494497067573" MODIFIED="1494497213119"/>
<node TEXT="par django pour l&apos;acc&#xe8;s &#xe0; l&apos;UI" ID="ID_1294506753" CREATED="1494497253634" MODIFIED="1494530432263"/>
<node TEXT="par apache kolekti-front pour le protocole svn" ID="ID_1945701362" CREATED="1494497263465" MODIFIED="1494497278962"/>
</node>
</node>
<node TEXT="Modele de collaboration" ID="ID_1559906046" CREATED="1494530503391" MODIFIED="1494530517876">
<node TEXT="lorsqu&apos;un modification est faite par un utilisateur" ID="ID_1348776687" CREATED="1494600433325" MODIFIED="1494600445853"/>
<node TEXT="" ID="ID_1276638340" CREATED="1494600446227" MODIFIED="1494600446227"/>
</node>
</node>
</node>
<node TEXT="impl&#xe9;mentation" ID="ID_826517491" CREATED="1494496570009" MODIFIED="1494496573419">
<node TEXT="Python 2.7" ID="ID_926196605" CREATED="1494496065173" MODIFIED="1494496071144"/>
<node TEXT="Lib Kolekti" ID="ID_1486139591" CREATED="1494496071522" MODIFIED="1494576697645">
<node TEXT="cli" ID="ID_1918995744" CREATED="1494496089034" MODIFIED="1494496099946"/>
<node TEXT="utilis&#xe9;es par les views django (controleurs)" ID="ID_625624729" CREATED="1494496100296" MODIFIED="1494496123540"/>
<node TEXT="kolekti.common.kolektiBase" ID="ID_1540764948" CREATED="1494507534581" MODIFIED="1494507573686"/>
<node TEXT="kolekti.publish.Releaser" ID="ID_1458354634" CREATED="1494508261574" MODIFIED="1494508267400"/>
<node TEXT="kolekti.publish.ReleasePublisher" ID="ID_1734093386" CREATED="1494507542976" MODIFIED="1494507568047"/>
<node TEXT="kolekti.synchro.SyncManager" ID="ID_672850436" CREATED="1494508217460" MODIFIED="1494508228905"/>
</node>
<node TEXT="Application Django" ID="ID_601101486" CREATED="1494496126261" MODIFIED="1494496131778">
<node TEXT="Kolekti_server" ID="ID_1647933269" CREATED="1494496355124" MODIFIED="1494496359106">
<node TEXT="Project Django" ID="ID_739663402" CREATED="1494508050587" MODIFIED="1494508054951"/>
<node TEXT="settings.py" ID="ID_1352088635" CREATED="1494505917846" MODIFIED="1494505922492">
<node TEXT="les settings vont prendre leurs valeur dans les variable d&apos;environnement" ID="ID_391363397" CREATED="1494505928900" MODIFIED="1494505943585"/>
</node>
<node TEXT="urls.py" ID="ID_162255497" CREATED="1494505923067" MODIFIED="1494505926382">
<node TEXT="Plan d&apos;url principal, appelle les plans d&apos;url de kserver_saas , elocus, auth..." ID="ID_1889239792" CREATED="1494505949182" MODIFIED="1494505970621"/>
</node>
</node>
<node TEXT="kserver" ID="ID_600478244" CREATED="1494496367155" MODIFIED="1494496370569">
<node TEXT="application django interface Kolekti" ID="ID_793878244" CREATED="1494505975203" MODIFIED="1494505985161"/>
<node TEXT="static" ID="ID_386315508" CREATED="1494506982711" MODIFIED="1494506986438">
<node TEXT="donn&#xe9;es statiques de l&apos;ensemble de l&apos;application" ID="ID_1000678137" CREATED="1494506989370" MODIFIED="1494507000874"/>
</node>
<node TEXT="modeles pour la gestion des projets" ID="ID_347465132" CREATED="1494507140406" MODIFIED="1494507149647"/>
<node TEXT="vues" ID="ID_758446201" CREATED="1494507155883" MODIFIED="1494507160001"/>
<node TEXT="templates" ID="ID_196379009" CREATED="1494507170414" MODIFIED="1494507172904"/>
</node>
<node TEXT="kserver_saas" ID="ID_1438665888" CREATED="1494496371999" MODIFIED="1494496377550">
<node TEXT="application django : gestion des utilisateurs et acc&#xe8;s aux projets" ID="ID_1588359487" CREATED="1494505989022" MODIFIED="1494506142521"/>
<node TEXT="surcharg&#xe9;s par elocus" ID="ID_1938833666" CREATED="1494508092492" MODIFIED="1494508100517"/>
</node>
<node TEXT="elocus" ID="ID_103930150" CREATED="1494496447845" MODIFIED="1494496450349">
<node TEXT="Interface elocus" ID="ID_963753231" CREATED="1494506145686" MODIFIED="1494506151150"/>
<node TEXT="templates" ID="ID_996936615" CREATED="1494506968993" MODIFIED="1494506973075"/>
<node TEXT="views" ID="ID_161528159" CREATED="1494508185320" MODIFIED="1494508188149"/>
<node TEXT="tasks" ID="ID_407935293" CREATED="1494508180144" MODIFIED="1494508183450"/>
<node TEXT="xsl" ID="ID_97240420" CREATED="1494508319773" MODIFIED="1494508322156"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Les d&#xe9;pots de donn&#xe9;es eLocus" POSITION="right" ID="ID_1576989592" CREATED="1494495255493" MODIFIED="1494495604304">
<edge COLOR="#ff00ff"/>
<node TEXT="existant" ID="ID_47695731" CREATED="1494836365674" MODIFIED="1494836373126">
<node TEXT="r&#xe9;f&#xe9;rentiels &#xe9;tablis" ID="ID_1135386643" CREATED="1494836373135" MODIFIED="1494836382832"/>
</node>
<node TEXT="perspective" ID="ID_1208045096" CREATED="1494836385673" MODIFIED="1494836389913">
<node TEXT="permettre aux utilisateurs de cr&#xe9;er leurs r&#xe9;f&#xe9;rentiels" ID="ID_1225058969" CREATED="1494836391434" MODIFIED="1494836405910"/>
</node>
</node>
<node TEXT="Les rapports elocus" POSITION="right" ID="ID_386421073" CREATED="1494495273928" MODIFIED="1494495620282">
<edge COLOR="#00ffff"/>
<node TEXT="&quot;Version&quot; kolekti" ID="ID_404482234" CREATED="1494508336166" MODIFIED="1494508458543">
<node TEXT="pr&#xe9;sence d&apos;un assemblage xhtml" ID="ID_1375198721" CREATED="1494508474891" MODIFIED="1494508483816"/>
<node TEXT="" ID="ID_1946644555" CREATED="1494508484332" MODIFIED="1494508484332"/>
<node TEXT="Cr&#xe9;&#xe9;" ID="ID_1420830898" CREATED="1494508494922" MODIFIED="1494509061348">
<node TEXT="a partir de" ID="ID_1576748959" CREATED="1494509062833" MODIFIED="1494509066663">
<node TEXT="trame" ID="ID_628501402" CREATED="1494508504948" MODIFIED="1494508506262">
<node TEXT="r&#xe9;f&#xe9;rentiel" ID="ID_752829104" CREATED="1494509090423" MODIFIED="1494509094666"/>
<node TEXT="topics" ID="ID_1540293974" CREATED="1494508623473" MODIFIED="1494508625886"/>
</node>
<node TEXT="parametres de publication" ID="ID_580245098" CREATED="1494508506499" MODIFIED="1494509325176"/>
<node TEXT="uservariables" ID="ID_1773589373" CREATED="1494508512267" MODIFIED="1494508516035">
<node TEXT="d&#xe9;finies dans le projet" ID="ID_1774784682" CREATED="1494508518316" MODIFIED="1494508527774"/>
<node TEXT="saisies lors de la cr&#xe9;ation du rapport" ID="ID_1120982307" CREATED="1494508528052" MODIFIED="1494508536642"/>
<node TEXT="peuvent &#xea;tre int&#xe9;gr&#xe9;es dans les requetes sparql" ID="ID_1159662690" CREATED="1494508536947" MODIFIED="1494508604276"/>
</node>
</node>
<node TEXT="dans l&apos;arbosrescence du projet" ID="ID_1702977075" CREATED="1494509024981" MODIFIED="1494509052877"/>
<node TEXT="non ajout&#xe9; au svn" ID="ID_548843512" CREATED="1494509053246" MODIFIED="1494509057391"/>
</node>
</node>
<node TEXT="action des utilisateurs" ID="ID_1751938702" CREATED="1494508640960" MODIFIED="1494508664912">
<node TEXT="Mettre &#xe0; jour" ID="ID_300046733" CREATED="1494508668200" MODIFIED="1494508673141">
<node TEXT="Recalcule les requ&#xea;tes Sparql" ID="ID_1347629477" CREATED="1494508674556" MODIFIED="1494508685393"/>
</node>
<node TEXT="Exporter" ID="ID_117857740" CREATED="1494508687778" MODIFIED="1494508697941">
<node TEXT="word" ID="ID_1060819986" CREATED="1494508699355" MODIFIED="1494508701247"/>
<node TEXT="pdf" ID="ID_1073623140" CREATED="1494508701738" MODIFIED="1494508961603"/>
<node TEXT="presentation html5" ID="ID_847499718" CREATED="1494508962113" MODIFIED="1494508967538"/>
</node>
<node TEXT="Partager" ID="ID_741844345" CREATED="1494508968516" MODIFIED="1494508971383">
<node TEXT="ajout du rapport au d&#xe9;pot svn" ID="ID_1015131669" CREATED="1494508979196" MODIFIED="1494508999839"/>
<node TEXT="synchronisation" ID="ID_586490488" CREATED="1494509000444" MODIFIED="1494509004869"/>
</node>
<node TEXT="Version publique" ID="ID_327555293" CREATED="1494508972805" MODIFIED="1494508977049"/>
<node TEXT="supprimer" ID="ID_1912440489" CREATED="1494509208772" MODIFIED="1494509212401">
<node TEXT="la suppression n&apos;est possible que si la version n&apos;est pas partag&#xe9;e" ID="ID_293862458" CREATED="1494509214114" MODIFIED="1494509231011"/>
</node>
<node TEXT="actions sur les composants" ID="ID_206410649" CREATED="1494509351298" MODIFIED="1494530350442"/>
</node>
<node TEXT="Structure des assemblages" ID="ID_1699443625" CREATED="1494509362813" MODIFIED="1494509369159">
<node TEXT="meta donn&#xe9;es" ID="ID_1163034822" CREATED="1494509370850" MODIFIED="1494509373593"/>
<node TEXT="arboresence de sections" ID="ID_1915984846" CREATED="1494509374249" MODIFIED="1494509402983">
<node TEXT="topics" ID="ID_1211759740" CREATED="1494509407228" MODIFIED="1494509558895">
<node TEXT="unit&#xe9; d&apos;affichage (indicateur)" ID="ID_659565233" CREATED="1494509562121" MODIFIED="1494509570199"/>
<node TEXT="ensemble de composants" ID="ID_369546368" CREATED="1494509570673" MODIFIED="1494509586625"/>
</node>
</node>
</node>
<node TEXT="Composants de visualisation" ID="ID_1891802189" CREATED="1494509629979" MODIFIED="1494509638745">
<node TEXT="Element d&apos;un topic" ID="ID_88758384" CREATED="1494509651453" MODIFIED="1494509690114">
<node TEXT="topics composites" ID="ID_1320669827" CREATED="1494509704446" MODIFIED="1494509822503">
<node TEXT="titre" ID="ID_1716785888" CREATED="1494509824011" MODIFIED="1494509826713"/>
<node TEXT="histogramme" ID="ID_1366176716" CREATED="1494509827500" MODIFIED="1494509831219"/>
<node TEXT="l&#xe9;gende" ID="ID_878657083" CREATED="1494509831570" MODIFIED="1494509834267"/>
<node TEXT="commentaire analyste" ID="ID_1037241842" CREATED="1494509834570" MODIFIED="1494509841175"/>
</node>
</node>
<node TEXT="Types de composants disponibles" ID="ID_417270052" CREATED="1494509660716" MODIFIED="1494510079516">
<node TEXT="Contenu" ID="ID_1022930593" CREATED="1494509979664" MODIFIED="1494509988689">
<node TEXT="title" ID="ID_1307703008" CREATED="1494509861087" MODIFIED="1494509880580"/>
<node TEXT="html" ID="ID_540751050" CREATED="1494509936479" MODIFIED="1494509938166"/>
<node TEXT="details" ID="ID_264109411" CREATED="1494509932317" MODIFIED="1494509935943"/>
<node TEXT="svg" ID="ID_533108691" CREATED="1494510083092" MODIFIED="1494510085382"/>
</node>
<node TEXT="Dataviz" ID="ID_1089351381" CREATED="1494509993512" MODIFIED="1494509998038">
<node TEXT="chart" ID="ID_614561102" CREATED="1494509922294" MODIFIED="1494509923909"/>
<node TEXT="map" ID="ID_865260740" CREATED="1494509938494" MODIFIED="1494510214797"/>
<node TEXT="navmap" ID="ID_16295900" CREATED="1494509952582" MODIFIED="1494509957075"/>
<node TEXT="navitem" ID="ID_1302740817" CREATED="1494509942318" MODIFIED="1494509951405"/>
<node TEXT="animated-heatmap" ID="ID_919694596" CREATED="1494509881148" MODIFIED="1494509920822"/>
</node>
<node TEXT="Commentaires analystes" ID="ID_1451829832" CREATED="1494510008225" MODIFIED="1494510014446">
<node TEXT="description" ID="ID_1665027157" CREATED="1494509924311" MODIFIED="1494509931995"/>
<node TEXT="wisiwyg" ID="ID_128030535" CREATED="1494509957778" MODIFIED="1494509970263"/>
</node>
</node>
<node TEXT="Xsl de description d&apos;un composant" ID="ID_1962696604" CREATED="1494510100054" MODIFIED="1494510109216">
<node TEXT="eLocus/src/kolekti_server/elocus/xsl/components" ID="ID_678874980" CREATED="1494510111445" MODIFIED="1494510228935"/>
<node TEXT="template /libs" ID="ID_351207405" CREATED="1494510231752" MODIFIED="1494510258855">
<node TEXT="ensemble des scripts et css necessaires &#xe0; la visualisation du composant" ID="ID_74123362" CREATED="1494510292795" MODIFIED="1494510319770"/>
<node TEXT="  &lt;xsl:template match=&quot;/libs&quot;&gt;&#xa;    &lt;libs&gt;&#xa;      &lt;css&gt;&#xa;        &lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/static/components/css/navmap.css&quot;/&gt;&#xa;      &lt;/css&gt;&#xa;      &lt;scripts&gt;&#xa;        &lt;script src=&quot;/static/d3.v4.min.js&quot;&gt;&lt;/script&gt;&#xa;        &lt;script src=&quot;https://d3js.org/d3-scale-chromatic.v1.min.js&quot;&gt;&lt;/script&gt;&#xa;        &lt;script type=&quot;text/javascript&quot;&#xa;                src=&quot;https://cdnjs.cloudflare.com/ajax/libs/wicket/1.3.2/wicket.min.js&quot;&gt;&lt;/script&gt;&#xa;        &lt;script src=&quot;/static/components/js/locale_fr.js&quot;/&gt;&#xa;        &lt;script type=&quot;text/javascript&quot; src=&quot;/static/components/js/navmap.js&quot;&gt;&lt;/script&gt;&#xa;      &lt;/scripts&gt;&#xa;    &lt;/libs&gt;&#xa;  &lt;/xsl:template&gt;" STYLE_REF="exemple" ID="ID_1173764758" CREATED="1494510322315" MODIFIED="1494510729430"/>
</node>
<node TEXT="templates html:div[@class=&quot;kolekti-component-xxx&quot;]" ID="ID_933936072" CREATED="1494510537117" MODIFIED="1494510780435">
<node TEXT="mode topictitle" ID="ID_1312115423" CREATED="1494510781941" MODIFIED="1494510799469">
<node TEXT="fragment a ins&#xe9;rer dans un titre de topic (en g&#xe9;n&#xe9;ral vide, sauf pour le component &quot;title&quot;" ID="ID_1783320993" CREATED="1494515461549" MODIFIED="1494515497241"/>
</node>
<node TEXT="mode topicbody" ID="ID_1558638311" CREATED="1494510800053" MODIFIED="1494510805490">
<node TEXT="fragment ins&#xe9;r&#xe9; dans le corps du topic, vue web standard" ID="ID_1641987552" CREATED="1494515499991" MODIFIED="1494515526741"/>
</node>
<node TEXT="mode topicpanelinfo" ID="ID_422755570" CREATED="1494510805998" MODIFIED="1494510818595"/>
<node TEXT="mode topicpanelaction" ID="ID_1862280520" CREATED="1494510818926" MODIFIED="1494510826042"/>
<node TEXT="mode topicpannelbutton" ID="ID_1983464170" CREATED="1494510826446" MODIFIED="1494510832878"/>
<node TEXT="mode topicpanelbody" ID="ID_562477827" CREATED="1494510833392" MODIFIED="1494510843055"/>
</node>
</node>
</node>
<node TEXT="Requetes sparql" ID="ID_331674828" CREATED="1494510923265" MODIFIED="1494510930455">
<node TEXT="ins&#xe9;r&#xe9;es dans les composants" ID="ID_813275364" CREATED="1494510933034" MODIFIED="1494510966824"/>
<node TEXT="dans les sources (modules et assemblages)" ID="ID_1982165401" CREATED="1494510967373" MODIFIED="1494510984684"/>
<node TEXT="fichiers de r&#xe9;sultats de requetes" ID="ID_886240780" CREATED="1494510985372" MODIFIED="1494511118985"/>
</node>
<node TEXT="Processus de cr&#xe9;ation" ID="ID_1009396807" CREATED="1494530325731" MODIFIED="1494530330989">
<node TEXT="variables" ID="ID_1020052735" CREATED="1494839721553" MODIFIED="1494839723739">
<node TEXT="topicvar" ID="ID_1062341376" CREATED="1494839724805" MODIFIED="1494839738053">
<node TEXT="D&#xe9;finies dans l&apos;url d&apos;appel du topic (trame)" ID="ID_214239588" CREATED="1494839743066" MODIFIED="1494839760235"/>
</node>
<node TEXT="uservar" ID="ID_431184701" CREATED="1494839730658" MODIFIED="1494839733040">
<node TEXT="D&#xe9;finies dans /sources/fr/variables" ID="ID_1948773172" CREATED="1494839763028" MODIFIED="1494839783058">
<node TEXT="Elements &quot;variable&quot;" ID="ID_1132958818" CREATED="1494839818642" MODIFIED="1494839835554">
<node TEXT="Nom" ID="ID_189217572" CREATED="1494839836669" MODIFIED="1494839838264"/>
<node TEXT="label" ID="ID_91299222" CREATED="1494839838921" MODIFIED="1494839840626"/>
<node TEXT="values" ID="ID_700601590" CREATED="1494839844288" MODIFIED="1494839847521">
<node TEXT="ensemble des valeurs possibles" ID="ID_939699114" CREATED="1494839848582" MODIFIED="1494839867709"/>
<node TEXT="r&#xe9;sultat d&apos;une requete" ID="ID_1551656162" CREATED="1494839867987" MODIFIED="1494839884959">
<node TEXT="select ValueData, ValueLabel" ID="ID_1238997797" CREATED="1494839885837" MODIFIED="1494839918285"/>
</node>
</node>
</node>
<node TEXT="Elements query" ID="ID_720896357" CREATED="1494840557655" MODIFIED="1494840567734"/>
</node>
<node TEXT="R&#xe9;f&#xe9;renc&#xe9;es depuis meta donn&#xe9;es trame" ID="ID_936581783" CREATED="1494839783539" MODIFIED="1494839817246"/>
</node>
</node>
</node>
<node TEXT="Processus de publication" ID="ID_736560848" CREATED="1494530331565" MODIFIED="1494530337879">
<node TEXT="production docx" ID="ID_1163648638" CREATED="1494837080640" MODIFIED="1494837090178"/>
</node>
</node>
<node TEXT="Cr&#xe9;er un composant" POSITION="right" ID="ID_506553153" CREATED="1494575184124" MODIFIED="1494575191635">
<edge COLOR="#7c0000"/>
<node TEXT="d&#xe9;terminer un nom" ID="ID_642109525" CREATED="1494575287549" MODIFIED="1494575441903">
<node TEXT="utilis&#xe9; comme partie de nom de fichier" ID="ID_573223990" CREATED="1494575319909" MODIFIED="1494575340529"/>
<node TEXT="utilis&#xe9; comme partie d&apos;attribut class" ID="ID_1564766525" CREATED="1494575341001" MODIFIED="1494575371552"/>
<node TEXT="pas d&apos;espace" ID="ID_1512114036" CREATED="1494575372238" MODIFIED="1494575376219"/>
<node TEXT="accents d&#xe9;conseill&#xe9;s" ID="ID_1180280109" CREATED="1494575376637" MODIFIED="1494575387120"/>
<node TEXT="exemple" ID="ID_256167548" CREATED="1494575387595" MODIFIED="1494575406577">
<node TEXT="percent-circles" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_141814193" CREATED="1494575408382" MODIFIED="1494575658171"/>
</node>
</node>
<node TEXT="d&#xe9;clarer le composant" ID="ID_289691657" CREATED="1494575432959" MODIFIED="1494575451019">
<node TEXT="/eLocus/src/kolekti_server/elocus/xsl/elocus_components.xsl" STYLE_REF="srcpath" ID="ID_760132377" CREATED="1494529649682" MODIFIED="1494577589546"/>
<node TEXT="ajouter" ID="ID_1448689947" CREATED="1494575645235" MODIFIED="1494575651004">
<node TEXT=" &lt;xsl:include href=&quot;components/percent-circles.xsl&quot;/&gt;" STYLE_REF="exemple" ID="ID_1149054341" CREATED="1494577503699" MODIFIED="1494577538142"/>
</node>
</node>
<node TEXT="cr&#xe9;er le xsl de g&#xe9;n&#xe9;ration de l&apos;UI du composant" ID="ID_89413771" CREATED="1494577544045" MODIFIED="1494577560774">
<node TEXT="/eLocus/src/kolekti_server/elocus/xsl/components/percent-circles.xsl" STYLE_REF="srcpath" ID="ID_163800150" CREATED="1494529649682" MODIFIED="1494577608083"/>
<node TEXT="a partir du template pr&#xe9;sent dans le dossier components/" ID="ID_1309802879" CREATED="1494577617477" MODIFIED="1494577632043"/>
<node TEXT="d&#xe9;clarer les &#xe9;l&#xe9;ments statiques &#xe0; utiliser" ID="ID_1282473825" CREATED="1494577632519" MODIFIED="1494577658976">
<node TEXT="scripts" ID="ID_176554320" CREATED="1494577661087" MODIFIED="1494577667025"/>
<node TEXT="feuilles de style" ID="ID_1935978164" CREATED="1494577667644" MODIFIED="1494577673210"/>
<node TEXT="autres" ID="ID_1650088283" CREATED="1494577673786" MODIFIED="1494577681503"/>
</node>
<node TEXT="&#xe9;crire les templates de visualisation" ID="ID_1696713933" CREATED="1494577687406" MODIFIED="1494577699527"/>
</node>
<node TEXT="cr&#xe9;er une instance de rapport utilisant le composant" ID="ID_103796572" CREATED="1494577702049" MODIFIED="1494577734767">
<node TEXT="dans le projet" ID="ID_1368956139" CREATED="1494577736978" MODIFIED="1494578568296">
<node TEXT="https://dev.elocus.fr/dev/" ID="ID_416090714" CREATED="1494578577576" MODIFIED="1494578604646"/>
</node>
<node TEXT="cr&#xe9;er un nouveau module" ID="ID_1082344005" CREATED="1494578608193" MODIFIED="1494578622907"/>
</node>
</node>
<node TEXT="D&#xe9;ploiement d&apos;une plateforme eLocus" POSITION="left" ID="ID_1550455446" CREATED="1494575269321" MODIFIED="1494575283694">
<edge COLOR="#007c7c"/>
</node>
<node TEXT="Administration de le plateforme" POSITION="left" ID="ID_831799362" CREATED="1494575255292" MODIFIED="1494577453970">
<edge COLOR="#7c007c"/>
</node>
<node TEXT="Adapter elocus &#xe0; une charte graphique" POSITION="left" ID="ID_310151068" CREATED="1494575237170" MODIFIED="1494575254518">
<edge COLOR="#007c00"/>
</node>
<node TEXT="Dimension collaborative" POSITION="left" ID="ID_1953319317" CREATED="1494575221193" MODIFIED="1494575228707">
<edge COLOR="#00007c"/>
<node TEXT="analyses saisies" ID="ID_1877393844" CREATED="1494836299742" MODIFIED="1494836332784"/>
<node TEXT="mise &#xe0; jour des rapport" ID="ID_43829810" CREATED="1494836333360" MODIFIED="1494836345413">
<node TEXT="notification" ID="ID_1773351401" CREATED="1494836345420" MODIFIED="1494836349473"/>
<node TEXT="trigger" ID="ID_1466624447" CREATED="1494836350797" MODIFIED="1494836353524"/>
</node>
</node>
<node TEXT="git" POSITION="left" ID="ID_1640423738" CREATED="1494856431569" MODIFIED="1494856433425">
<edge COLOR="#ff0000"/>
<node TEXT="branches" ID="ID_534242269" CREATED="1494856435372" MODIFIED="1494856438107">
<node TEXT="master" ID="ID_1888394570" CREATED="1494856439020" MODIFIED="1494856441501">
<node TEXT="sert aux releases" ID="ID_593188755" CREATED="1494856442464" MODIFIED="1494856447038"/>
<node TEXT="on de dev pas dedans" ID="ID_1191236811" CREATED="1494856447652" MODIFIED="1494856453313"/>
</node>
<node TEXT="dev" ID="ID_198857536" CREATED="1494856454551" MODIFIED="1494856456064"/>
</node>
</node>
</node>
</map>
