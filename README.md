eLocus
=======

eLocus permet la production automatisée de rapports ad hoc, à partir de données ouvertes ou propriétaires.


Votre domaine d’expertise (santé, social, tourisme, économie, etc.) est modélisé à l’aide de référentiels métiers utilisés à la fois pour l’organisation des données et des documents.


Des pré-rapports sont créés d’un seul clic, avec les données à jour et présentées dans des modes visuels pertinents (graphiques, tableaux, cartographie, dataviz, etc.). Il est ainsi possible de se concentrer sur les tâches d’analyse et de rédaction à l’aide d’un éditeur de texte collaboratif.


Les résultats sont partagés sous forme de sites Web, documents Office ou tableaux de bords. Ils peuvent être améliorés de façon itérative à l’aide d’avis et d’informations complémentaires collectées lors de la consultation. 

Installation
------------

### Pre-requis

docker, docker-compose svn

### Installation

Pour installer eLocus :

    mkdir elocus-compose
    cd elocus-compose
    git clone https://gitlab.com/elocus/elocus eLocus
    cp eLocus/docker-compose.yml.sample docker-compose.yml
    cp eLocus/.env.sample .env
    mkdir -p run/svn run/db run/projects run/log/kolekti run/log/apache run/private run/static run/fonts 

Adaptez le `.env` et le `docker-compose.yml` selon vos besoins.

Lancez le tout :

    docker-compose up

Créer un utilisateur d'administration de la plateforme 

```
docker exec -ti  elocus_kolekti_1 python /kolekti/src/kolekti_server/manage.py createsuperuser
```
Se connecter sur https://subdomain.elocus.fr/admin/ en tant qu'utilisateur admin

1. Créer un utilisateur, Auth>User
1. Créer un kolekti>user_profile lié à cet utilisateur
1. Créer un template pour les nouveaux projets, (le template est un export d'une hiérarchie existante dans un svn autre (``https://demo.elocus.fr/svn/demo_elocus`` pour initialiser un nouveau projet à partir du projetdemo type)
1. Créer un nouveau projet basé sur ce template
1. Créer un nouveau UserProject pour associer l'utilisateur et le projet.
 
**Note :** Si vous configurez eLocus pour fonctionner sur `localhost`, vous
aurez probablement une erreur `400 BAD request` en allant sur
`http://localhost:8001`. Ce problèm viens du fait que Django n'autorise pas
l'accès par `localhost` en prod. Pour régler ce soucis, décommentez la ligne
suivante dans le `.env` :

    KOLEKTI_DEBUG=True



Mise à jour
-----------

Les fichiers `docker-compose.yaml.sample` et `.env.sample` peuvent évoluer.
Si après une mise à jour du dépôt (`git pull`), vous n'arrivez plus à démarrer
votre instance eLocus, pensez à faire un `diff` entre votre version de ces
fichiers et ceux sur le dépôt et au besoin mettez à jour vos fichiers.

