# Changelog


## 2017-06-05  Stéphane Bonhomme <stephane@exselt.com> bd7539667fcd4cd38d9433bc01596dd7eefbb236

* The SVN authentication method has changed, to make it work again, change the
password of at least one user, and save on UserProject


## 2017-06-04  Stéphane Bonhomme <stephane@exselt.com>

*   Due to a directory organisation change, it is required to run from
 elocus-deploy directory:

	    mkdir run
	    for d in svn log projects db; do mv $d run/; done
	    mkdir run/private run/static run/fonts
	    # Get the last docker-compose (you might want do a diff before)
	    cp eLocus/docker-compose.yml.sample docker-compose.yml
	    diff .env eLocus/.env.sample
	    # IMPORTANT: you have to manually merge the .env file at this point
	    docker-compose pull
